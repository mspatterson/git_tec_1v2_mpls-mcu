//******************************************************************************
//
// main.c
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-     		DD          Initial Implementation
//	....
//
//	2013-09-18 		DD			Update RFC packet structure
//	2013-10-22		DD			Turn On/Off the Strain gauges in Idle mode. Strain gauges ADCs initialised in InitIdleMode() only.
//	2014-07-17		DD			use ADC internal IDAC instead of external Vref.
//*******************************************************************************


#include "..\drivers\drivers.h"


#define ZB_TX_PERIOD		5		// +1ms to read the compass 			//in ms
#define IGNORE_FIRST_SAMPLES	500//2000
#define OFFSET_CALIBRATION_SAMPLES	10
#define COUNTS_PER_POUND	64
#define ADC_OFFSET_TIMEOUT	10000

#define ALL_CHANNELS_OFFSET_DONE	0x7F	//0x3F
#define ZB_NETWORK_TIMEOUT		10
#define SAMPLES_IN_PACKET		2

#define ADC_OFFSET_SAMPLES		1000
#define CMD_START				0x23
#define CMD_STOP				0x34


MAGNETIC_DATA	sMagnetData;

ACCELEROMETOR_DATA	sAccelData;


typedef enum
{
	ST_INIT,
	ST_HUNT,
	ST_IDLE,
	ST_STREAMING,
	ST_DEEP_SLEEP
}WTTTS_STATES;

WTTTS_STATES currentState;
//WTTTS_STATES newState;
const WTTTS_RATE_TYPE getRfcRate = SRT_RFC_RATE;
const WTTTS_RATE_TYPE getRfcTimeout = SRT_RFC_TIMEOUT;
const WTTTS_RATE_TYPE getStreamRate = SRT_STREAM_RATE;
const WTTTS_RATE_TYPE getStreamTimeout = SRT_STREAM_TIMEOUT;
const WTTTS_RATE_TYPE getPairTimeout = SRT_PAIR_TIMEOUT;
const UVOLTS SG_DIVIDER_VALUE = 2050000l;


//char * pcString;
static BYTE adcNumber;
static ADS1220_SOURCE analogChannel;
static BYTE bDataArray[30] = {0};
static UVOLTS uvResultTemp[ADS1220_TOTAL] = {0};
UVOLTS uvResultAverage[ADS1220_TOTAL];
UVOLTS uvResultAverageTemp[ADS1220_TOTAL];


static UVOLTS uvOffsetAverage[ADS1220_TOTAL];

static TIMERHANDLE thGenTimer, thGenTimer1,thGenTimer2;
static TIMERHANDLE thTimerSec1,thTimerSec2;

WORD	wCounter;
static BYTE bStGyroId;


static BYTE pUartRxData[BUF_LENGTH];

signed long gyroSumAverage;
signed long slVar;
static WORD nbrTransmission;
static WORD nbrResponses;
WORD maxPktLength;

WORD SamplesCounter[ADC_USED_LAST+2] = {0};
static BYTE AdcReadingDone = 0;
static BYTE firstSGSample[ADS1220_TOTAL] = {0};
static WORD batSampleCounter;
static 	BYTE takeSample;
static BYTE firstGyroSample;
static 	BOOL readTorque;
static 	BOOL readTension;
static BOOL readCompass;
static BOOL readAccelerometer;
static BOOL radioRxOn;
static WORD errorPackets;

BOOL digGyroOnOff;
BOOL tensionChOn;

WORD wGyroReadCounter;
WORD digGyroSampleCounter;

ADS1220_IDAC_CURRENT idacCurrent250ua = AD1220_IDAC_CURRENT_250uA;
ADS1220_IDAC_CURRENT idacCurrent1000ua = AD1220_IDAC_CURRENT_1000uA;
ADS1220_IDAC_CURRENT idacCurrent1500ua = AD1220_IDAC_CURRENT_1500uA;		// only for use with 1 KOhm Strain Gauge
ADS1220_IDAC_CURRENT idacCurrent500ua = AD1220_IDAC_CURRENT_500uA;		// only for use with 1 KOhm Strain Gauge
ADS1220_IDAC_CURRENT idacCurrent[ADS1220_TOTAL];		// only for use with 1 KOhm Strain Gauge



///////////////////////////////////////////////////////////
//
// Local function declaration
//
///////////////////////////////////////////////////////////

void SampleData(BYTE bIndex);

void EpsonGyroRead(void);
void StrainGaugeRead(void);
void ReadExtGyro(void);

void SystemPowerDown(void);
void TestPowerDownMode(void);
void SystemInit(void);
void TestCompass(void);
void TestGasGauge(void);

void InitStreamMode(void);
void InitIdleMode(void);
void RfCommTask(void);
void WtttsMainTask(void);
void StreamingTask(void);
void IdleTask(void);
void PowerDownTask(void);

BOOL ReadCompass(void);
void EpsonGyroGetOffset(void);
void InitHuntingMode(void);
void ConnectingTask(void);
void InitAccelerometer(void);
BOOL ReadAccelerometer(void);
void TestAccelerometer(void);
void InitGasGauge(void);
BOOL StrainGaugeReadInIdleMode(void);
void TurnOnAllAdcRef(void);
void TurnOffAllAdcRef(void);
void InitStrainGaugesReading(void);
void TestStGyro(void);
void InitADCInRFC(void);
void PowerDownADC(void);
void ReadSerNumber(void);
void TestSerNumber(void);
void TestSensorReadings(void);
void TestMcuAdc(void);
void InitAnalogChInStreamingMode(void);
void ReadTorqueGauges(void);
void ReadTensionGauges(void);
void DummyReadTensionGaugesRotation(void);
void UpdateTensionGaugesInTxPacket(void);
void ReadTensionGaugesInTmpArray(void);
void StrainGaugeReadInIdleModeV1(void);
void StrainGaugeReadInIdleModeV2(void);
void AverageTheResults(void);
void StrainGaugeDummyRead(void);
void PowerDownMCU(void);
void MeasureStrainGaugeValueAndSetCurrent(void);


void main(void)
{
	initMPS430Pins();
	currentState = ST_INIT;
	ReadLastResetCause();


	memset(&sMagnetData, 0,sizeof(sMagnetData));
	memset(&pUartRxData, 0,sizeof(pUartRxData));
	memset(&uvResultAverageTemp, 0,sizeof(uvResultAverageTemp));
	
	StopWatchdog();
	SetWatchdog();
//	__delay_cycles(5000);

	ResetWatchdog();
//	initMPS430Pins();
	Set_System_Clock();
	SpiA0Initialize();
	InitTimers();
	__bis_SR_register(GIE);
	EnableAllPower();
	thGenTimer = RegisterTimer();
	thGenTimer1 = RegisterTimer();
	thGenTimer2 = RegisterTimer();
	thTimerSec1 = RegisterSecTimer();
	thTimerSec2 = RegisterSecTimer();
	StartSecTimer(thTimerSec1);
	StartSecTimer(thTimerSec2);

	ResetWatchdog();

	InitRadioInterface();

	ReedSwitchInit();
	SetOutputPin(SW_LED, TRUE );

	ResetTimer( thGenTimer, 10000 );
	StartTimer( thGenTimer );
	StartTimer( thGenTimer2 );

	InitPIBInterface();
//	SetOutputPin(SW_LED, FALSE );
// start test
	// test delete
//	while(1)
//	{
//		LedSwPowerDownSequence();
//		__delay_cycles(50000);
//
//	}
	//end of test

	//EvalBatType();
	MeasureBatVoltageAdc();

	#if ST_GYRO_ENABLE
	initST();
	readSTid();
	bStGyroId = gedStId();
	SelfTestStGyro();
	calibrateStGyro();
	#endif

	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}

	SetOutputPin(SW_LED, FALSE );
	ResetTimer( thGenTimer, ZB_TX_PERIOD );
	InitGasGauge();

//	TestStGyro();
//	TestSensorReadings();
//	TestMcuAdc();

	ReadSerNumber();
	StopRtc();
	errorPackets = 0;
	ResetSecTimer( thTimerSec2, 10 );

	EnableExtPower();
	while(1)
	{
		ResetWatchdog();
		RfCommTask();
		WtttsMainTask();
		updateMplCommState();
		if(IsSwitchOn()==FALSE)
		{
			SystemPowerDown();
		}
		// check for low battery voltage and power down
		if(GetBatVoltageAdc()<UVLO_LOCKOUT_MV)
		{
			SystemPowerDown();
		}

	}

}






//******************************************************************************
//
//  Function: EpsonGyroRead
//
//  Arguments: none
//
//  Returns: none
//
//  Description: read the analog gyro output. Do weighted average and accumulate the result.
//
//******************************************************************************
void EpsonGyroRead(void)
{
//	WORD wStatus;

	if(ADS1220HasResult(ADC_EPSON_GYRO))
	{
		ADS1220ReadVoltageCont(ADC_EPSON_GYRO, &uvResultTemp[ADC_EPSON_GYRO]);
		if(firstGyroSample)
		{
			uvResultAverage[ADC_EPSON_GYRO]=uvResultTemp[ADC_EPSON_GYRO];
			firstGyroSample=0;
		}
		else
			uvResultAverage[ADC_EPSON_GYRO]=(uvResultAverage[ADC_EPSON_GYRO]*3+uvResultTemp[ADC_EPSON_GYRO])/4;

		gyroSumAverage += uvResultAverage[ADC_EPSON_GYRO];		//
		wGyroReadCounter++;
	}
}


//******************************************************************************
//
//  Function: EpsonGyroGetOffset
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  	Read the analog gyro offset. The unit should not be moving.
//				  	This function is not used in the current firmware.
// 					The offset is calculated by the computer software.
//******************************************************************************
void EpsonGyroGetOffset(void)
{
	int i;
	analogChannel = ADS1220_IN_AIN1_AIN2;
//	analogChannel = ADS1220_IN_AVDD_BY_4;
	//analogChannel = ADS1220_IN_VREF;
	ADS1220Configure(analogChannel,ADC_EPSON_GYRO, AD1220_IDAC_CURRENT_OFF );
	ResetTimer( thGenTimer, 10 );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
	ADS1220ReadAllRegs(ADC_EPSON_GYRO, bDataArray,sizeof(bDataArray) );
	ADS1220StartConversion(ADC_EPSON_GYRO);
	i = 500;
	uvOffsetAverage[ADC_EPSON_GYRO] = 0;
	while(i)
	{
		if(ADS1220HasResult(ADC_EPSON_GYRO))
		{
			ADS1220ReadVoltageCont(ADC_EPSON_GYRO, &uvResultTemp[ADC_EPSON_GYRO]);
			uvOffsetAverage[ADC_EPSON_GYRO]=(uvOffsetAverage[ADC_EPSON_GYRO]*3+uvResultTemp[ADC_EPSON_GYRO])/4;
			i--;
		}
		ResetWatchdog();
	}
}


//******************************************************************************
// Function name:    StrainGaugeRead
//******************************************************************************
// Description:      read all strain gauges ADC on a rotational basis
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void StrainGaugeRead(void)
{
	// sample all ADC except the Gyro ADC on a rotational basis
	if(adcNumber>ADC_USED_LAST)
		adcNumber = ADC_USED_FIRST;

	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
		uvResultAverage[adcNumber]=(uvResultAverage[adcNumber]*15+uvResultTemp[adcNumber])/16;
	}

	adcNumber++;
}

//******************************************************************************
// Function name:    InitStrainGaugesReading
//******************************************************************************
// Description:      initialize the strain gauges reading in Idle mode
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void InitStrainGaugesReading(void)
{
	memset(&SamplesCounter, 0,sizeof(SamplesCounter));
	memset(&firstSGSample, 0,sizeof(firstSGSample));

	AdcReadingDone = 0;
	adcNumber = ADC_USED_FIRST;
}

//******************************************************************************
// Function name:    StrainGaugeReadInIdleMode
//******************************************************************************
// Description:      read all strain gauges ADC on a rotational basis "IDLE_MODE_ADC_SAMPLES" times and average them.
//
// parameters:       none
//
// Returned value:   TRUE - when all strain gauges ADC are sampled "IDLE_MODE_ADC_SAMPLES" times, FALSE otherwise
//
//******************************************************************************
BOOL StrainGaugeReadInIdleMode(void)
{

	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);

		if(firstSGSample[adcNumber] == 0)
		{
			uvResultAverage[adcNumber]=uvResultTemp[adcNumber];		// don't average the first sample
			firstSGSample[adcNumber] = 1;
		}
		else
			uvResultAverage[adcNumber]=(uvResultAverage[adcNumber]+uvResultTemp[adcNumber])/2;

		SamplesCounter[adcNumber]++;
	}

	if(SamplesCounter[adcNumber] >=IDLE_MODE_ADC_SAMPLES)
	{
		AdcReadingDone|=1<< adcNumber;
	}

	adcNumber++;
	// sample all ADC except the Gyro ADC on a rotational basis
	if(adcNumber>ADC_USED_LAST)
		adcNumber = ADC_USED_FIRST;

	if(AdcReadingDone!= ALL_SIX_ADC_READ)
		return FALSE;
	else
		return TRUE;
}

//******************************************************************************
// Function name:    StrainGaugeReadInIdleMode
//******************************************************************************
// Description:      read all strain gauges ADC on a rotational basis "IDLE_MODE_ADC_SAMPLES" times and average them.
//
// parameters:       none
//
// Returned value:   TRUE - when all strain gauges ADC are sampled "IDLE_MODE_ADC_SAMPLES" times, FALSE otherwise
//
//******************************************************************************
void StrainGaugeReadInIdleModeV1(void)
{

	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);

		if(firstSGSample[adcNumber] == 0)
		{
			uvResultAverage[adcNumber]=uvResultTemp[adcNumber];		// don't average the first sample
			firstSGSample[adcNumber] = 1;
		}
		else
			uvResultAverage[adcNumber]=(uvResultAverage[adcNumber]*7+uvResultTemp[adcNumber])/8;
//			uvResultAverage[adcNumber]=uvResultTemp[adcNumber];	// no averaging

	}


	adcNumber++;
	// sample all ADC except the Gyro ADC on a rotational basis
	if(adcNumber>ADC_USED_LAST)
		adcNumber = ADC_USED_FIRST;

}

//******************************************************************************
// Function name:    StrainGaugeReadInIdleMode
//******************************************************************************
// Description:      read all strain gauges ADC on a rotational basis and accumulate them over one transmission period.
//					 Count number of samples.
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void StrainGaugeReadInIdleModeV2(void)
{

	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);

		if(firstSGSample[adcNumber] == 0)
		{
			uvResultAverage[adcNumber]=uvResultTemp[adcNumber];		// don't average the first sample
			firstSGSample[adcNumber] = 1;
		}
		else
			uvResultAverage[adcNumber] += uvResultTemp[adcNumber];

		SamplesCounter[adcNumber]++;
	}

	adcNumber++;
	// sample all ADC except the Gyro ADC on a rotational basis
	if(adcNumber>ADC_USED_LAST)
		adcNumber = ADC_USED_FIRST;

}


void AverageTheResults(void)
{
	int i;

	// average the results if data has been read from the ADC
	// otherwise send the old values.

	for(i=ADC_USED_FIRST;i<=ADC_USED_LAST;i++)
	{
		if(SamplesCounter[i])		// we read at least one data from the ADC.
		{
			uvResultAverage[i] /= SamplesCounter[i];
		}
	}


}


//******************************************************************************
// Function name:    StrainGaugeDummyRead
//******************************************************************************
// Description:      read the SG in a temporary array
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void StrainGaugeDummyRead(void)
{

	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
	}

	adcNumber++;
	// sample all ADC except the Gyro ADC on a rotational basis
	if(adcNumber>ADC_USED_LAST)
		adcNumber = ADC_USED_FIRST;

}

//******************************************************************************
// Function name:    ReadTorqueGauges
//******************************************************************************
// Description:      read only Torque strain gauges ADC
//					 This includes channel 0 and 1
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void ReadTorqueGauges(void)
{
	adcNumber = ADC_TORQUE_CH0;
	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
		uvResultAverage[adcNumber]=(uvResultAverage[adcNumber]*15+uvResultTemp[adcNumber])/16;
	}

	adcNumber = ADC_TORQUE_CH1;
	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
		uvResultAverage[adcNumber]=(uvResultAverage[adcNumber]*15+uvResultTemp[adcNumber])/16;
	}

}


//******************************************************************************
// Function name:    ReadTensionGauges
//******************************************************************************
// Description:      read only Tension strain gauges ADC
//					 This includes channels 2, 3, 4 and 5
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void ReadTensionGauges(void)
{
	adcNumber = ADC_TENSION_CH0;
	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
		uvResultAverage[adcNumber]=(uvResultAverage[adcNumber]*3+uvResultTemp[adcNumber])/4;
	}
	adcNumber = ADC_TENSION_CH1;
	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
		uvResultAverage[adcNumber]=(uvResultAverage[adcNumber]*3+uvResultTemp[adcNumber])/4;
	}
	adcNumber = ADC_TENSION_CH2;
	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
		uvResultAverage[adcNumber]=(uvResultAverage[adcNumber]*3+uvResultTemp[adcNumber])/4;
	}
	adcNumber = ADC_TENSION_CH3;
	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
		uvResultAverage[adcNumber]=(uvResultAverage[adcNumber]*3+uvResultTemp[adcNumber])/4;
	}

}


//******************************************************************************
// Function name:    ReadTensionGaugesInTmpArray
//******************************************************************************
// Description:      read only Tension strain gauges ADC
//					 This includes channels 2, 3, 4 and 5
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void ReadTensionGaugesInTmpArray(void)
{
	adcNumber = ADC_TENSION_CH0;
	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
		uvResultAverageTemp[adcNumber]=(uvResultAverageTemp[adcNumber]*3+uvResultTemp[adcNumber])/4;
	}
	adcNumber = ADC_TENSION_CH1;
	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
		uvResultAverageTemp[adcNumber]=(uvResultAverageTemp[adcNumber]*3+uvResultTemp[adcNumber])/4;
	}
	adcNumber = ADC_TENSION_CH2;
	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
		uvResultAverageTemp[adcNumber]=(uvResultAverageTemp[adcNumber]*3+uvResultTemp[adcNumber])/4;
	}
	adcNumber = ADC_TENSION_CH3;
	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
		uvResultAverageTemp[adcNumber]=(uvResultAverageTemp[adcNumber]*3+uvResultTemp[adcNumber])/4;
	}

}

//******************************************************************************
// Function name:    ReadTensionGaugesInTmpArray
//******************************************************************************
// Description:      read only Tension strain gauges ADC
//					 This includes channels 2, 3, 4 and 5
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void UpdateTensionGaugesInTxPacket(void)
{

	uvResultAverage[ADC_TENSION_CH0]=uvResultAverageTemp[ADC_TENSION_CH0];
	uvResultAverage[ADC_TENSION_CH1]=uvResultAverageTemp[ADC_TENSION_CH1];
	uvResultAverage[ADC_TENSION_CH2]=uvResultAverageTemp[ADC_TENSION_CH2];
	uvResultAverage[ADC_TENSION_CH3]=uvResultAverageTemp[ADC_TENSION_CH3];
}


void SystemPowerDown(void)
{
	//SetOutputPin(SW_LED, TRUE );
	DisableAnalogPower();
	DisableDigitalPower();
	DisableRadioPower();
	DisableAnalogGyro();

	SendSleepgMode();

	LedSwPowerDownSequence();
	SendSleepgMode();
	ResetTimer( thGenTimer, 1000 );
	StartTimer( thGenTimer );
	while(TimerExpired( thGenTimer)==FALSE)
	{
		ResetWatchdog();
	}
	SetOutputPin(SW_LED, FALSE );

	StopWatchdog();

	SetSystemDeepSleep();
	SystemInit();
//	LedSwPowerUpSequence();
//	WDTCTL = WDT_MDLY_0_064;
//	while(1);
}


void SystemInit(void)
{
	currentState = ST_INIT;
	SetOutputPin(SW_LED, TRUE );
	memset(&sMagnetData, 0,sizeof(sMagnetData));
	memset(&pUartRxData, 0,sizeof(pUartRxData));

	//StopWatchdog();
#ifdef WATCHDOG_ENABLE
		ResetWatchdog();
#endif

	initMPS430Pins();
	EnableAllPower();
	Set_System_Clock();
	SpiA0Initialize();
//	InitTimers();
	SysTimerConfig();
	__bis_SR_register(GIE);

//	thGenTimer = RegisterTimer();
//	thGenTimer1 = RegisterTimer();
//	thTimerSec1 = RegisterSecTimer();
//	thTimerSec2 = RegisterSecTimer();

	InitRadioInterface();

	ReedSwitchInit();
	LedSwPowerUpSequence();

	MeasureBatVoltageAdc();
	#if COMPASS_ENABLE
	CompassInit();
//	while(CompassTxDone()==FALSE);
	ResetTimer( thGenTimer, 10 );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
//	CompassRegisterDump();
//	while(CompassDataReady()==FALSE);
//	CompassGetRawData(bDataArray,sizeof(bDataArray));
	#endif

	#if ST_GYRO_ENABLE
	initST();
	readSTid();
	bStGyroId = gedStId();
	SelfTestStGyro();
	calibrateStGyro();
	#endif

	ResetTimer( thGenTimer, ZB_TX_PERIOD );
	ResetWatchdog();

	memset(&firstSGSample, 0,sizeof(firstSGSample));

	SetOutputPin(SW_LED, FALSE );
}




void RfCommTask(void)
{
	static WORD pktLength;

	if (ReceiverHasData())
	{
		pktLength = ReadReceiverData(pUartRxData, sizeof(pUartRxData));
		if(pktLength >sizeof(pUartRxData))
		{
			__no_operation();
		}
		if(pktLength >maxPktLength)
		{
			maxPktLength = pktLength;
		}
		if(HaveWtttsPacket(pUartRxData, pktLength))
		{
			ExecuteCommand();

			// after command different from "NO_COMMAND" send RFC after RFC_RESPONSE_TIME
			// does not send right away because the radio relies on 2 ms interval between the packets
			if(GetCommandStatus()==FALSE)
			{
				ResetTimer( thGenTimer, RFC_RESPONSE_TIME );
			}
		}
		else
		{
			errorPackets++;
		}

	}
}


void WtttsMainTask(void)
{
	switch(currentState)
	{
	case ST_INIT:
		InitHuntingMode();
		currentState = ST_HUNT;
		break;
	case ST_HUNT:
		ConnectingTask();
		break;
	case ST_IDLE:
		IdleTask();
		break;
	case ST_STREAMING:
		StreamingTask();
		break;
	case ST_DEEP_SLEEP:
		PowerDownTask();
		break;
	default:
		currentState = ST_INIT;
		break;

	}
}


//******************************************************************************
//
//  Function: InitHuntingMode
//
//  Arguments: none
//
//  Returns: none
//
//  Description: Initialise the board for hunting mode. It's the same as InitIdleMode for now.
//
//
//
//******************************************************************************
void InitHuntingMode(void)
{
	EnableAllPower();
	InitRadioInterface();
	ResetWatchdog();
	UpdateTimeParamsFromFlash();

	// set the compass in power down with command. Do not turn of the power
	//because it pulls low the I2C bus
	// TO DO

	// TO DO - ST gyro init or not ?

	// turn off the reference voltage
	TurnOffAllAdcRef();

	ResetWatchdog();

	SendHuntingMode();

	// configure ADC6 as temperature sensor
	ADS1220ReadAllRegs(ADC_USED_AS_TEMP, bDataArray, sizeof(bDataArray));
	ResetTimer( thGenTimer, 5 );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}

	ADS1220ConfigAsTemperatureSensor(ADC_USED_AS_TEMP);
	ResetTimer( thGenTimer, 5);
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
			ResetWatchdog();
	}
	ADS1220ReadAllRegs(ADC_USED_AS_TEMP, bDataArray,sizeof(bDataArray));
	ADS1220StartConversion(ADC_USED_AS_TEMP);

	// configure the timers
	ResetTimer( thGenTimer, RFC_HUNTING_PERIOD_MS );
	ResetTimer( thGenTimer1, CHANNEL_CYCLE_MS );
	ResetSecTimer( thTimerSec1, GetTimeParameter(getPairTimeout) );
	StartTimer( thGenTimer );
	StartTimer( thGenTimer1 );
	StartSecTimer( thTimerSec1);
	nbrTransmission = 0;
	nbrResponses = 0;


}

//******************************************************************************
//
//  Function: ConnectingTask
//
//  Arguments: none
//
//  Returns: none
//
//  Description: read temperature, RPM and gas gauge. sends RFC every 100 ms.
// 				If it doesn't get response cycle the channels every UPDATE_CHANNEL_NBR_TR transmission
//				If it doesn't get response after HUNTING_MODE_TIMEOUT_SEC go to sleep
//				If it gets response go to IDLE mode
//******************************************************************************
void ConnectingTask(void)
{

	// if a valid radio packet has been received increment nbrResponses
	if(GetRadioPacketStatus()==TRUE)
	{
		ClearRadioPacketStatus();
		nbrResponses++;
	}

	// if response (NoCommand) is received more than NBR_RESP_CONNECTION on one channel
	// the correct channel is found - go to IDLE mode
	if(nbrResponses>=NBR_RESP_CONNECTION)
	{
		currentState = ST_IDLE;
		InitIdleMode();
		return;
	}

	// RFC timeout - send the message
	if(TimerExpired( thGenTimer))
	{
		ResetTimer( thGenTimer, RFC_HUNTING_PERIOD_MS );
		ReadTemperature();
		mplSendRfcPIB();
		nbrTransmission++;
	}

	// channel cycle timeout - change the channel
	if(nbrTransmission>=UPDATE_CHANNEL_NBR_TR)
	{
		// wait until the UART finishes sending the previous packet
		while(IsZbTiSending())
		{
			ResetWatchdog();
		}
		SendHuntingMode();
		// wait for the radio to send the data over the air and get ready for the packet
		ResetTimer( thGenTimer1, RFC_RESPONSE_TIME );
		while(TimerExpired( thGenTimer1)==FALSE)
		{
			ResetWatchdog();
		}
		IncreaseZbChannel();							// increment the channel
		SetRadioChannel();								// set the new channel in the radio
		nbrTransmission = 0;
		nbrResponses = 0;
	}

	// read St Gyro data
	if(stGyroHasData())
	{
		GetSTGyroData();
	}

	// check for power down timeout
	if(SecTimerExpired( thTimerSec1))
	{
		currentState = ST_DEEP_SLEEP;
	}

}



void InitIdleMode(void)
{
//	EnableAllPower();
//	InitRadioInterface();
//	UpdateTimeParamsFromFlash();
	SendActiveMode();
	DisableAnalogGyro();
	initST();
	CompassInit();					// initialize the compass and wait 10ms
	ResetTimer( thGenTimer, 10 );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}

	MeasureStrainGaugeValueAndSetCurrent();


	SetRadioReceiverOn();

	// TO DO
	// configure accelerometer


// after version 2v06 we do not use external Vref, because they tend to break
// when subjected to temperature stress, due to the different temp.coeff between the potting material and the PCB.
// We use the ADC internal current reference

	// configure all ADCs connected to strain gauge
	for(adcNumber = ADC_USED_FIRST; adcNumber<=ADC_USED_LAST; adcNumber++)
	{

		ADS1220Reset(adcNumber);

		ADS1220ReadAllRegs(adcNumber, bDataArray,sizeof(bDataArray));
		ResetTimer( thGenTimer, ZB_TX_PERIOD );
		StartTimer( thGenTimer );
		while(!TimerExpired( thGenTimer))
		{
			ResetWatchdog();
		}
		analogChannel = ADS1220_IN_AIN1_AIN2;
		//analogChannel = ADS1220_IN_AVDD_BY_4;	// measures AVDD
		//analogChannel = ADS1220_IN_VREF;	// measures Vref

		ADS1220Configure(analogChannel,adcNumber, idacCurrent[adcNumber]);
		ResetTimer( thGenTimer, ZB_TX_PERIOD );
		StartTimer( thGenTimer );
		while(!TimerExpired( thGenTimer))
		{
			ResetWatchdog();
		}
		ADS1220ReadAllRegs(adcNumber, bDataArray,sizeof(bDataArray));
		ADS1220StartConversion(adcNumber);
	}

	// configure ADC7 as temperature sensor
	ADS1220ReadAllRegs(ADC_USED_AS_TEMP, bDataArray,sizeof(bDataArray));
	ResetTimer( thGenTimer, ZB_TX_PERIOD );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
	ADS1220ConfigAsTemperatureSensor(ADC_USED_AS_TEMP);
	ResetTimer( thGenTimer, ZB_TX_PERIOD );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
	ADS1220ReadAllRegs(ADC_USED_AS_TEMP, bDataArray,sizeof(bDataArray));
	ADS1220StartConversion(ADC_USED_AS_TEMP);

//	ADS1220Powerdown(ADC_NOT_USED);

	SendActiveMode();	// send it again just in case :)

	ResetTimer( thGenTimer, GetTimeParameter(getRfcRate) );
	ResetTimer( thGenTimer1, GetTimeParameter(getRfcRate) );
	ResetTimer( thGenTimer2, GetTimeParameter(getRfcRate)/2 );
//	ResetTimer( thGenTimer, INIT_IDLE_PERIOD );
	ResetSecTimer( thTimerSec1, GetTimeParameter(getPairTimeout) );
	ResetSecTimer( thTimerSec2, 0 );
	nbrTransmission = 0;
	batSampleCounter = SAMPLE_BATTERY_TIME_SEC;
	takeSample = 0;
	digGyroSampleCounter = 0;
	digGyroOnOff = TRUE;
	SetPinAsOutput(P9_TP39 );
	InitStrainGaugesReading();
	readTorque = FALSE;
}




//******************************************************************************
//
//  Function: IdleTask
//
//  Arguments: none
//
//  Returns: none
//
//  Description: read temperature, RPM and gas gauge. sends RFC every RFC_RATE ms.
//	Power down radio between transmissions.
//
//******************************************************************************
void IdleTask(void)
{

	ResetWatchdog();

	if(batSampleCounter>=SAMPLE_BATTERY_TIME_SEC)
	{
		batSampleCounter = 0;
		takeSample=1;
	}


	if(IsRadioActive())
	{
		if(TimerExpired( thGenTimer1))
		{
			PowerDownRadio();
			if(digGyroOnOff==FALSE)
			{
				WakeUpStGyro();
			}
			digGyroOnOff ^=1;
//			PowerDownMCU();
		}
		else
		{
			if(GetCommandStatus()==TRUE)
			{
				PowerDownRadio();
				SetNoCommand(FALSE);
				ResetSecTimer( thTimerSec1, GetTimeParameter(getPairTimeout) );
				nbrTransmission = 0;
				if(digGyroOnOff==FALSE)
				{
					WakeUpStGyro();
				}
				digGyroOnOff ^=1;
	//			PowerDownMCU();
			}
			else
			{
				if(GetRadioPacketStatus())
				{
					ResetSecTimer( thTimerSec1, GetTimeParameter(getPairTimeout) );
					ClearRadioPacketStatus();
					nbrTransmission = 0;
				}

			}

		}
	}

	// read the strain gauges and the gyro in the second half
	if(readTorque == TRUE)
	{
//		StrainGaugeReadInIdleModeV2();
		if(digGyroOnOff==TRUE)
		{
			if(stGyroHasData())
			{
				digGyroSampleCounter++;
				GetSTGyroData();
			}
		}
	}
	else
	{
		if(TimerExpired( thGenTimer2))
		{
			InitADCInRFC();
			if(takeSample)
			{
				ADS1220ConfigAsTemperatureSensor(ADC_USED_AS_TEMP);
				ADS1220StartConversion(ADC_USED_AS_TEMP);
			}

			ResetTimer( thGenTimer2, 30 );
			while(!TimerExpired( thGenTimer2))
			{
				StrainGaugeDummyRead();
				ResetWatchdog();
			}


			readTorque = TRUE;

			// start reading from PIB and sensor boards. The actual reading is done in updateMplCommState(), which needs to be called periodically.
			if(isMplCommFinish()==TRUE)
			{
				mplCommStart((BYTE)1);
			}
		}

	}



	if(TimerExpired( thGenTimer))
	{

		if(digGyroOnOff==TRUE)
		{
			PowerDownStGyro();
		}

		if(takeSample)
		{
			BatteryGasMonitorReadAllRegs();
		}

//		ResetTimer( thGenTimer1, 5 );		// wait for the strain gauges to stabilize
//		while(!TimerExpired( thGenTimer1));

		// turn on the radio
		if(IsRadioActive()==FALSE)
		{
			ResetRadio();
			ResetTimer( thGenTimer1, 3 );		// wait 3ms to wake up the radio
			while(!TimerExpired( thGenTimer1));
			SetRadioChannel();
			ResetTimer( thGenTimer1, 2 );		// wait 2ms to execute the command
			while(!TimerExpired( thGenTimer1));
		}
		else
		{
			ResetTimer( thGenTimer1, 5 );		// wait to read the gas gauge
			while(!TimerExpired( thGenTimer1));
		}

		if(takeSample)
		{
			ReadTemperature();

			if(STC3100DataReady()==TRUE)
			{
				STC3100GetRawData(bDataArray,sizeof(bDataArray));
				ParseGasGaugeData(bDataArray);
			}
			MeasureBatVoltageAdc();
		}

		PowerDownADC();

		CompassReadDataStart();
		ResetTimer( thGenTimer1, 2 );		// wait
		while(!TimerExpired( thGenTimer1));

		ResetTimer( thGenTimer1,4 );
		// wait to read the compass or timeout occurs
		while(CompassDataReady()==FALSE)
		{
			ResetWatchdog();
			if(TimerExpired( thGenTimer1))
				break;
		}

		ReadCompass();
		// TO DO - read accelerometer
//		AverageTheResults();
		mplSendRfcPIB();
		InitStrainGaugesReading();
		nbrTransmission++;
		SetNoCommand(FALSE);
		if(IsChannelChangePending()== TRUE)
		{
			ResetTimer( thGenTimer1, 5 );		// wait 5ms to for the RFC to be sent
			while(!TimerExpired( thGenTimer1));
			SetNewChannel();
		}

		ResetTimer( thGenTimer, (GetTimeParameter(getRfcRate) - IDLE_TASK_TIME_MS) );
		ResetTimer( thGenTimer1, GetTimeParameter(getRfcTimeout) );
		ResetTimer( thGenTimer2, GetTimeParameter(getRfcRate)/2 -IDLE_TASK_TIME_MS);

		batSampleCounter++;
		takeSample = 0;
		readTorque = FALSE;
	}

	// read digital gyro
	// only if it's ON
//	if(digGyroOnOff==TRUE)
//	{
//		if(stGyroHasData())
//		{
//			digGyroSampleCounter++;
//			GetSTGyroData();
//
//		}
//	}

	// check for start streaming command
	if(GetStartStreamStatus())
	{
		currentState = ST_STREAMING;
		SetStartStream(FALSE);
		InitStreamMode();
	}

	// check for power down command
	if(GetPowerDownStatus())
	{
		currentState = ST_DEEP_SLEEP;
		SetPowerDown(FALSE);
	}

	// check for communication lost timeout
	// if it doesn't receive response for 10 transmissions
	// start hunting for base radio again
	if(nbrTransmission>TRANSMISSION_TIMEOUT)
	{
		currentState = ST_INIT;
	}
}




//******************************************************************************
//
//  Function: InitStreamMode
//
//  Arguments: none
//
//  Returns: none
//
//  Description: Initialize the system for streaming mode.
//	update Jan 30 2015	- Keep all ADCs On all the time.
//
//******************************************************************************
void InitStreamMode(void)
{
	int i;
	EnableAllPower();
	EnableAnalogGyro();
	PowerDownStGyro();
	// configure compass
	CompassInit();


	ResetTimer( thGenTimer, 10 );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}

	if(IsRadioActive()==FALSE)
	{
		ResetRadio();
		ResetTimer( thGenTimer1, 4 );		// wait 4ms to wake up the radio
		while(!TimerExpired( thGenTimer1));
		SetRadioChannel();
		ResetTimer( thGenTimer1, 3 );		// wait 3ms to execute the command
		while(!TimerExpired( thGenTimer1));
//		SetRadioReceiverOff();
//		ResetTimer( thGenTimer1, 2 );		// wait 2ms to execute the command
//		while(!TimerExpired( thGenTimer1));

	}
//	else
//	{
//		SetRadioReceiverOff();
//		ResetTimer( thGenTimer1, 2 );		// wait 2ms to execute the command
//		while(!TimerExpired( thGenTimer1));
//	}


	// configure the Gyro ADC, all others ADCs are configured in IDLE mode
//	ADS1220ReadAllRegs(ADC_EPSON_GYRO, bDataArray, sizeof(bDataArray));
//	ResetTimer( thGenTimer, ZB_TX_PERIOD );
//	StartTimer( thGenTimer );
//	while(!TimerExpired( thGenTimer))
//	{
//		ResetWatchdog();
//	}

	// configure channels 0 and 1, power down ch. 2 to 6
//	InitAnalogChInStreamingMode();

	InitADCInRFC();

	analogChannel = ADS1220_IN_AIN1_AIN2;
	//analogChannel = ADS1220_IN_AVDD_BY_4;
	//analogChannel = ADS1220_IN_VREF;
	ADS1220Configure(analogChannel,ADC_EPSON_GYRO,AD1220_IDAC_CURRENT_OFF);
	ResetTimer( thGenTimer, ZB_TX_PERIOD );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
	ADS1220ReadAllRegs(ADC_EPSON_GYRO, bDataArray,sizeof(bDataArray));
	ADS1220StartConversion(ADC_EPSON_GYRO);

	//InitADCInRFC();
	// TO DO
	// configure accelerometer

	gyroSumAverage = 0; // ?
	firstGyroSample=1;

	// turn on the reference voltage
	//TurnOnAllAdcRef();



	 //at power up the analog gyro output VOUT_GZ overshoots for 2.5ms
	// wait 4ms to stabilize the gyro
	ResetTimer( thGenTimer1, 60 );
	while(!TimerExpired( thGenTimer1))
	{
		if(ADS1220HasResult(ADC_EPSON_GYRO))
			ADS1220ReadVoltageCont(ADC_EPSON_GYRO, &uvResultTemp[ADC_EPSON_GYRO]);
	}


	ResetTimer( thGenTimer, GetTimeParameter(getStreamRate) );
	ResetSecTimer( thTimerSec1, GetTimeParameter(getStreamTimeout ) );
	ResetTimer( thGenTimer1, GetTimeParameter(getStreamRate) /2);
	ResetTimer( thGenTimer2, TENSION_SG_OFF_PERIOD );
	StartTimer( thGenTimer );
	StartTimer( thGenTimer1 );
	StartTimer( thGenTimer2 );
	StartSecTimer( thTimerSec1 );
	readTorque = FALSE;
	readTension = FALSE;
	tensionChOn = FALSE;

	SetStopStream(FALSE); // reset the flag

//	SetRadioReceiverOff();
	radioRxOn=FALSE;
	wGyroReadCounter = 0;

	ADS1220Powerdown(ADC_SWITCH_IN);	// disable ADC not used in Stream mode

	InitStrainGaugesReading();
	// read the SG a few times before sending the first data
	i=64;
	while(i--)
	{
		StrainGaugeRead();
	}
}

//******************************************************************************
//
//  Function: StreamingTask
//
//  Arguments: none
//
//  Returns: none
//
//  Description: Send the Streaming Data every 10 ms
//	update Jan 30 2015	- Keep all ADCs On all the time. Keep toggling Radio receiver On/Off
//	update Feb 02 2015	- Keep the Radio always On.
//  update Feb 04 2015  - use moving average - StrainGaugeRead();
//******************************************************************************
void StreamingTask(void)
{

	if(TimerExpired( thGenTimer))
	{
		ReadCompass();
		ResetTimer( thGenTimer, GetTimeParameter(getStreamRate) );
		ResetTimer( thGenTimer1, GetTimeParameter(getStreamRate)/2 );
		readCompass = TRUE;
		readAccelerometer = TRUE;
		readTorque = FALSE;
//		AverageTheResults();
		SendStreamData();
//		InitStrainGaugesReading();
//		wGyroReadCounter = 0;
	}

	if(GetStartStreamStatus())
	{
		SetStartStream(FALSE);
		ResetSecTimer( thTimerSec1, GetTimeParameter(getStreamTimeout ) );
	}

	StrainGaugeRead();
	EpsonGyroRead();

//	StrainGaugeReadInIdleModeV2();	// read the ADCs on a rotational basis


	if(readCompass)
	{
		if(TimerExpired( thGenTimer1))
		{
			CompassReadDataStart();
			readCompass = FALSE;
		}
	}


	if(SecTimerExpired( thTimerSec1))
	{
		currentState = ST_IDLE;
		InitIdleMode();
	}

	if(GetStopStreamStatus())
	{
		currentState = ST_IDLE;
		SetStopStream(FALSE);
		InitIdleMode();
	}

}

//void StreamingTask(void)
//{
//static BOOL readCompass;
//static BOOL readAccelerometer;
//
//	if(TimerExpired( thGenTimer))
//	{
//		ReadCompass();
//		ResetTimer( thGenTimer, GetTimeParameter(getStreamRate) );
//		ResetTimer( thGenTimer1, GetTimeParameter(getStreamRate)/2 );
//		readCompass = TRUE;
//		readAccelerometer = TRUE;
//		SendStreamData();
//	}
//
//	if(GetStartStreamStatus())
//	{
//		SetStartStream(FALSE);
//		ResetSecTimer( thTimerSec1, GetTimeParameter(getStreamTimeout ) );
//	}
//
//	StrainGaugeRead();
//	EpsonGyroRead();
//
//	if(readCompass)
//	{
//		if(TimerExpired( thGenTimer1))
//		{
//			CompassReadDataStart();
//			readCompass = FALSE;
//		}
//	}
//
//	if(SecTimerExpired( thTimerSec1))
//	{
//		currentState = ST_IDLE;
//		InitIdleMode();
//	}
//
//	if(GetStopStreamStatus())
//	{
//		currentState = ST_IDLE;
//		SetStopStream(FALSE);
//		InitIdleMode();
//	}
//
//}




BOOL ReadCompass(void)
{
	if(CompassDataReady()==TRUE)
	{
		CompassGetRawData(bDataArray,sizeof(bDataArray));
		sMagnetData.iXField[0] = bDataArray[0];
		sMagnetData.iXField[0] = (sMagnetData.iXField[0]<<8)|bDataArray[1];
		sMagnetData.iZField[0] = bDataArray[2];
		sMagnetData.iZField[0] = (sMagnetData.iZField[0]<<8)|bDataArray[3];
		sMagnetData.iYField[0] = bDataArray[4];
		sMagnetData.iYField[0] = (sMagnetData.iYField[0]<<8)|bDataArray[5];
		return TRUE;
	}
	return FALSE;
}


void PowerDownTask(void)
{
	SystemPowerDown();
}


void InitGasGauge(void)
{
#if GAS_GAUGE_ENABLE
	// 	enable 3V3_D. Otherwise it keeps the I2C lines low.
	ResetTimer( thGenTimer, 1000 );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
	ResetTimer( thGenTimer, 1000 );
	BatteryGasMonitorReadAllRegs();
	while(STC3100DataReady()==FALSE)
	{
		ResetWatchdog();
		if(TimerExpired( thGenTimer))
			break;
	}
	STC3100GetRawData(bDataArray,sizeof(bDataArray));
	ResetTimer( thGenTimer, 1000 );
	BatteryGasMonitorReadId();
	while(STC3100DataReady()==FALSE)
	{
		ResetWatchdog();
		if(TimerExpired( thGenTimer))
			break;
	}

	STC3100GetRawData(bDataArray,sizeof(bDataArray));
	//InitBatteryGasMonitor();
	ResetTimer( thGenTimer, 1000 );
	StartTimer( thGenTimer );
	ResetBatteryGasMonitor();
	//	while(STC3100TxDone()==FALSE)
	//	{
	//		ResetWatchdog();
	//	}
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
	ResetTimer( thGenTimer, 1000 );
	BatteryGasMonitorReadAllRegs();
	while(STC3100DataReady()==FALSE)
	{
		ResetWatchdog();
		if(TimerExpired( thGenTimer))
			break;
	}
	STC3100GetRawData(bDataArray,sizeof(bDataArray));
#endif
}

//******************************************************************************
// Function name:    TurnOnAllAdcRef
//******************************************************************************
// Description:      Connect all Vrefs to StrainGauge Connectors
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void TurnOnAllAdcRef(void)
{
	// turn on the reference voltage
	for(adcNumber =ADC_USED_FIRST; adcNumber<=ADC_USED_LAST; adcNumber++)
	{
		ADS1220TurnOnReference(adcNumber);
	}
}

//******************************************************************************
// Function name:    TurnOffAllAdcRef
//******************************************************************************
// Description:      Disconnect all Vrefs from StrainGauge Connectors
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void TurnOffAllAdcRef(void)
{
	for(adcNumber =ADC_USED_FIRST; adcNumber<=ADC_USED_LAST; adcNumber++)
	{
		ADS1220TurnOffReference(adcNumber);
	}
}



void InitAccelerometer(void)
{
#if ACCELEROMETER_ENABLE
	AccelInit();
	while(AccelTxDone()==FALSE)
	{
		ResetWatchdog();
	}
#endif
}

BOOL ReadAccelerometer(void)
{
	if(AccelDataReady()==TRUE)
	{
		AccelGetRawData(bDataArray,sizeof(bDataArray));
		sAccelData.iXField[0] = bDataArray[0];
		sAccelData.iXField[0] = (sAccelData.iXField[0]<<8)|bDataArray[1];
		sAccelData.iYField[0] = bDataArray[2];
		sAccelData.iYField[0] = (sAccelData.iYField[0]<<8)|bDataArray[3];
		sAccelData.iZField[0] = bDataArray[4];
		sAccelData.iZField[0] = (sAccelData.iZField[0]<<8)|bDataArray[5];
		return TRUE;
	}
	return FALSE;
}

//******************************************************************************
// Function name:    InitADCInRFC
//******************************************************************************
// Description:      Configure All ADCs for RFC mode:
//					ADC 0 to 6 (connected to strain gauges)- differential input AIN1 AIN2
//					ADC 7 (connected to Analog Gyro)- as temperature sensor
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void InitADCInRFC(void)
{
	// turn on the ADC CLK
	P11SEL |= BIT0;
	// configure all ADCs connected to strain gauge
	for(adcNumber =ADC_USED_FIRST; adcNumber<=ADC_USED_LAST; adcNumber++)
	{

		analogChannel = ADS1220_IN_AIN1_AIN2;
		//analogChannel = ADS1220_IN_AVDD_BY_4;
		//analogChannel = ADS1220_IN_VREF;
		ADS1220Configure(analogChannel,adcNumber, idacCurrent[adcNumber]);
		ADS1220StartConversion(adcNumber);
	}

}

//******************************************************************************
// Function name:    PowerDownADC
//******************************************************************************
// Description:      Send a power down command to all ADCs.
//					 Turn off the ADC CLK
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void PowerDownADC(void)
{
	for(adcNumber =ADC_USED_FIRST; adcNumber<ADS1220_TOTAL; adcNumber++)
	{

		ADS1220Powerdown(adcNumber);
	}

	P11SEL &= ~BIT0;
}

void ReadSerNumber(void)
{
	BYTE crcSnCalc;
	ResetTimer( thGenTimer, 10 );
	StartTimer( thGenTimer );


	SerNumberReadDataStart();
	while(SerNumberDataReady()==FALSE)
	{
		ResetWatchdog();
		if(TimerExpired( thGenTimer))
			break;
	}

	SerNumberGetRawData(bDataArray,sizeof(bDataArray));
	crcSnCalc=CalcSnCRC(bDataArray, 7);
	SaveSerNumber(bDataArray);


}


//******************************************************************************
//
//  Function: InitAnalogChInStreamingMode
//
//  Arguments:	none
//
//  Returns: none
//
//  Description: initialize the torque SG (channels 0 and 1), and disables the tension SG (channels 2 to 6)
//
//******************************************************************************
void InitAnalogChInStreamingMode(void)
{
	// turn on the ADC CLK
	P11SEL |= BIT0;
	analogChannel = ADS1220_IN_AIN1_AIN2;
	//analogChannel = ADS1220_IN_AVDD_BY_4;
	//analogChannel = ADS1220_IN_VREF;	// measures Vref
	for(adcNumber =ADC_USED_FIRST; adcNumber<=ADC_USED_LAST; adcNumber++)
	{

		if(adcNumber<2)
		{
			ADS1220Configure(analogChannel,adcNumber, idacCurrent[adcNumber]);
			ADS1220StartConversion(adcNumber);
			//ADS1220TurnOnReference(adcNumber);
		}
		else
		{
			ADS1220Powerdown(adcNumber);
			//ADS1220TurnOffReference(adcNumber);
		}
	}
}



//******************************************************************************
// Function name:    DummyReadTensionGaugesRotation
//******************************************************************************
// Description:      dummy read only Tension strain gauges ADC one at a time
//					 This includes channels 2, 3, 4 and 5
//					 Used in the first few ms after the ADC has been turned on to remove the first bad readings.
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void DummyReadTensionGaugesRotation(void)
{
	static WORD adcNbrTensionDummy = ADC_TENSION_CH0;


	if(ADS1220HasResult(adcNbrTensionDummy))
	{
		ADS1220ReadVoltageCont(adcNbrTensionDummy, &uvResultTemp[adcNbrTensionDummy]);
	}

	switch(adcNbrTensionDummy)
	{
		case ADC_TENSION_CH0:
			adcNbrTensionDummy = ADC_TENSION_CH1;
			break;
		case ADC_TENSION_CH1:
			adcNbrTensionDummy = ADC_TENSION_CH2;
			break;
		case ADC_TENSION_CH2:
			adcNbrTensionDummy = ADC_TENSION_CH3;
			break;
		case ADC_TENSION_CH3:
			adcNbrTensionDummy = ADC_TENSION_CH0;
			break;
		default:
			adcNbrTensionDummy = ADC_TENSION_CH0;
			break;
	}

}



//******************************************************************************
// Function name:    PowerDownMCU
//******************************************************************************
// Description:      power down MCU for a IDLE_SLEEP_TIME
//
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void PowerDownMCU(void)
{
	StopWatchdog();
//	SetupLPMode((WORD) IDLE_SLEEP_TIME);
	SetupLPMode( GetTimeParameter(getRfcRate)/2 - IDLE_TASK_TIME_MS);
	__bis_SR_register(LPM0_bits + GIE);       // Enter LPM0 w/interrupt
	__no_operation();                         // For debugger
	DisableLPMode();
	SetWatchdog();
}



//******************************************************************************
// Function name:    MeasureStrainGaugeValue
//******************************************************************************
// Description:     measure the Strain gauge value and set the IDAC current based on the value.
//					For now the SG are either 5K or 1K.
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void MeasureStrainGaugeValueAndSetCurrent(void)
{
	static UVOLTS tmpVar;
	// configure all ADCs connected to strain gauge

	for(adcNumber = ADC_USED_FIRST; adcNumber<=ADC_USED_LAST; adcNumber++)
	{
		ADS1220Reset(adcNumber);
		analogChannel = ADS1220_IN_AIN0_AIN3;
		ADS1220ConfigureInSGTestMode(analogChannel,adcNumber, idacCurrent250ua);
		ResetTimer( thGenTimer, ZB_TX_PERIOD );
		StartTimer( thGenTimer );
		while(!TimerExpired( thGenTimer))
		{
			ResetWatchdog();
		}
		ADS1220ReadAllRegs(adcNumber, bDataArray,sizeof(bDataArray));
		ADS1220StartConversion(adcNumber);
	}

	// read the SG for 1000 ms
	ResetTimer( thGenTimer, TIME_1000MS );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
		StrainGaugeReadInIdleModeV1();
	}


	for(adcNumber = ADC_USED_FIRST; adcNumber<=ADC_USED_LAST; adcNumber++)
	{
		if(uvResultAverage[adcNumber]>SG_DIVIDER_VALUE)
		{
			idacCurrent[adcNumber] = idacCurrent250ua;	// 5K Strain Gauge
		}
		else
		{
			idacCurrent[adcNumber] = idacCurrent1500ua; // 1K Strain Gauge
		}

//		// there will be current to voltage converter board on the tension strain gauges.
//		// set the current to 1.5mA to power the converter
		switch(adcNumber)
		{
			case ADC_TENSION_000:
				idacCurrent[adcNumber] = idacCurrent1500ua;
			break;
			case ADC_TENSION_090:
				idacCurrent[adcNumber] = idacCurrent1500ua;
			break;
			case ADC_TENSION_180:
				idacCurrent[adcNumber] = idacCurrent1500ua;
			break;
			case ADC_TENSION_270:
				idacCurrent[adcNumber] = idacCurrent1500ua;
			break;
			default:
				break;
		}
	}

	idacCurrent[ADC_SWITCH_IN] = idacCurrent500ua;	// temperature sensor will be attached to this channel. Fix the current to 500uA.


}























/************************************
 ******* TEST FUNCTIONS**************
************************************/

#ifdef TEST_FUNCTIONS

void TestPowerDownMode(void)
{
	initMPS430Pins();
	EnableAllPower();
	Set_System_Clock();
	InitTimers();
	SysTimerConfig();
	__bis_SR_register(GIE);
	ReedSwitchInit();
	SetOutputPin(SW_LED, TRUE );
	InitRadioInterface();
	thGenTimer = RegisterTimer();
	ResetTimer( thGenTimer, 3000 );
	StartTimer( thGenTimer );
	while(1)
	{
		if(IsSwitchOn())
		{
	//		if(TimerExpired( thGenTimer))
	//		{
	//			SetSystemDeepSleep();
	//			initMPS430Pins();
	//			SetOutputPin(LED_CONTROL, FALSE );
	//			SetOutputPin(SW_LED, TRUE );
	//			Set_System_Clock();
	//			SysTimerConfig();
	//			ResetTimer( thGenTimer, 3000 );
	//			StartTimer( thGenTimer );
	//		}
		}
		else
		{
			if(TimerExpired( thGenTimer))
			{
				//DisableAllPower();
				DisableAnalogPower();
				DisableDigitalPower();
	//			DisableRadioPower();
				PowerDownRadio();
				DisableAnalogGyro();
				SetOutputPin(SW_LED, FALSE );

				//			EnableDigitalPower();
				SetSystemDeepSleep();
				initMPS430Pins();
				EnableAllPower();
				SetPinAsOutput(ZB_RSTn);
				SetOutputPin(ZB_RSTn, FALSE );

				SetOutputPin(SW_LED, TRUE );

				Set_System_Clock();
				InitTimers();
				SysTimerConfig();
				__bis_SR_register(GIE);
				ReedSwitchInit();
				InitRadioInterface();
				ResetTimer( thGenTimer, 3000 );
				StartTimer( thGenTimer );
				SetOutputPin(ZB_RSTn, TRUE );
			}

		}
	}
}



void TestAccelerometer(void)
{
	EnableAllPower();
	__delay_cycles(90000);
	InitAccelerometer();
	__delay_cycles(90000);
	AccelRegisterDump();
	while(AccelDataReady()==FALSE)
	{
		ResetWatchdog();
	}
	AccelGetRawData(bDataArray,sizeof(bDataArray));
	while(1)
	{
		AccelReadDataStart();
		while(AccelDataReady()==FALSE)
		{
			ResetWatchdog();
		}
		ReadAccelerometer();
		__delay_cycles(90000);
		ResetWatchdog();
	}


}



void TestGasGauge(void)
{
#if GAS_GAUGE_ENABLE
	ResetTimer( thGenTimer, 1000 );
	StartTimer( thGenTimer );
	BatteryGasMonitorReadAllRegs();
	while(STC3100DataReady()==FALSE);
	STC3100GetRawData(bDataArray,sizeof(bDataArray));
	BatteryGasMonitorReadId();
	while(STC3100DataReady()==FALSE)
	{
		ResetWatchdog();
	}
	STC3100GetRawData(bDataArray,sizeof(bDataArray));
	//InitBatteryGasMonitor();
	ResetBatteryGasMonitor();
	while(STC3100TxDone()==FALSE)
	{
		ResetWatchdog();
	}
	BatteryGasMonitorReadAllRegs();
	while(STC3100DataReady()==FALSE)
	{
		ResetWatchdog();
	}
	STC3100GetRawData(bDataArray,sizeof(bDataArray));

	while(1)
	{
		if(TimerExpired( thGenTimer))
		{
			BatteryGasMonitorReadAllRegs();
			while(STC3100DataReady()==FALSE)
			{
				ResetWatchdog();
			}
			STC3100GetRawData(bDataArray,sizeof(bDataArray));
			ParseGasGaugeData(bDataArray);
			ResetTimer( thGenTimer, 1000 );
			StartTimer( thGenTimer );
		}
		ResetWatchdog();
	}

#endif
}







void TestCompass(void)
{
	// test compass
	#if COMPASS_ENABLE
		ResetTimer( thGenTimer, 10 );
		StartTimer( thGenTimer );

		while(1)
		{
			if(TimerExpired( thGenTimer))
			{
				// reading data from the compass takes about 920us
//				SetOutputPin(P8_TP7, TRUE);
				CompassReadDataStart();
				while(CompassDataReady()==FALSE)
				{
					ResetWatchdog();
				}
				CompassGetRawData(bDataArray,sizeof(bDataArray));
				sMagnetData.iXField[0] = bDataArray[0];
				sMagnetData.iXField[0] = (sMagnetData.iXField[0]<<8)|bDataArray[1];
				sMagnetData.iZField[0] = bDataArray[2];
				sMagnetData.iZField[0] = (sMagnetData.iZField[0]<<8)|bDataArray[3];
				sMagnetData.iYField[0] = bDataArray[4];
				sMagnetData.iYField[0] = (sMagnetData.iYField[0]<<8)|bDataArray[5];
//				SetOutputPin(P8_TP7, FALSE);
				ResetTimer( thGenTimer, 10 );
				StartTimer( thGenTimer );
			}
			ResetWatchdog();

		}
	#endif
	// end test compass
}




void TestStGyro(void)
{
	int gyroData[30];
	WORD timeBtwSamples[30];
	WORD timesToStable[30];
	unsigned int i,j,k;
	BOOL gyroStable;

	while(1)
	{
		for(i=0;i<30;i++)
		{

			PowerDownStGyro();
			__delay_cycles(40000);
			__delay_cycles(40000);

			SetSystemUTCTime( (DWORD) 0 );

			setSTreg1();
			setSTreg3();
			gyroStable = FALSE;
			j=0;
			k=0;
			while(k<5)
			{
				while(stGyroHasData()==FALSE);

				gyroStable = !stGyroReadData( &gyroData[i]);
				j++;
				if(gyroStable==TRUE)
					k++;
				else
					k=0;
			}
			timesToStable[i] = j;
			while(stGyroHasData()==FALSE);
			stGyroReadData( &gyroData[i]);

			timeBtwSamples[i] = (WORD)GetSystemUTCTime();

		}

		ResetWatchdog();


	}

}


void TestSerNumber(void)
{
	BYTE crcSnCalc;
	ResetTimer( thGenTimer, 10 );
	StartTimer( thGenTimer );

	while(1)
	{
		if(TimerExpired( thGenTimer))
		{
			SerNumberReadDataStart();
			while(SerNumberDataReady()==FALSE)
			{
				ResetWatchdog();
			}

			SerNumberGetRawData(bDataArray,sizeof(bDataArray));
			crcSnCalc=CalcSnCRC(bDataArray, 7);
			if(crcSnCalc == bDataArray[7])
			{
				SaveSerNumber(bDataArray);
			}
			ResetTimer( thGenTimer, 10 );
			StartTimer( thGenTimer );
		}
	}
}



//******************************************************************************
//
//  Function: TestSensorReadings
//
//  Arguments: void
//
//  Returns: WORD number of bad sensors.
//
//  Description: read all sensors 10 times, toggle the LDO if there is non responsive sensor,
//				and return the number of non responsive sensors after the last reading cycle
//
//******************************************************************************
void TestSensorReadings(void)
{
	WORD i,j;
	WORD sensorsNotSending;
	WORD wCounterR;
	j=10;
	sensorsNotSending = 0;
	EnableExtPower();
	SetOutputPin(RS485_PWR_EN, FALSE);
	UARTA3Init();
	UARTA3RxEnable();

	while(1)
	{
		// toggle the Sensor LDO - if a board doesn't send data
		j=0;
		if(sensorsNotSending>0)
		{
			sensorsNotSending = 0;
			SetOutputPin(RS485_PWR_EN, TRUE);
			__delay_cycles(16000);
			SetOutputPin(RS485_PWR_EN, FALSE);
		}

		ResetTimer( thGenTimer, ZB_TX_PERIOD );
		while(TimerExpired( thGenTimer)==FALSE)
		{
			ResetWatchdog();
		}
		SetOutputPin(EXT_DEV_EN, TRUE);
		SetOutputPin(EXT_DEV_ADD, FALSE);
		ResetTimer( thGenTimer, ZB_TX_PERIOD );
		while(TimerExpired( thGenTimer)==FALSE)
		{
			ResetWatchdog();
		}
		SetOutputPin(EXT_DEV_EN, FALSE);
		ResetTimer( thGenTimer, ZB_TX_PERIOD );
		while(TimerExpired( thGenTimer)==FALSE)
		{
			ResetWatchdog();
		}
		for(i=0;i<4;i++)
		{
			SetOutputPin(EXT_DEV_ADD, TRUE);
			__delay_cycles(40000);	//	wait for 12B to transfer - 1.15mS, 12B at 125Kbps will take 0.96mS		- 2ms
			SetOutputPin(EXT_DEV_ADD, FALSE);
			__delay_cycles(1600);	// add 0.2mS guard band

			wCounterR = ReadUartA3Data(pUartRxData);
			if(wCounterR==12)
			{
				j++;
				wCounterR=0;
				sensorsNotSending++;
			}
			else
			{
				wCounterR=1;
				sensorsNotSending++;
			}

			ResetWatchdog();
		}

	}


}

void TestMcuAdc(void)
{


	while(1)
	{
		__delay_cycles(80000);
		MeasureBatVoltageAdc();
	}
}

#endif

//******************************************************************************
//
// PIBProtocolUtils.c: PIB Communication protocol routines
//
//  Copyright (c) 2016, Microlynx Systems Ltd.
//  ALL RIGHTS RESERVED
//
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2016-Oct-04     DD        Initial Version
//
//******************************************************************************

#include "PIBProtocolUtils.h"

//#pragma package(smart_init)


//
// Private Declarations
//

#define DEFAULT_SOL_CTRL		0x00
#define DEFAULT_CONT_CTRL		0x00
#define DEFAULT_SENS_PWR_CTRL	0x01
#define DEFAULT_SOL_TIMEOUT		10		// seconds


#define PIB_NBR_RSP				4   // Total number of PIB RESP_... defines
#define RS485_PKT_NBR			64

#define MPL_COMM_TIMEOUT		500	//ms
static TIMERHANDLE thPibTimer;

typedef struct {
    BYTE pktPibHdr;        // Expected header byte
    BYTE cmdRespByte;   // One of the WTTTS_CMD_... or WTTTS_RESP_ defines
    BYTE expDataLen;    // Required value for header pktLen param
} PIB_PKT_INFO;

static const PIB_PKT_INFO m_pktInfo[ PIB_NBR_RSP] = {
	{ PIB_ORIG_HDR, PIB_RSP_SET_PARAMS, sizeof(PIB_SET_PARAM_RSP_PKT) },
    { PIB_ORIG_HDR, PIB_RSP_GET_PARAMS, sizeof(PIB_GET_PARAM_RSP_PKT) },
	{ PTS_ORIG_HDR, PTS_RSP_SET_PARAMS, sizeof(PTS_GET_PARAM_RSP_PKT) },
    { PTS_ORIG_HDR, PTS_RSP_GET_PARAMS, sizeof(PTS_GET_PARAM_RSP_PKT) },
};


static BYTE rs485Packet[RS485_PKT_NBR];
static BOOL  havePIBPkt = FALSE;
static WORD tmpWord;

static PIB_PKT_HDR* pktPibHdr;
static BYTE* pktPibData;
static PIB_SET_PARAM_CMD_PKT* pPibParamCmd;
static PIB_GET_PARAM_RSP_PKT* pPibParamRsp;
static PTS_SET_PARAM_CMD_PKT* pPtsParamCmd;
static PTS_GET_PARAM_RSP_PKT* pPtsParamRsp;

static PIB_SET_SOLENOID_CMD_PKT* pPibSolCmd;
static PIB_SET_CONTACT_CMD_PKT*  pPibContCmd;
static PIB_SET_SOL_TIMEOUT_CMD_PKT*  pPibSolTimeCmd;
static PIB_RESET_INPUTS_CMD_PKT* pPibResInputCmd;

static PIB_SET_PARAM_CMD_PKT sPibParamCmd;
static PIB_GET_PARAM_RSP_PKT sPibParamRsp;
static PTS_SET_PARAM_CMD_PKT sPtsParamCmd;
static PTS_GET_PARAM_RSP_PKT sPtsParamRsp;

static BYTE* pPibCRC;

static BYTE currentSolCtrl;		// activate/deactivate PIB solenoids
static BYTE currentContCtrl;	// activate/deactivate PIB contacts
static BYTE currentSensPwrCtrl;	// turn ON/OFF power to the sensor boards
static WORD currentSolTimeout;	// maximum time a solenoid can be in active state, measured in seconds.

const WTTTS_RATE_TYPE getSolTimeout = SRT_SOLENOID_TIMEOUT;
const BYTE PTS_ADDRESS[NBR_PTS_BOARDS]={1,2,3,4};	//	PTS: address range of 1 through 16.


static BYTE pibCommBuff[4*sizeof(PTS_GET_PARAM_RSP_PKT)];
typedef enum{
	MPL_COMM_START,
	MPL_GET_PIB_PARAMS,
	MPL_READ_PIB_PARAMS,
	MPL_GET_PTS_PARAMS,
	MPL_READ_PTS_PARAMS,
	MPL_COMM_IDLE,
	NBR_MPL_COMM_STATES
}MPL_COMM_STATE;

MPL_COMM_STATE	mplCommState;

static BYTE currentPtsBrd;
static BYTE totalToReadPtsBrd;

void parsePIBRsp(void);
void parsePTSRsp(void);

void InitPIBInterface(void)
{

	InitRs485Interface();
	currentSolCtrl = DEFAULT_SOL_CTRL;
	currentContCtrl = DEFAULT_CONT_CTRL;
	currentSensPwrCtrl = DEFAULT_SENS_PWR_CTRL;
	currentSolTimeout = GetTimeParameter(getSolTimeout);
	currentPtsBrd = 0;

	thPibTimer = RegisterTimer();
	StartTimer( thPibTimer );
	mlpCommStateInit();
}



//******************************************************************************
//
//  Function: GetPibParam
//
//  Arguments: none
//
//  Returns: none
//
//  Description: send command "Get PIB Parameters". It's zero data length command.
//
//******************************************************************************
void GetPibParam(void)
{
	// Initialise the pointers
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+0];

	pktPibHdr->pktHdr = TEC_ORIG_HDR;
	pktPibHdr->pktType = PIB_CMD_GET_PARAMS;
	pktPibHdr->slaveAddr = PIB_ADDRESS;
	pktPibHdr->dataLen = 0;

	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+0);

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+0+1);

}

//******************************************************************************
//
//  Function: SetPibParam
//
//  Arguments: 	BYTE solCtrl	- activate/deactivate PIB solenoids
//			 	BYTE contCtrl	- activate/deactivate PIB contacts
//				BYTE sensPwrCtrl - turn ON/OFF power to the sensor boards
//				WORD solTimeout	 - maximum time a solenoid can be in active state, measured in seconds.
//
//  Returns: none
//
//  Description: send command "Set PIB Parameters".
//
//******************************************************************************
void SetPibParam(BYTE solCtrl, BYTE contCtrl, BYTE sensPwrCtrl, WORD solTimeout)
{
	// Initialise the pointers
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPibParamCmd = (PIB_SET_PARAM_CMD_PKT*)&rs485Packet[sizeof(PIB_PKT_HDR)];
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+sizeof(PIB_SET_PARAM_CMD_PKT)];

	pktPibHdr->pktHdr = TEC_ORIG_HDR;
	pktPibHdr->pktType = PIB_CMD_SET_PARAMS;
	pktPibHdr->slaveAddr = PIB_ADDRESS;
	pktPibHdr->dataLen = sizeof( PIB_SET_PARAM_CMD_PKT );

	pPibParamCmd->solenoidCtrl = solCtrl;
	pPibParamCmd->contactCtrl = contCtrl ;
	pPibParamCmd->sensorsPwrCtrl = sensPwrCtrl;
	pPibParamCmd->solenoidTimeout = solTimeout;
	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+sizeof(PIB_SET_PARAM_CMD_PKT));

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+sizeof(PIB_SET_PARAM_CMD_PKT)+1);

}

//******************************************************************************
//
//  Function: SetPibSolenoid
//
//  Arguments: 	BYTE solCtrl	- activate/deactivate PIB solenoids
//
//  Returns: none
//
//  Description: send command "SetPibSolenoid".
//
//******************************************************************************
void SetPibSolenoid(BYTE solCtrl)
{
	// Initialise the pointers
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPibSolCmd = (PIB_SET_SOLENOID_CMD_PKT*)&rs485Packet[sizeof(PIB_PKT_HDR)];;
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+sizeof(PIB_SET_SOLENOID_CMD_PKT)];

	pktPibHdr->pktHdr = TEC_ORIG_HDR;
	pktPibHdr->pktType = PIB_CMD_SET_SOLENOID;
	pktPibHdr->slaveAddr = PIB_ADDRESS;
	pktPibHdr->dataLen = sizeof( PIB_SET_SOLENOID_CMD_PKT );

	pPibSolCmd->solenoidCtrl = solCtrl;
	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+sizeof(PIB_SET_SOLENOID_CMD_PKT));

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+sizeof(PIB_SET_SOLENOID_CMD_PKT)+1);

}


//******************************************************************************
//
//  Function: SetPibContacts
//
//  Arguments: 	BYTE contCtrl	- activate/deactivate PIB contacts
//
//  Returns: none
//
//  Description: send command "SetPibContacts".
//
//******************************************************************************
void SetPibContacts(BYTE contCtrl)
{
	// Initialise the pointers
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPibContCmd = (PIB_SET_CONTACT_CMD_PKT*)&rs485Packet[sizeof(PIB_PKT_HDR)];;
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+sizeof(PIB_SET_CONTACT_CMD_PKT)];

	pktPibHdr->pktHdr = TEC_ORIG_HDR;
	pktPibHdr->pktType = PIB_CMD_SET_CONTACT;
	pktPibHdr->slaveAddr = PIB_ADDRESS;
	pktPibHdr->dataLen = sizeof( PIB_SET_CONTACT_CMD_PKT );

	pPibContCmd->contactCtrl = contCtrl;
	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+sizeof(PIB_SET_CONTACT_CMD_PKT));

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+sizeof(PIB_SET_CONTACT_CMD_PKT)+1);

}

//******************************************************************************
//
//  Function: SetPibSolTimeout
//
//  Arguments: 	WORD solTimeout	- solenoid active timeout
//
//  Returns: none
//
//  Description: send command "SetPibSolTimeout".
//
//******************************************************************************
void SetPibSolTimeout(WORD solTimeout)
{
	// Initialise the pointers
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPibSolTimeCmd = (PIB_SET_SOL_TIMEOUT_CMD_PKT*)&rs485Packet[sizeof(PIB_PKT_HDR)];;
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+sizeof(PIB_SET_SOL_TIMEOUT_CMD_PKT)];

	pktPibHdr->pktHdr = TEC_ORIG_HDR;
	pktPibHdr->pktType = PIB_CMD_SET_SOL_TIMEOUT;
	pktPibHdr->slaveAddr = PIB_ADDRESS;
	pktPibHdr->dataLen = sizeof( PIB_SET_SOL_TIMEOUT_CMD_PKT );

	pPibSolTimeCmd->solenoidTimeout = currentSolTimeout;
	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+sizeof(PIB_SET_SOL_TIMEOUT_CMD_PKT));

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+sizeof(PIB_SET_SOL_TIMEOUT_CMD_PKT)+1);

}


//******************************************************************************
//
//  Function: SetPtsParam
//
//  Arguments: 	BYTE ptsAddress	- sensor board address
//			 	BYTE mgtSampleRate - magnetometer sample rate
//				BYTE avgFactor - averaging factor
//				BYTE resetM	 - reset min/max values
//
//  Returns: none
//
//  Description: send command "Set PTS Parameters".
//
//******************************************************************************
void SetPtsParam(BYTE ptsAddress, BYTE mgtSampleRate, BYTE avgFactor, BYTE resetM)
{

	// Initialise the pointers
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPtsParamCmd = (PTS_SET_PARAM_CMD_PKT*)&rs485Packet[sizeof(PIB_PKT_HDR)];
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+sizeof(PTS_SET_PARAM_CMD_PKT)];

	pktPibHdr->pktHdr = TEC_ORIG_HDR;
	pktPibHdr->pktType = PTS_CMD_SET_PARAMS;
	pktPibHdr->slaveAddr = ptsAddress;
	pktPibHdr->dataLen = sizeof( PTS_SET_PARAM_CMD_PKT );


	pPtsParamCmd->magnetSampleRate = mgtSampleRate;
	pPtsParamCmd->averagingFactor = avgFactor;
	pPtsParamCmd->resetMinMax = resetM;

	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+sizeof(PTS_SET_PARAM_CMD_PKT));

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+sizeof(PTS_SET_PARAM_CMD_PKT)+1);
}

//******************************************************************************
//
//  Function: GetPtsParam
//
//  Arguments: 	BYTE ptsAddress	- sensor board address
//
//  Returns: none
//
//  Description: send command "Get PTS Parameters". It's zero data length command.
//
//******************************************************************************
void GetPtsParam(BYTE ptsAddress)
{

	// Initialise the pointers
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+0];

	pktPibHdr->pktHdr = TEC_ORIG_HDR;
	pktPibHdr->pktType = PTS_CMD_GET_PARAMS;
	pktPibHdr->slaveAddr = ptsAddress;
	pktPibHdr->dataLen = 0;

	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+0);

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+0+1);
}

//******************************************************************************
//
//  Function: HavePIBPacket
//
//  Arguments: 	BYTE *pBuff - pointer to incomming data buffer
//				WORD buffLen - length of the data buffer to check.
//  Returns: BOOL - TRUE if a valid packet is found, FALSE otherwise
//
//  Description: examine the incoming data for valid data packet.
//
//******************************************************************************
BOOL HavePIBPacket(BYTE *pBuff, WORD buffLen)
{
	// Scans the passed buffer for a PIB command or response. Returns true if
	// a packet is found, in which case pktPibHdr and pktPibData pointers are asigned.
	// Returns false if no packet is found.
    WORD bytesSkipped = 0;

    BOOL foundInfoItem = FALSE;
    int  dataLen       = 0;
    int  expPktLen     = 0;
    unsigned int iInfoItem = 0;
    BYTE expCRC;

    havePIBPkt = FALSE;

    if( buffLen < (WORD)MIN_PIB_PKT_LEN )
        return FALSE;

    while( bytesSkipped + (WORD)MIN_PIB_PKT_LEN <= buffLen )
       {
    		ResetWatchdog();
           // Have enough bytes for a packet. Header byte must be the first
           // byte we see
           if(( pBuff[bytesSkipped] != PIB_ORIG_HDR )&&(pBuff[bytesSkipped] != PTS_ORIG_HDR ))
           {
               bytesSkipped++;
               continue;
           }

           // Looks like the start of a header. See if the rest of the packet
           // makes sense.
           pktPibHdr = (PIB_PKT_HDR*)&(pBuff[bytesSkipped]);

           for(iInfoItem = 0; iInfoItem < PIB_NBR_RSP; iInfoItem++ )
           {
               if( m_pktInfo[iInfoItem].cmdRespByte != pktPibHdr->pktType )
                   continue;

               // Found a hdr / type pair match. Calc the number of bytes required
               // for this packet. That would be a header, the data portion, and
               // a checksum byte.
               dataLen   = m_pktInfo[iInfoItem].expDataLen;
               expPktLen = MIN_PIB_PKT_LEN + dataLen;

               foundInfoItem = TRUE;
               break;
           }

           if( !foundInfoItem )
           {
               bytesSkipped++;
               continue;
           }

           // If we have enough bytes, check the CRC. Otherwise, we have to
           // wait for more bytes to come in
           if( bytesSkipped + expPktLen > buffLen )
               break;

           // Have enough bytes - validate the CRC
           expCRC = CalculateCRC( &( pBuff[bytesSkipped] ), expPktLen - 1 );

           if( pBuff[ bytesSkipped + expPktLen - 1 ] == expCRC )
           {
               // Success!
              // memcpy( &pktPibHdr, &( pBuff[bytesSkipped] ), sizeof( WTTTS_PKT_HDR ) );
        	   pktPibHdr = (PIB_PKT_HDR*)&pBuff[bytesSkipped];

               if( dataLen > 0 )
               {
                  pktPibData = &pBuff[bytesSkipped + sizeof( PIB_PKT_HDR )] ;
               }
               // Remove this packet from the buffer
               bytesSkipped += expPktLen;

               havePIBPkt = TRUE;
               break;
           }

           // CRC error - continue scanning
           bytesSkipped++;
       }

    return havePIBPkt;
}




void mlpCommStateInit(void)
{
	// initialize to idle
	mplCommState = MPL_COMM_IDLE;
}

void mplCommStart(BYTE ptsNbrToRead)
{
	totalToReadPtsBrd = ptsNbrToRead;
	mplCommState = MPL_COMM_START;
}


BOOL isMplCommFinish(void)
{
	if(mplCommState == MPL_COMM_IDLE)
		return TRUE;
	else
		return FALSE;
}


void updateMplCommState(void)
{
	switch(mplCommState)
	{
	case MPL_COMM_START:
		mplCommState = MPL_GET_PIB_PARAMS;
		currentPtsBrd = 0;
		break;
	case MPL_GET_PIB_PARAMS:
		EnTXRS485();
		GetPibParam();
		ResetTimer(thPibTimer,MPL_COMM_TIMEOUT);
		mplCommState = MPL_READ_PIB_PARAMS;
		break;
	case MPL_READ_PIB_PARAMS:
		if(rs485ReceiverHasData())
		{
			tmpWord = rs485ReadReceiverData(pibCommBuff, sizeof(pibCommBuff));
			if(HavePIBPacket(pibCommBuff, tmpWord))
			{
				parsePIBRsp();
				if(totalToReadPtsBrd)
					mplCommState = MPL_GET_PTS_PARAMS;
				else
					mplCommState = MPL_COMM_IDLE;
			}
		}
		if(TimerExpired(thPibTimer))
		{
			if(totalToReadPtsBrd)
				mplCommState = MPL_GET_PTS_PARAMS;
			else
				mplCommState = MPL_COMM_IDLE;
		}
		break;
	case MPL_GET_PTS_PARAMS:
		EnTXRS485();
		GetPtsParam(currentPtsBrd);
		ResetTimer(thPibTimer,MPL_COMM_TIMEOUT);
		mplCommState = MPL_READ_PTS_PARAMS;
		break;
	case MPL_READ_PTS_PARAMS:
		if(rs485ReceiverHasData())
		{
			tmpWord = rs485ReadReceiverData(pibCommBuff, sizeof(pibCommBuff));
			if(HavePIBPacket(pibCommBuff, tmpWord))
			{
				parsePTSRsp();
				if(currentPtsBrd>=(totalToReadPtsBrd-1))
				{
					mplCommState = MPL_COMM_IDLE;
				}
				else
				{
					currentPtsBrd++;
					mplCommState = MPL_GET_PTS_PARAMS;
				}
			}
		}
		if(TimerExpired(thPibTimer))
		{
			mplCommState = MPL_COMM_IDLE;
		}
		break;
	case MPL_COMM_IDLE:
		mplCommState = MPL_COMM_IDLE;
		break;
	default:
		mplCommState = MPL_COMM_IDLE;
		break;
	}
}


void parsePIBRsp(void)
{
	PIB_GET_PARAM_RSP_PKT* pPibParamDest;
	PIB_GET_PARAM_RSP_PKT* pPibParamSource;

	pPibParamDest = (PIB_GET_PARAM_RSP_PKT*)getPibParamAddr();
	pPibParamSource = (PIB_GET_PARAM_RSP_PKT*)pktPibData ;
	memcpy(pPibParamDest,pPibParamSource,sizeof(PIB_GET_PARAM_RSP_PKT));

}


void parsePTSRsp(void)
{
	PTS_GET_PARAM_RSP_PKT* pPtsParamDest;
	PTS_GET_PARAM_RSP_PKT* pPtsParamSource;

	pPtsParamDest = (PTS_GET_PARAM_RSP_PKT*)getPTSParamAddr(currentPtsBrd);
	pPtsParamSource = (PTS_GET_PARAM_RSP_PKT*)pktPibData ;
	memcpy(pPtsParamDest,pPtsParamSource,sizeof(PTS_GET_PARAM_RSP_PKT));

}


//******************************************************************************
//
//  Function: ResetPibInputs
//
//  Arguments: 	BYTE devAddress
//				BYTE switchFlag
//
//  Returns: none
//
//  Description: send command "ResetPibInputs".
//
//******************************************************************************
void ResetPibInputs(BYTE devAddress, BYTE switchFlag)
{
	BYTE boardAddress;
	// Initialise the pointers
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPibResInputCmd = (PIB_RESET_INPUTS_CMD_PKT*)&rs485Packet[sizeof(PIB_PKT_HDR)];;
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+sizeof(PIB_RESET_INPUTS_CMD_PKT)];

	switch(devAddress)
	{
		case 0:
			boardAddress = PIB_ADDRESS;
			break;
		default:	// TODO - clarify
			boardAddress = devAddress;
			break;

	}

	pktPibHdr->pktHdr = TEC_ORIG_HDR;
	pktPibHdr->pktType = PIB_CMD_RESET_INPUTS;
	pktPibHdr->slaveAddr = boardAddress;
	pktPibHdr->dataLen = sizeof( PIB_RESET_INPUTS_CMD_PKT );

	pPibResInputCmd->switchFlag = switchFlag;
	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+sizeof(PIB_RESET_INPUTS_CMD_PKT));

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+sizeof(PIB_RESET_INPUTS_CMD_PKT)+1);

}





//******************************************************************************
//
//  Function: SendHuntingMode
//
//  Arguments: none
//
//  Returns: none
//
//  Description: send to PIB the operation mode - hunting
//
//******************************************************************************
void SendHuntingMode(void)
{
	// Initialise the pointers
	EnTXRS485();
	ResetTimer(thPibTimer,2);
	while(!TimerExpired(thPibTimer))
	{
		ResetWatchdog();
	}
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+0];

	pktPibHdr->pktHdr = TEC_ORIG_HDR;
	pktPibHdr->pktType = PIB_CMD_HUNTING_MODE;
	pktPibHdr->slaveAddr = PIB_ADDRESS;
	pktPibHdr->dataLen = 0;

	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+0);

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+0+1);
	ResetTimer(thPibTimer,MPL_COMM_TIMEOUT);
	while(!TimerExpired(thPibTimer))
	{
		ResetWatchdog();
	}
}


//******************************************************************************
//
//  Function: SendActiveMode
//
//  Arguments: none
//
//  Returns: none
//
//  Description: send to PIB the operation mode - active
//
//******************************************************************************
void SendActiveMode(void)
{
	// Initialise the pointers
	EnTXRS485();
	ResetTimer(thPibTimer,2);
	while(!TimerExpired(thPibTimer))
	{
		ResetWatchdog();
	}
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+0];

	pktPibHdr->pktHdr = TEC_ORIG_HDR;
	pktPibHdr->pktType = PIB_CMD_ACTIVE_MODE;
	pktPibHdr->slaveAddr = PIB_ADDRESS;
	pktPibHdr->dataLen = 0;

	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+0);

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+0+1);
	ResetTimer(thPibTimer,MPL_COMM_TIMEOUT);
	while(!TimerExpired(thPibTimer))
	{
		ResetWatchdog();
	}

}

//******************************************************************************
//
//  Function: SendSleepgMode
//
//  Arguments: none
//
//  Returns: none
//
//  Description: send to PIB the operation mode - sleep. Need to add delay after using that function to make sure that the message has been sent
//
//******************************************************************************
void SendSleepgMode(void)
{
	// Initialise the pointers
	EnTXRS485();
	ResetTimer(thPibTimer,2);
	while(!TimerExpired(thPibTimer))
	{
		ResetWatchdog();
	}
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+0];

	pktPibHdr->pktHdr = TEC_ORIG_HDR;
	pktPibHdr->pktType = PIB_CMD_SLEEP_MODE;
	pktPibHdr->slaveAddr = PIB_ADDRESS;
	pktPibHdr->dataLen = 0;

	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+0);

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+0+1);
	ResetTimer(thPibTimer,MPL_COMM_TIMEOUT);
	while(!TimerExpired(thPibTimer))
	{
		ResetWatchdog();
	}
}













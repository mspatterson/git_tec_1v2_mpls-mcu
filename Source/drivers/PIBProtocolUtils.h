//******************************************************************************
//
//  PIBProtocolUtils.h:
//
//      Copyright (c) 2016, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//	2016-Oct-04		DD 			Initial Implementation
//*******************************************************************************


#ifndef PIBProtocolUtilsH
#define PIBProtocolUtilsH

#include "drivers.h"


// Packet formats (inbound and outbound). All structures are byte-aligned
//#pragma pack(push, 1)

// Packet header
typedef struct {
    BYTE pktHdr;       // PIB_HDR_... define
    BYTE pktType;      // defines what the packet is for. Command or Response...
    BYTE slaveAddr;    // Identifies the slave in either command or response packet.
    BYTE dataLen;      // Number of bytes of packet data to follow; can be zero
} PIB_PKT_HDR;


typedef struct {
	BYTE solenoidCtrl;	// bit flags to activate or desactivate the corresponding solenoid
	BYTE contactCtrl;	// bit flags to close or open the contacts.
	BYTE sensorsPwrCtrl;	// Sensor boards power supply control -  bit 0 - '1' ON, '0' OFF
	WORD solenoidTimeout;	// unsigned 16-bit value representing the maximum time a solenoid can be in active state, measured in seconds.
} PIB_SET_PARAM_CMD_PKT;

typedef struct {
	BYTE solenoidCtrl;
}PIB_SET_SOLENOID_CMD_PKT;

typedef struct {
	BYTE contactCtrl;
}PIB_SET_CONTACT_CMD_PKT;

typedef struct {
	WORD solenoidTimeout;
}PIB_SET_SOL_TIMEOUT_CMD_PKT;


typedef struct {
	BYTE sensorPwerCtrl;
}PIB_SET_SENS_PWR_CMD_PKT;

typedef struct {
	BYTE switchFlag;
}PIB_RESET_INPUTS_CMD_PKT;


typedef struct {
	BYTE cmdRsp;
	BYTE rspRfu;
} PIB_SET_PARAM_RSP_PKT;

typedef struct {
	BYTE solenoidStatus;
	BYTE contactStatus;
	BYTE sensorPwrStatus;
	WORD batVoltage;		// battery voltage in mV
	BYTE solenoidCurrent;	// solenoid current in mA
} PIB_GET_PARAM_RSP_PKT;

typedef struct {
	BYTE magnetSampleRate;	// magnetometer sample rate
	BYTE averagingFactor;	// Averaging coefficient �alpha�. Must be one of 1, 2, 4, 8, 16, 32, 64, or 128.
	BYTE resetMinMax;		// reset min/max.
} PTS_SET_PARAM_CMD_PKT;

typedef struct {
	BYTE cmdRsp;
	BYTE rspRfu;
} PTS_SET_PARAM_RSP_PKT;


typedef struct {
	BYTE magnetSampleRate;
	BYTE averagingFactor;
	WORD magnetXavg;
	WORD magnetYavg;
	WORD magnetZavg;
/*	WORD magnetXmax;
	WORD magnetYmax;
	WORD magnetZmax;
	WORD magnetXmin;
	WORD magnetYmin;
	WORD magnetZmin;*/
}PTS_GET_PARAM_RSP_PKT;


#define SIZEOF_PIB_CHECKSUM  1

#define MIN_PIB_PKT_LEN      ( sizeof( PIB_PKT_HDR ) + SIZEOF_PIB_CHECKSUM )
#define MAX_PIB_PKT_LEN      ( sizeof( PIB_PKT_HDR ) + sizeof( PTS_PARAM_RSP_PKT ) + SIZEOF_PIB_CHECKSUM )


#define PIB_ADDRESS		17

// Packet header defines
#define TEC_ORIG_HDR	0x51	// TEC initialized packet
#define PIB_ORIG_HDR	0x53	// PIB initialized packet
#define PTS_ORIG_HDR	0x57	// PTS initialized packet

// Packet type definitions - commands:
#define PIB_CMD_SET_PARAMS		0x71
#define PIB_CMD_GET_PARAMS		0x73
#define PTS_CMD_SET_PARAMS		0x75
#define PTS_CMD_GET_PARAMS		0x77
#define PIB_CMD_SET_SOLENOID	0x81
#define PIB_CMD_SET_CONTACT		0x83
#define PIB_CMD_SET_SOL_TIMEOUT	0x85
#define PIB_CMD_RESET_INPUTS	0x87
#define PIB_CMD_HUNTING_MODE	0x89
#define PIB_CMD_SLEEP_MODE		0x8A
#define PIB_CMD_ACTIVE_MODE		0x8C

// Packet type definitions - responses:
#define PIB_RSP_SET_PARAMS		0x72
#define PIB_RSP_GET_PARAMS		0x74
#define PTS_RSP_SET_PARAMS		0x76
#define PTS_RSP_GET_PARAMS		0x78


void InitPIBInterface(void);
void GetPibParam(void);
void SetPibParam(BYTE solCtrl, BYTE contCtrl, BYTE sensPwrCtrl, WORD solTimeout);
void SetPtsParam(BYTE ptsAddress, BYTE mgtSampleRate, BYTE avgFactor, BYTE resetM);
void GetPtsParam(BYTE ptsAddress);
void SetPibSolenoid(BYTE solCtrl);
void SetPibContacts(BYTE contCtrl);
void SetPibSolTimeout(WORD solTimeout);
BOOL HavePIBPacket(BYTE *pBuff, WORD buffLen);
void mlpCommStateInit(void);
void mplCommStart(BYTE ptsNbrToRead);
BOOL isMplCommFinish(void);
void updateMplCommState(void);

void ResetPibInputs(BYTE devAddress, BYTE switchFlag);

void SendHuntingMode(void);
void SendActiveMode(void);
void SendSleepgMode(void);
#endif	/*PIBProtocolUtilsH*/

//******************************************************************************
//
//  WTTTSProtocolUtils.c: Communication protocol routines
//
//  Copyright (c) 2013, Microlynx Systems Ltd.
//  ALL RIGHTS RESERVED
//
//  This module provides functions to read and write to the information memory
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2013-March-26     DD        Initial Version
//	2013-Nov-04		DD 			Update RFC packet structure - RFC now reports voltage and mA/h used.
//******************************************************************************

#include "WTTTSProtocolUtils.h"

//#pragma package(smart_init)


//
// Private Declarations
//

// The WTTTS packet parser is table driven. To add a new command
// or response, do the following:
//
//   1. In the .h file, add a new WTTTS_CMD_ or WTTTS_RESP_ define
//      (if required)
//
//   2. Change the NBR_CMD_TYPES or NBR_RESP_TYPES define (if
//      a new cmd or response was defined).
//
//   3. If a new command or response has been defined, and that
//      item has a data payload, define a structure for the
//      payload in the .h file, and add a member of that structure
//      type to the WTTTS_DATA_UNION union definition.
//
//   4. Finally, add a row to (for new cmds or responses) or
//      modify an existing entry in teh m_pktInfo[] table.

#define NBR_CMD_TYPES  			13   // Total number of WTTTS_CMD_... defines
#define NBR_RESP_TYPES 			4   // Total number of WTTTS_RESP_... defines
#define RFC_RATE_DEFAULT		1000		//ms
#define RFC_TIMEOUT_DEFAULT		100			//ms
#define STREAM_RATE_DEFAULT		10			//ms
#define STREAM_TIMEOUT_DEFAULT	300		//seconds
#define PAIR_TIMEOUT_DEFAULT	900		//seconds
#define SOL_TIMEOUT_DEFAULT		10		// seconds
#define RF_PACKET_NUMBER		BUF_LENGTH

#define RFC_RATE_MIN		10		//ms
#define RFC_TIMEOUT_MIN		3			//ms
#define STREAM_RATE_MIN		9			//ms
#define STREAM_TIMEOUT_MIN	5		//seconds
#define PAIR_TIMEOUT_MIN	10		//seconds
#define SER_NUMBER_SIZE	8
typedef struct {
    BYTE pktHdr;        // Expected header byte
    BYTE cmdRespByte;   // One of the WTTTS_CMD_... or WTTTS_RESP_ defines
    BYTE expDataLen;    // Required value for header pktLen param
} WTTTS_PKT_INFO;

static const WTTTS_PKT_INFO m_pktInfo[ NBR_CMD_TYPES ] = {
	{ MPL_HDR_FROM_HOST, WTTTS_CMD_NO_CMD,           0 },
	{ MPL_HDR_FROM_HOST,MPL_CMD_GET_MIN_MAX,  	sizeof(MPL_GET_MIN_MAX_PKT)	 },
	{ MPL_HDR_FROM_HOST,MPL_CMD_CHANNEL_IN_USE,  	0 },
	{ MPL_HDR_FROM_HOST,MPL_CMD_SET_PIB_CONTACTS, sizeof(MPL_SET_PIB_CONTACTS_PKT) },
	{ MPL_HDR_FROM_HOST,MPL_CMD_SET_PIB_SOLENOID, sizeof(MPL_SET_PIB_SOLENOID_PKT) },
	{ MPL_HDR_FROM_HOST,MPL_CMD_SET_SENS_PARAM,   sizeof(MPL_RESET_INPUTS_PKT)	 },
	{ MPL_HDR_FROM_HOST,MPL_CMD_RESET_PIB_INPUTS,   sizeof(MPL_SET_SENS_PARAM_PKT)	 },
    { MPL_HDR_FROM_HOST, WTTTS_CMD_SET_RATE,         sizeof( WTTTS_RATE_PKT ) },
	{ MPL_HDR_FROM_HOST, WTTTS_CMD_QUERY_VER,        0 },
    { MPL_HDR_FROM_HOST, WTTTS_CMD_ENTER_DEEP_SLEEP, 0 },
    { MPL_HDR_FROM_HOST, WTTTS_CMD_SET_RF_CHANNEL,   sizeof( WTTTS_SET_CHAN_PKT ) },
    { MPL_HDR_FROM_HOST, WTTTS_CMD_GET_CFG_DATA,     sizeof( WTTTS_CFG_ITEM )     },
    { MPL_HDR_FROM_HOST, WTTTS_CMD_SET_CFG_DATA,     sizeof( WTTTS_CFG_DATA )     },

};

static WORD wtttsTimeParams[NBR_SET_RATE_TYPES];

const WORD wtttsTimeParamsDefault[NBR_SET_RATE_TYPES]= {
		RFC_RATE_DEFAULT,
		RFC_TIMEOUT_DEFAULT,
		STREAM_RATE_DEFAULT,
		STREAM_TIMEOUT_DEFAULT,
		PAIR_TIMEOUT_DEFAULT,
		SOL_TIMEOUT_DEFAULT,
};


const WORD wtttsTimeParamsMin[NBR_SET_RATE_TYPES]= {
		RFC_RATE_MIN,
		RFC_TIMEOUT_MIN,
		STREAM_RATE_MIN,
		STREAM_TIMEOUT_MIN,
		PAIR_TIMEOUT_MIN,
};
//static WTTTS_RFC_PKT	wtttsRfcPacket;

static BYTE radioPacket[RF_PACKET_NUMBER];
//static BYTE wtttsPacket[RF_PACKET_NUMBER];
static BYTE *wtttsPacket;
static BYTE sequenceNumber;
static BOOL commNoCommand;
static BOOL commStartStream;
static BOOL commStopStream;
static BOOL commPowerDown;
static BYTE currMode =1;
static BYTE pageNumber;
static BYTE flashWriteResult;
static BOOL  havePkt = FALSE;
static BOOL  radioBusy = FALSE;
WTTTS_PKT_HDR* pktHdr;
BYTE* pktData;
WTTTS_CFG_DATA* cfgData;
WTTTS_CFG_ITEM* cfgItem;
static BYTE DeviceSerialNum[8];
static WORD tmpWord;

extern UVOLTS uvResultAverage[ADS1220_TOTAL];
extern signed long gyroSumAverage;
extern MAGNETIC_DATA	sMagnetData;
extern ACCELEROMETOR_DATA	sAccelData;

extern WORD wGyroReadCounter;
extern WORD digGyroSampleCounter;

extern WORD digGyroMoveCounter;
extern WORD digGyroZeroCounter;
extern WORD digGyroLastMovePoint;
extern WORD SamplesCounter[ADC_USED_LAST+2];

void word2ByteArray(void *pVoid, WORD wValue);


static PIB_GET_PARAM_RSP_PKT sPibParams;
static PTS_GET_PARAM_RSP_PKT sPtsParams[NBR_PTS_BOARDS];



void UpdateTimeParamsFromFlash(void)
{
	WORD i;
	WORD wtttsTimes[NBR_SET_RATE_TYPES];
	BOOL varUpdated = FALSE;

	ReadPageFromFlash((BYTE) UPDATE_RATE_PAGE_IN_FLASH, (BYTE *)&wtttsTimes, sizeof(wtttsTimes));
	ResetWatchdog();
	for(i=0;i<NBR_SET_RATE_TYPES;i++)
	{
		if((wtttsTimes[i] == 0XFFFF)||(wtttsTimes[i] < wtttsTimeParamsMin[i]))
		{
			wtttsTimeParams[i] = wtttsTimeParamsDefault[i];
			varUpdated = TRUE;
		}
		else
		{
			wtttsTimeParams[i] = wtttsTimes[i];
		}
	}
	ResetWatchdog();
	// if a variable has been updated write back to flash
	if(varUpdated)
	{
		WritePageInFlash((BYTE) UPDATE_RATE_PAGE_IN_FLASH, (BYTE *)&wtttsTimeParams, BYTES_IN_PAGE);
	}

}


BOOL SetTimeParameter(BYTE paramNum, WORD paramVal)
{
	if(paramNum>=NBR_SET_RATE_TYPES)
		return FALSE;

	wtttsTimeParams[paramNum] = paramVal;

	WritePageInFlash((BYTE) UPDATE_RATE_PAGE_IN_FLASH, (BYTE *)&wtttsTimeParams, BYTES_IN_PAGE);

	return TRUE;
}


WORD GetTimeParameter(const WTTTS_RATE_TYPE paramNum)
{
	return wtttsTimeParams[paramNum];

}

// RFC packet:
// HEADER is 6 bytes
// RFC packet is 54 bytes
// CRC is 1 byte
// Total - 61 bytes
//void SendRfc(void)
//{
//	WTTTS_PKT_HDR*	pWtttsRfcHeader;
//	WTTTS_RFC_PKT*	pWtttsRfcPacket;
//	BYTE*	pWtttsRfcCRC;
//	DWORD pressure;
////	WORD wTemp;
//
//	radioPacket[0] = TX_DATA;
//	// Initialise the pointers
//	wtttsPacket = &radioPacket[1];
//	pWtttsRfcHeader = (WTTTS_PKT_HDR*)wtttsPacket;
//	pWtttsRfcPacket = (WTTTS_RFC_PKT*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)];
//	pWtttsRfcCRC	= (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_RFC_PKT)];
//
//	pWtttsRfcHeader->pktHdr = WTTTS_HDR_RX;
//	pWtttsRfcHeader->pktType = WTTTS_RESP_REQ_FOR_CMD;
//	pWtttsRfcHeader->seqNbr = sequenceNumber++;
//	pWtttsRfcHeader->dataLen = sizeof( WTTTS_RFC_PKT );
//
//	word2ByteArray(&pWtttsRfcHeader->timeStamp,GetTenMsCount());
//
//
//	pWtttsRfcPacket->temperature = GetTemperature();	// function to do
//	pWtttsRfcPacket->battType = GetBatType();
//	word2ByteArray(&pWtttsRfcPacket->battVoltage,GetBatVoltageAdc());
//	word2ByteArray(&pWtttsRfcPacket->battUsed,GetBatUsedmAh());
//
////	pressure = 12345;						// function to do
////	pWtttsRfcPacket->pressure[0] = (BYTE)pressure;
////	pWtttsRfcPacket->pressure[1] = (BYTE)(pressure>>8);
////	pWtttsRfcPacket->pressure[2] = (BYTE)(pressure>>16);
//	ConvertLongTo3Bytes(uvResultAverage[ADC_SWITCH_IN], pWtttsRfcPacket->pressure);
//
//	pWtttsRfcPacket->rpm = GetRPM();
//	pWtttsRfcPacket->lastReset	= GetLastReset();
//	pWtttsRfcPacket->rfChannel = GetRfChannel();
//	pWtttsRfcPacket->currMode = GetCurrMode();
//	pWtttsRfcPacket->rfu = 0;
//
//	word2ByteArray(&pWtttsRfcPacket->rfcRate,GetTimeParameter(SRT_RFC_RATE));
//	word2ByteArray(&pWtttsRfcPacket->rfcTimeout,GetTimeParameter(SRT_RFC_TIMEOUT));
//	word2ByteArray(&pWtttsRfcPacket->streamRate, GetTimeParameter(SRT_STREAM_RATE));
//	word2ByteArray(&pWtttsRfcPacket->streamTimeout, GetTimeParameter(SRT_STREAM_TIMEOUT));
//	word2ByteArray(&pWtttsRfcPacket->pairingTimeout, GetTimeParameter(SRT_PAIR_TIMEOUT));
//
//
//	ConvertLongTo3Bytes(uvResultAverage[ADC_TORQUE_000], pWtttsRfcPacket->torque000);
//	ConvertLongTo3Bytes(uvResultAverage[ADC_TORQUE_180], pWtttsRfcPacket->torque180);
//	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_000], pWtttsRfcPacket->tension000);
//	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_090], pWtttsRfcPacket->tension090);
//	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_180], pWtttsRfcPacket->tension180);
//	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_270], pWtttsRfcPacket->tension270);
//
//	pWtttsRfcPacket->compassX[0] = (BYTE)sMagnetData.iXField[0];
//	pWtttsRfcPacket->compassX[1] = (BYTE)(sMagnetData.iXField[0]>>8);
//	pWtttsRfcPacket->compassY[0] = (BYTE)sMagnetData.iYField[0];
//	pWtttsRfcPacket->compassY[1] = (BYTE)(sMagnetData.iYField[0]>>8);
//	pWtttsRfcPacket->compassZ[0] = (BYTE)sMagnetData.iZField[0];
//	pWtttsRfcPacket->compassZ[1] = (BYTE)(sMagnetData.iZField[0]>>8);
////	wTemp = GetBatVoltageGasGauge();		// temp feature send Battery Voltage in mV instead of Accel X - bat voltage measured with gas gauge
////
////	pWtttsRfcPacket->compassZ[0] = (BYTE)digGyroLastMovePoint;
////	pWtttsRfcPacket->compassZ[1] = (BYTE)(digGyroLastMovePoint>>8);
//
//	pWtttsRfcPacket->accelX[0] = (BYTE)SamplesCounter[0];
//	pWtttsRfcPacket->accelX[1] = (BYTE)(SamplesCounter[0]>>8);
//	pWtttsRfcPacket->accelY[0] = (BYTE)SamplesCounter[2];
//	pWtttsRfcPacket->accelY[1] = (BYTE)(SamplesCounter[2]>>8);
//	pWtttsRfcPacket->accelZ[0] = (BYTE)SamplesCounter[4];
//	pWtttsRfcPacket->accelZ[1] = (BYTE)(SamplesCounter[4]>>8);
//
////	pWtttsRfcPacket->accelX[0] = (BYTE)digGyroSampleCounter;
////	pWtttsRfcPacket->accelX[1] = (BYTE)(digGyroSampleCounter>>8);
////	pWtttsRfcPacket->accelY[0] = (BYTE)digGyroZeroCounter;
////	pWtttsRfcPacket->accelY[1] = (BYTE)(digGyroZeroCounter>>8);
////	pWtttsRfcPacket->accelZ[0] = (BYTE)digGyroSampleCounter;
////	pWtttsRfcPacket->accelZ[1] = (BYTE)(digGyroSampleCounter>>8);
////	digGyroLastMovePoint = 0;
////	digGyroMoveCounter=0;
////	digGyroZeroCounter=0;
//
//	digGyroSampleCounter = 0;
//
//
////	pWtttsRfcPacket->accelX[0] = (BYTE)sAccelData.iXField[0];
////	pWtttsRfcPacket->accelX[1] = (BYTE)(sAccelData.iXField[0]>>8);
////	pWtttsRfcPacket->accelY[0] = (BYTE)sAccelData.iYField[0];
////	pWtttsRfcPacket->accelY[1] = (BYTE)(sAccelData.iYField[0]>>8);
////	pWtttsRfcPacket->accelZ[0] = (BYTE)sAccelData.iZField[0];
////	pWtttsRfcPacket->accelZ[1] = (BYTE)(sAccelData.iZField[0]>>8);
//
//	*pWtttsRfcCRC = CalculateCRC(wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_RFC_PKT));
//
//	ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_RFC_PKT)+2);
//
//}



BOOL HaveWtttsPacket(BYTE *pBuff, WORD buffLen)
{
	   // Scans the passed buffer for a WTTTS command or response. Returns true if
	    // a packet is found, in which case pktHdr and pktData pointers are asigned.
	    // Returns false if no packet is found.
    WORD bytesSkipped = 0;

    BOOL foundInfoItem = FALSE;
    int  dataLen       = 0;
    int  expPktLen     = 0;
    unsigned int iInfoItem = 0;
    BYTE expCRC;

    havePkt = FALSE;

    if( buffLen < (WORD)MIN_WTTTS_PKT_LEN )
        return FALSE;

    while( bytesSkipped + (WORD)MIN_WTTTS_PKT_LEN <= buffLen )
       {
    		ResetWatchdog();
           // Have enough bytes for a packet. Header byte must be the first
           // byte we see
           if( pBuff[bytesSkipped] != MPL_HDR_FROM_HOST )
           {
               bytesSkipped++;
               continue;
           }

           // Looks like the start of a header. See if the rest of the packet
           // makes sense.
           //WTTTS_PKT_HDR* pHdr = (WTTTS_PKT_HDR*)&( pBuff[bytesSkipped] );
           pktHdr = (WTTTS_PKT_HDR*)&( pBuff[bytesSkipped] );

           for(iInfoItem = 0; iInfoItem < NBR_CMD_TYPES ; iInfoItem++ )
           {

               if( m_pktInfo[iInfoItem].cmdRespByte != pktHdr->pktType )
                   continue;

               // Found a hdr / type pair match. Calc the number of bytes required
               // for this packet. That would be a header, the data portion, and
               // a checksum byte.
               dataLen   = m_pktInfo[iInfoItem].expDataLen;
               expPktLen = MIN_WTTTS_PKT_LEN + dataLen;

               foundInfoItem = TRUE;
               break;
           }

           if( !foundInfoItem )
           {
               bytesSkipped++;
               continue;
           }

           // If we have enough bytes, check the CRC. Otherwise, we have to
           // wait for more bytes to come in
           if( bytesSkipped + expPktLen > buffLen )
               break;

           // Have enough bytes - validate the CRC
           expCRC = CalculateCRC( &( pBuff[bytesSkipped] ), expPktLen - 1 );

           if( pBuff[ bytesSkipped + expPktLen - 1 ] == expCRC )
           {
               // Success!
              // memcpy( &pktHdr, &( pBuff[bytesSkipped] ), sizeof( WTTTS_PKT_HDR ) );
        	   pktHdr = (WTTTS_PKT_HDR*)&pBuff[bytesSkipped];

               if( dataLen > 0 )
               {
                  // memcpy( &pktData, &( pBuff[bytesSkipped + sizeof( WTTTS_PKT_HDR )] ), dataLen );
            	   pktData = &pBuff[bytesSkipped + sizeof( WTTTS_PKT_HDR )] ;
               }
               // Remove this packet from the buffer
               bytesSkipped += expPktLen;

               havePkt = TRUE;
               break;
           }

           // CRC error - continue scanning
           bytesSkipped++;
       }

    return havePkt;
}


void ExecuteCommand(void)
{
	WORD wData;
	radioBusy = TRUE;
	ResetWatchdog();
	switch(pktHdr->pktType)
	{
		case WTTTS_CMD_NO_CMD:
			SetNoCommand(TRUE);
			radioBusy = FALSE;
			break;
		case WTTTS_CMD_START_STREAM:
			SetStartStream(TRUE);
			break;
		case WTTTS_CMD_STOP_STREAM:
			SetStopStream(TRUE);
			break;
		case WTTTS_CMD_SET_RATE:
			wData = pktData[3];
			wData <<= 8;
			wData |=pktData[2];
			SetTimeParameter(pktData[0], wData);
			if(pktData[0] == SRT_SOLENOID_TIMEOUT)
			{
				SetPibSolTimeout(wData);
			}
			break;
		case WTTTS_CMD_QUERY_VER:
			SendVersionInfo();
			break;
		case WTTTS_CMD_ENTER_DEEP_SLEEP:
			SetPowerDown(TRUE);
			break;
		case WTTTS_CMD_SET_RF_CHANNEL:
			SetChangeChannelPending(pktData[0]);
			break;
		case WTTTS_CMD_SET_CFG_DATA:
			cfgData = (WTTTS_CFG_DATA*)pktData;
			pageNumber = cfgData->pageNbr;
			flashWriteResult = WritePageInFlash(pageNumber, cfgData->pageData, (WORD)NBR_BYTES_PER_WTTTS_PAGE);
			SendCfgEvent();
			break;
		case WTTTS_CMD_GET_CFG_DATA:
			cfgItem = (WTTTS_CFG_ITEM*)pktData;
			pageNumber = cfgItem->pageNbr;
			SendCfgEvent();
			break;
		case MPL_CMD_GET_MIN_MAX:
			mplSendMinMax();
			break;
		case MPL_CMD_CHANNEL_IN_USE:
			mplSetDoNotUseChannel();
			break;
		case MPL_CMD_SET_PIB_CONTACTS:
			mplSetPibContacts(pktData[0]);
			break;
		case MPL_CMD_SET_PIB_SOLENOID:
			mplSetPibSolenoid(pktData[0], pktData[1]);
			break;
		case MPL_CMD_SET_SENS_PARAM:
			mplSetPibParameters(pktData[0], pktData[1]);
			break;
		case MPL_CMD_RESET_PIB_INPUTS:
			//TODO
			mplResetPibInput(pktData[0], pktData[1]);
			break;
		default:
			break;
	}
}


void SendVersionInfo(void)
{
	WTTTS_PKT_HDR*	pWtttsHeader;
	WTTTS_VER_PKT*	pWtttsVerPacket;
	BYTE*	pWtttsCRC;

	// Initialise the pointers
	radioPacket[0] = TX_DATA;
	wtttsPacket = &radioPacket[1];
	pWtttsHeader = (WTTTS_PKT_HDR*)wtttsPacket;
	pWtttsVerPacket = (WTTTS_VER_PKT*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)];
	pWtttsCRC	= (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_VER_PKT)];

	pWtttsHeader->pktHdr = MPL_HDR_FROM_DEVICE;
	pWtttsHeader->pktType = WTTTS_RESP_VER_PKT;
	pWtttsHeader->seqNbr = sequenceNumber++;
	pWtttsHeader->dataLen = sizeof( WTTTS_VER_PKT );
//	pWtttsHeader->timeStamp = GetTenMsCount();
	word2ByteArray(&pWtttsHeader->timeStamp,GetTenMsCount());


	pWtttsVerPacket->hardwareSettings = GetHardwareRev();
	// the fw version is defined in builds.h
	pWtttsVerPacket->fwVer[0] = MAJOR_VERSION;
	pWtttsVerPacket->fwVer[1] = MINOR_VERSION;
	pWtttsVerPacket->fwVer[2] = BUILD_NUMBER;
	pWtttsVerPacket->fwVer[3] = REL_DBG;

	*pWtttsCRC = CalculateCRC(wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_VER_PKT));

	ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_VER_PKT)+2);

}

void SendCfgEvent(void)
{
	WTTTS_PKT_HDR*	pWtttsHeader;
	WTTTS_CFG_DATA*	pWtttsCfgPacket;
	BYTE*	pWtttsCRC;

	// Initialise the pointers
	radioPacket[0] = TX_DATA;
	wtttsPacket = &radioPacket[1];
	pWtttsHeader = (WTTTS_PKT_HDR*)wtttsPacket;
	pWtttsCfgPacket =(WTTTS_CFG_DATA*) &wtttsPacket[sizeof(WTTTS_PKT_HDR)];
	pWtttsCRC	= (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_CFG_DATA)];

	pWtttsHeader->pktHdr = MPL_HDR_FROM_DEVICE;
	pWtttsHeader->pktType = WTTTS_RESP_CFG_DATA;
	pWtttsHeader->seqNbr = sequenceNumber++;
	pWtttsHeader->dataLen = sizeof( WTTTS_CFG_DATA );
//	pWtttsHeader->timeStamp = GetTenMsCount();
	word2ByteArray(&pWtttsHeader->timeStamp,GetTenMsCount());

	pWtttsCfgPacket->pageNbr = pageNumber;
	pWtttsCfgPacket->result = ReadPageFromFlash(pageNumber, pWtttsCfgPacket->pageData, (WORD)NBR_BYTES_PER_WTTTS_PAGE);

	*pWtttsCRC = CalculateCRC(wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_CFG_DATA));

	ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_CFG_DATA)+2);

}

// Stream Packet :
// Header - 6 bytes
// Stream Data - 34 bytes
// CRC - 1 byte
// Total - 41 bytes
void SendStreamData(void)
{
	WTTTS_PKT_HDR*	pWtttsRfcHeader;
	WTTTS_STREAM_DATA*	pWtttsStreamPacket;
	BYTE*	pWtttsRfcCRC;

//static WORD gyroFakeValue;

	// Initialise the pointers
	radioPacket[0] = TX_DATA;
	wtttsPacket = &radioPacket[1];
	pWtttsRfcHeader = (WTTTS_PKT_HDR*)wtttsPacket;
	pWtttsStreamPacket = (WTTTS_STREAM_DATA*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)];
	pWtttsRfcCRC	= (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_STREAM_DATA)];

	pWtttsRfcHeader->pktHdr = MPL_HDR_FROM_DEVICE;
	pWtttsRfcHeader->pktType = WTTTS_RESP_STREAM_DATA;
	pWtttsRfcHeader->seqNbr = sequenceNumber++;
	pWtttsRfcHeader->dataLen = sizeof( WTTTS_STREAM_DATA );
	//pWtttsRfcHeader->timeStamp = GetTenMsCount();	// function to do
	word2ByteArray(&pWtttsRfcHeader->timeStamp,GetTenMsCount());
	tmpWord = GetTenMsCount();

	ConvertLongTo3Bytes(uvResultAverage[ADC_TORQUE_000], pWtttsStreamPacket->torque000);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TORQUE_180], pWtttsStreamPacket->torque180);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_000], pWtttsStreamPacket->tension000);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_090], pWtttsStreamPacket->tension090);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_180], pWtttsStreamPacket->tension180);
	ConvertLongTo3Bytes(uvResultAverage[ADC_TENSION_270], pWtttsStreamPacket->tension270);	// test uncomment
//	ConvertLongTo3Bytes(uvResultAverage[ADC_SWITCH_IN], pWtttsStreamPacket->tension270);	// test delete

//	ConvertLongTo4Bytes(gyroSumAverage, pWtttsStreamPacket->gyro);	// convert in mdeg for now
	ConvertLongTo4Bytes(gyroSumAverage, pWtttsStreamPacket->gyro);	// convert in mdeg for now
//	ConvertLongTo4Bytes(gyroFakeValue++, pWtttsStreamPacket->gyro);	// convert in mdeg for now

	pWtttsStreamPacket->compassX[0] = (BYTE)sMagnetData.iXField[0];
	pWtttsStreamPacket->compassX[1] = (BYTE)(sMagnetData.iXField[0]>>8);
	pWtttsStreamPacket->compassY[0] = (BYTE)sMagnetData.iYField[0];
	pWtttsStreamPacket->compassY[1] = (BYTE)(sMagnetData.iYField[0]>>8);
	pWtttsStreamPacket->compassZ[0] = (BYTE)sMagnetData.iZField[0];
	pWtttsStreamPacket->compassZ[1] = (BYTE)(sMagnetData.iZField[0]>>8);
//	pWtttsStreamPacket->accelX[0] = (BYTE)sAccelData.iXField[0];
//	pWtttsStreamPacket->accelX[1] = (BYTE)(sAccelData.iXField[0]>>8);
//	pWtttsStreamPacket->accelY[0] = (BYTE)sAccelData.iYField[0];
//	pWtttsStreamPacket->accelY[1] = (BYTE)(sAccelData.iYField[0]>>8);
//	pWtttsStreamPacket->accelZ[0] = (BYTE)sAccelData.iZField[0];
//	pWtttsStreamPacket->accelZ[1] = (BYTE)(sAccelData.iZField[0]>>8);

	pWtttsStreamPacket->accelX[0] = (BYTE)wGyroReadCounter;
	pWtttsStreamPacket->accelX[1] = (BYTE)(wGyroReadCounter>>8);
	pWtttsStreamPacket->accelY[0] = 0;
	pWtttsStreamPacket->accelY[1] = 0;
	pWtttsStreamPacket->accelZ[0] = 0;
	pWtttsStreamPacket->accelZ[1] = 0;

//	ConvertLongTo3Bytes(uvResultAverage[ADC_SWITCH_IN], pWtttsStreamPacket->adcSwIn);

//	tmpWord  %= 100;
//	if(tmpWord < 1)
//	{
//		*pWtttsRfcCRC = CalculateCRC(wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_STREAM_DATA));
//	}
//	else
//	{
//		*pWtttsRfcCRC = 0;
//		pWtttsRfcHeader->pktType = 0;
//	}

	*pWtttsRfcCRC = CalculateCRC(wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_STREAM_DATA));
	ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_STREAM_DATA)+2);

}

void SetNoCommand(BOOL bVal)
{
	commNoCommand = bVal;
}

BOOL GetCommandStatus(void)
{
	return commNoCommand;
}
void SetStartStream(BOOL bVal)
{
	commStartStream = bVal;
}

void SetStopStream(BOOL bVal)
{
	commStopStream = bVal;
}

BOOL GetStartStreamStatus(void)
{
	return commStartStream;
}

BOOL GetStopStreamStatus(void)
{
	return commStopStream;
}

void SetPowerDown(BOOL bVal)
{
	commPowerDown = bVal;
}

BOOL GetPowerDownStatus(void)
{
	return commPowerDown;
}

// TO DO
BYTE GetCurrMode(void)
{
	return currMode;
}

void ConvertLongTo3Bytes(signed long slValue, BYTE *pData)
{
	if(slValue>INT24_MAX_POSITIVE)
		slValue = INT24_MAX_POSITIVE;

	if(slValue < INT24_MAX_NEGATIVE)
			slValue = INT24_MAX_NEGATIVE;

	pData[0] = 	(BYTE) slValue;
	pData[1] = 	(BYTE) (slValue>>8);
	pData[2] = 	(BYTE) (slValue>>16);

}


void ConvertLongTo4Bytes(signed long slValue, BYTE *pData)
{

	pData[0] = 	(BYTE) slValue;
	pData[1] = 	(BYTE) (slValue>>8);
	pData[2] = 	(BYTE) (slValue>>16);
	pData[3] = 	(BYTE) (slValue>>24);

}

void word2ByteArray(void *pVoid, WORD wValue)
{
	BYTE *pByte;
	WORD wTemp;
	wTemp = wValue;	// function to do
	pByte = (BYTE*)	pVoid;
	*pByte++ = (BYTE)wTemp;
	*pByte = (BYTE)(wTemp>>8);
}



BOOL GetRadioPacketStatus(void)
{
	return havePkt;
}


void ClearRadioPacketStatus(void)
{
	havePkt = FALSE;
}

BOOL IsRadioBusy(void)
{
	return radioBusy;
}

void SetRadioBusy(BOOL bValue)
{
	radioBusy = bValue;
}

void SaveSerNumber(BYTE *pData)
{
	WORD i;
	for(i=0;i<SER_NUMBER_SIZE;i++)
	{
		DeviceSerialNum[i] = *pData++;
	}
}



/*
 * This command is PIB specific. It instructs the PIB to report the current min / max values it has saved all sensors
 * readings managed by the PIB. If the Reset parameter in the command packet is set to 1, the PIB shall reset the
 * min / max values to current average value for each sensor.
 *
 */
// TODO
void mplSendMinMax(void)
{
	WTTTS_PKT_HDR*	pWtttsHeader;
	WTTTS_VER_PKT*	pWtttsVerPacket;
	BYTE*	pWtttsCRC;

	// Initialise the pointers
	radioPacket[0] = TX_DATA;
	wtttsPacket = &radioPacket[1];
	pWtttsHeader = (WTTTS_PKT_HDR*)wtttsPacket;
	pWtttsVerPacket = (WTTTS_VER_PKT*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)];
	pWtttsCRC	= (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_VER_PKT)];

	pWtttsHeader->pktHdr = MPL_HDR_FROM_DEVICE;
	pWtttsHeader->pktType = WTTTS_RESP_VER_PKT;
	pWtttsHeader->seqNbr = sequenceNumber++;
	pWtttsHeader->dataLen = sizeof( WTTTS_VER_PKT );
//	pWtttsHeader->timeStamp = GetTenMsCount();
	word2ByteArray(&pWtttsHeader->timeStamp,GetTenMsCount());


	pWtttsVerPacket->hardwareSettings = GetHardwareRev();
	// the fw version is defined in builds.h
	pWtttsVerPacket->fwVer[0] = MAJOR_VERSION;
	pWtttsVerPacket->fwVer[1] = MINOR_VERSION;
	pWtttsVerPacket->fwVer[2] = BUILD_NUMBER;
	pWtttsVerPacket->fwVer[3] = REL_DBG;

	*pWtttsCRC = CalculateCRC(wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_VER_PKT));

	ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_VER_PKT)+2);

}

/* mplSetPibContacts(BYTE bmContacts)
 * This command is PIB specific. It instructs the PIB to set the state of its contact outputs
 * according to the bit flags passed in this command. Note that the state of any or all contacts
 * can be changed simultaneously in a single command.
 * Bit flags indicating the desired state of the output contacts (1 = contact closed, 0 = contact open)
 *		D0 = contact 1
 *		D1 = contact 2
 *		D2 = contact 3
 *		D3 = contact 4
 *		D4  D7 = RFU
 *
 */
void mplSetPibContacts(BYTE bmContacts)
{

	// TODO
	SetPibContacts(bmContacts);

#ifdef EMULATE_PIB
		EmulatePIB_SetPibContacts(bmContacts);
#endif
}


/*
 * void mplSetPibSolenoid(BYTE nbrSolenoid, BYTE stateSolenoid)
 * This command is PIB specific. It instructs the PIB to open or close a solenoid.
 * Only one solenoid can be active at a time. If a command to activate a solenoid
 * is received when a different solenoid is already active, then the previous solenoid
 * is deactivated and the new one is activated
 *		Solenoid Number	1	0 = All solenoids (deactivate only)
 *							1  6 = Desired solenoid
 *		State	1	0 = deactivate, 1 = activate
 *
 */
void mplSetPibSolenoid(BYTE nbrSolenoid, BYTE stateSolenoid)
{
	BYTE solCtrl;
	solCtrl = 0;

	if(nbrSolenoid==0)
		solCtrl = 0;
	else
		solCtrl =(stateSolenoid<<(nbrSolenoid-1));

	SetPibSolenoid(solCtrl);

#ifdef EMULATE_PIB
		EmulatePIB_SetPIBSolenoid(solCtrl);
#endif

}


/*
 * void mplSetPibSolenoid(BYTE nbrSolenoid, BYTE stateSolenoid)
 * This command is PIB specific. It sets the PIB magnetometer parameters.
 * The PIB can support up to four magnetometer boards. All boards operate with the same parameters.
 *
 */
void mplSetPibParameters(BYTE mSampleRate, BYTE mAveragingFactor)
{

	// TODO

}

//******************************************************************************
//
//  Function: mplSendRfcPIB
//
//  Arguments: none
//
//  Returns: none
//
//  Description: assemble and sends PIB RFC packet
//
//******************************************************************************
void mplSendRfcPIB(void)
{
	// TODO
	WTTTS_PKT_HDR*	pWtttsRfcHeader;
	BYTE*	pWtttsRfcCRC;
//	DWORD pressure;
	WORD i;

	MPL_RFC_PKT* pMplRfcPacket;

	radioPacket[0] = TX_DATA;
	// Initialise the pointers
	wtttsPacket = &radioPacket[1];
	pWtttsRfcHeader = (WTTTS_PKT_HDR*)wtttsPacket;
	pMplRfcPacket = (MPL_RFC_PKT*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)];
	pWtttsRfcCRC	= (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(MPL_RFC_PKT)];

	pWtttsRfcHeader->pktHdr = MPL_HDR_FROM_DEVICE;
	pWtttsRfcHeader->pktType = MPL_RESP_RFC_PIB;
	pWtttsRfcHeader->seqNbr = sequenceNumber++;
	pWtttsRfcHeader->dataLen = sizeof( MPL_RFC_PKT );

	word2ByteArray(&pWtttsRfcHeader->timeStamp,GetTenMsCount());


	pMplRfcPacket->temperature = GetTemperature();	// function to do
//	pMplRfcPacket->battType = GetBatType();
	pMplRfcPacket->battType = BATTERY_LI;
	word2ByteArray(&pMplRfcPacket->battVoltage,sPibParams.batVoltage);
	word2ByteArray(&pMplRfcPacket->battUsed,GetBatUsedmAh());


	ConvertLongTo3Bytes(uvResultAverage[ADC_SWITCH_IN], pMplRfcPacket->pressure);

	pMplRfcPacket->rpm = GetRPM();
	pMplRfcPacket->lastReset	= GetLastReset();
	pMplRfcPacket->rfChannel = GetRfChannel();
	pMplRfcPacket->currMode = GetCurrMode();
	pMplRfcPacket->contactState = sPibParams.contactStatus;
	pMplRfcPacket->inputState = 0;	//TODO

	// convert the solenoid bit mask to number
	if(sPibParams.solenoidStatus==0)
	{
		pMplRfcPacket->solenoidState = 0x00;	//no solenoid is active
	}
	else
	{
		for(i=0;i<8;i++)
		{
			if(sPibParams.solenoidStatus&(1<<i))
				break;
		}
		pMplRfcPacket->solenoidState = i+1;	//solenoid is active
	}

	pMplRfcPacket->rfuArray[0]= 0;
	pMplRfcPacket->rfuArray[1]= 0;
	pMplRfcPacket->rfuArray[2]= 0;
	pMplRfcPacket->rfuArray[3]= 0;


	word2ByteArray(&pMplRfcPacket->rfcRate,GetTimeParameter(SRT_RFC_RATE));
	word2ByteArray(&pMplRfcPacket->rfcTimeout,GetTimeParameter(SRT_RFC_TIMEOUT));
	word2ByteArray(&pMplRfcPacket->solenoidTimeout, GetTimeParameter(SRT_SOLENOID_TIMEOUT));
	pMplRfcPacket->sensorSampleRate=sPtsParams[0].magnetSampleRate;
	pMplRfcPacket->sensorAveraging=sPtsParams[0].averagingFactor;
	word2ByteArray(&pMplRfcPacket->pairingTimeout,GetTimeParameter(SRT_PAIR_TIMEOUT));


	// Unique device ID from serial number chip
	pMplRfcPacket->deviceID[0] = DeviceSerialNum[1];
	pMplRfcPacket->deviceID[1] = DeviceSerialNum[2];
	pMplRfcPacket->deviceID[2] = DeviceSerialNum[3];
	pMplRfcPacket->deviceID[3] = DeviceSerialNum[4];
	pMplRfcPacket->deviceID[4] = DeviceSerialNum[5];
	pMplRfcPacket->deviceID[5] = DeviceSerialNum[6];

	/*
	*	4 Byte values that contain the sensor number being reported in the following fields.
	* 	The first byte is Sensor 1, second Sensor 2, etc. If a sensor is not used, its byte value shall be set to 0xFF
	*/
	pMplRfcPacket->rptSensors[0] = 1;
	pMplRfcPacket->rptSensors[1] = 0xFF;
	pMplRfcPacket->rptSensors[2] = 0xFF;
	pMplRfcPacket->rptSensors[3] = 0xFF;


// rearange the mpl RFC packet definition to match the software
	word2ByteArray(&pMplRfcPacket->sens1AvgValueX,sPtsParams[0].magnetXavg);
	word2ByteArray(&pMplRfcPacket->sens1AvgValueY,sPtsParams[0].magnetYavg);
	word2ByteArray(&pMplRfcPacket->sens1AvgValueZ,sPtsParams[0].magnetZavg);
	word2ByteArray(&pMplRfcPacket->sens2AvgValueX,sPtsParams[1].magnetXavg);
	word2ByteArray(&pMplRfcPacket->sens2AvgValueY,sPtsParams[1].magnetYavg);
	word2ByteArray(&pMplRfcPacket->sens2AvgValueZ,sPtsParams[1].magnetZavg);
	word2ByteArray(&pMplRfcPacket->sens3AvgValueX,sPtsParams[2].magnetXavg);
	word2ByteArray(&pMplRfcPacket->sens3AvgValueY,sPtsParams[2].magnetYavg);
	word2ByteArray(&pMplRfcPacket->sens3AvgValueZ,sPtsParams[2].magnetZavg);
	word2ByteArray(&pMplRfcPacket->sens4AvgValueX,sPtsParams[3].magnetXavg);
	word2ByteArray(&pMplRfcPacket->sens4AvgValueY,sPtsParams[3].magnetYavg);
	word2ByteArray(&pMplRfcPacket->sens4AvgValueZ,sPtsParams[3].magnetZavg);

//	word2ByteArray(&pMplRfcPacket->sens1AvgValueX,1);
//	word2ByteArray(&pMplRfcPacket->sens1AvgValueY,2);
//	word2ByteArray(&pMplRfcPacket->sens1AvgValueZ,3);
//	word2ByteArray(&pMplRfcPacket->sens2AvgValueX,11);
//	word2ByteArray(&pMplRfcPacket->sens2AvgValueY,12);
//	word2ByteArray(&pMplRfcPacket->sens2AvgValueZ,13);
//	word2ByteArray(&pMplRfcPacket->sens3AvgValueX,21);
//	word2ByteArray(&pMplRfcPacket->sens3AvgValueY,22);
//	word2ByteArray(&pMplRfcPacket->sens3AvgValueZ,23);
//	word2ByteArray(&pMplRfcPacket->sens4AvgValueX,31);
//	word2ByteArray(&pMplRfcPacket->sens4AvgValueY,32);
//	word2ByteArray(&pMplRfcPacket->sens4AvgValueZ,33);


	*pWtttsRfcCRC = CalculateCRC((BYTE*)wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(MPL_RFC_PKT));

	ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(MPL_RFC_PKT)+2);

}

//******************************************************************************
//
//  Function: getPibParamAddr
//
//  Arguments: none
//
//  Returns: PIB_GET_PARAM_RSP_PKT*	- address of the local PIB_PARAM structure
//
//  Description: return the address of the local PIB_PARAM structure
//
//******************************************************************************
void* getPibParamAddr(void)
{
	return (void*)(&sPibParams);
}

//******************************************************************************
//
//  Function: getPTSParamAddr
//
//  Arguments: BYTE nbrPts
//
//  Returns: PTS_GET_PARAM_RSP_PKT*	- address of the local PTS_PARAM structure
//
//  Description: return the address of the local PTS_PARAM structure
//
//******************************************************************************
void* getPTSParamAddr(BYTE nbrPts)
{
	return (void*)(&sPtsParams[nbrPts]);
}



/*
 * void mplSetPibSolenoid(BYTE nbrSolenoid, BYTE stateSolenoid)
 * This command is PIB specific. When a switch closure is detected on the PIB board,
 * or a digital input is activated on a sensor board, the firmware in those devices
 * will continue to report the switch or input as set until cleared by this command.
 * Device Address	Address of the device to clear an indication.
 * 					Address 0 is used for PIB board switches; a non-zero address is used to indicate a sensor board.
 * 	Clear Switch Flags	1	Bit flags used as follows:
 *						D0  if set, clear switch 1 indication
 *						D1  if set, clear switch 2 indication
 *						D2  D7  RFU
 *
 */
void mplResetPibInput(BYTE devAddress, BYTE switchFlag)
{

	ResetPibInputs(devAddress,switchFlag);

#ifdef EMULATE_PIB
	EmulatePIB_ResetInputs(devAddress,switchFlag);
#endif

}



//******************************************************************************
//
//  Function: EmulatePIB_SetPIBSolenoid
//
//  Arguments: BYTE solCtrl
//
//  Returns: none
//
//  Description: used to emulate PIB board - it sets the PIB solenoid status internally bypassing the PIB ccommunication.
//
//******************************************************************************
void EmulatePIB_SetPIBSolenoid(BYTE solCtrl)
{
	sPibParams.solenoidStatus = solCtrl;
	if(solCtrl)
	{
		sPibParams.contactStatus |=1<<4;
	}
	else
	{
		sPibParams.contactStatus &=~(1<<4);
	}
}


//******************************************************************************
//
//  Function: EmulatePIB_SetPibContacts
//
//  Arguments: BYTE solCtrl
//
//  Returns: none
//
//  Description: used to emulate PIB board - it sets the PIB contact status internally bypassing the PIB ccommunication.
//
//******************************************************************************
void EmulatePIB_SetPibContacts(BYTE bmContacts)
{
	sPibParams.contactStatus = bmContacts;
}


//******************************************************************************
//
//  Function: EmulatePIB_SetPIBSolenoid
//
//  Arguments: BYTE solCtrl
//
//  Returns: none
//
//  Description: used to emulate PIB board - it sets the PIB solenoid status internally bypassing the PIB ccommunication.
//
//******************************************************************************
void EmulatePIB_ResetInputs(BYTE devAddress, BYTE switchFlag)
{

	sPibParams.contactStatus &=~(1<<4);

}



BYTE CalculateCRC(BYTE *bPointer, WORD wLength)
{
	const WORD c1 = 52845;
    const WORD c2 = 22719;
    WORD r;
    WORD i;
    DWORD sum;
	BYTE cipher;

	r = 55665;
	sum = 0;

	for( i=0; i<wLength;i++)
	{
		cipher = ( *bPointer ^ ( r >> 8 ) );
		r = ( cipher + r ) * c1 + c2;
		sum += cipher;
		bPointer++;
	}

	return (BYTE)sum;
}



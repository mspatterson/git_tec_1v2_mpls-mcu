//******************************************************************************
//
//  WTTTSProtocolUtils.h:
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-     		KM          Initial Implementation
//	2013-Sep-18 	DD			Update RFC packet structure
//	2013-Nov-04		DD 			Update RFC packet structure - RFC now reports voltage and mA/h used.
//*******************************************************************************


#ifndef WTTTSProtocolUtilsH
#define WTTTSProtocolUtilsH

#include "drivers.h"



//
// Packet Definitions for WTTTS V2 Communications
// Refer to Specification 153-0084 for further info
//
// Note: the capability exists to perform over-the-air updating of
// the WTTTS firmware. This capability is only implemented in a
// specialized software program. Therefore, all definitions related
// to that ability have been excluded from this file.
//


// Packet formats (inbound and outbound). All structures are byte-aligned
//#pragma pack(push, 1)

// Packet header
typedef struct {
    BYTE pktHdr;       // WTTTS_HDR_... define
    BYTE pktType;      // WTTTS_CMD_... or WTTTS_RESP_... define
    BYTE seqNbr;       // Incremented on each tx packet. Rolls over to zero at 255
    BYTE dataLen;      // Number of bytes of packet data to follow; can be zero
    WORD timeStamp;    // Packet time, in 10's of msec, since device power-up
} WTTTS_PKT_HDR;


// Stream data packet data
// Sep 09 2015 - added ADC_SWITCH_IN
typedef struct {
    BYTE torque000[3];
    BYTE torque180[3];
    BYTE tension000[3];
    BYTE tension090[3];
    BYTE tension180[3];
    BYTE tension270[3];
    BYTE gyro[4];
    BYTE compassX[2];
    BYTE compassY[2];
    BYTE compassZ[2];
    BYTE accelX[2];
    BYTE accelY[2];
    BYTE accelZ[2];
//    BYTE adcSwIn[3];
} WTTTS_STREAM_DATA;

// Request for command packet
#define NBR_WTTTS_PRESSURE_BYTES  3

typedef struct {
	BYTE  temperature; // signed value, 0.5C per count with, -64C to +63.5C
	BYTE  battType;    // 1 = lithium, 2 = NiMH
	WORD  battVoltage;	//Voltage in mV (eg 3.742V would be 3742)
	WORD  battUsed;    // Value returns number of mA/h used since last time the battery was inserted
	BYTE  pressure[NBR_WTTTS_PRESSURE_BYTES];	//24 bit raw A/D value for pressure.
	BYTE  rpm;         // current RPM
	BYTE  lastReset;   // 1 = power-up, 2 = WDT, 3 = brownout reset
	BYTE  rfChannel;   // 11, 15, 20, 25
	BYTE  currMode;    // 0 - entering deep sleep, 1 = normal, 2 = low-power
	BYTE  rfu;
	WORD  rfcRate;
	WORD  rfcTimeout;
	WORD  streamRate;
	WORD  streamTimeout;
	WORD  pairingTimeout;
	BYTE torque000[3];
	BYTE torque180[3];
	BYTE tension000[3];
	BYTE tension090[3];
	BYTE tension180[3];
	BYTE tension270[3];
	BYTE compassX[2];
	BYTE compassY[2];
	BYTE compassZ[2];
	BYTE accelX[2];
	BYTE accelY[2];
	BYTE accelZ[2];
} WTTTS_RFC_PKT;


// Payload for set rate command.
typedef enum {
    SRT_RFC_RATE,                   // Sets RFC tx rate, in msecs
    SRT_RFC_TIMEOUT,                // Sets how long the WTTTS receiver remains active after sending a RFC
    SRT_STREAM_RATE,                // Sets the rate at which stream packets are reported
    SRT_STREAM_TIMEOUT,             // Sets how long the WTTTS will send stream packets before automatically stopping
    SRT_PAIR_TIMEOUT,               // Sets how long before the WTTTS enters 'deep sleep' if it can't connect
	SRT_SOLENOID_TIMEOUT,			// Used by PIB board to determine the max time a solenoid can be in the active state
    NBR_SET_RATE_TYPES
} WTTTS_RATE_TYPE;

typedef struct {
    BYTE  rateType;                 // One of the SRT_ enums from above
    BYTE  rfu;                      // Must always be zero
    WORD  newValue;                 // Value in secs or msecs, see enum defs above
} WTTTS_RATE_PKT;

// The following payloads are used to get / set configuration information.
// Configuration data is stored in in pages. Each page consists of 16 bytes
// of data. All pages can stored in the WTTTS can be read. However, the host
// can only write to a subset of all pages.
#define NBR_WTTTS_CFG_PAGES        32
#define WTTTS_CFG_FIRST_HOST_PAGE  16
#define NBR_BYTES_PER_WTTTS_PAGE   16

#define WTTTS_PG_RESULT_SUCCESS    0
#define WTTTS_PG_RESULT_BAD_PG     1

typedef struct {
    BYTE pageNbr;
} WTTTS_CFG_ITEM;

typedef struct {
    BYTE pageNbr;
    BYTE result;    // Always zero host to WTTTS; from WTTTS: 0 = success, 1 = invalid page
    BYTE pageData[NBR_BYTES_PER_WTTTS_PAGE];
} WTTTS_CFG_DATA;

// Unit version information.
#define WTTTS_FW_VER_LEN   4

typedef struct {
    BYTE hardwareSettings;          // Physical jumper settings on sub
    BYTE fwVer[WTTTS_FW_VER_LEN];   // Firmware version info
} WTTTS_VER_PKT;

// Set RF Channel command
typedef struct {
    BYTE chanNbr;     // 11 through 26
} WTTTS_SET_CHAN_PKT;

// All packet payloads are defined in the following union
typedef union {
    WTTTS_STREAM_DATA  streamData;
    WTTTS_RFC_PKT      rfcPkt;
    WTTTS_CFG_DATA     cfgData;
    WTTTS_CFG_ITEM     cfgRequest;
    WTTTS_VER_PKT      verPkt;
    WTTTS_RATE_PKT     ratePkt;
    WTTTS_SET_CHAN_PKT chanPkt;
} WTTTS_DATA_UNION;

//#pragma pack(pop)


#define SIZEOF_WTTTS_CHECKSUM  1

#define MIN_WTTTS_PKT_LEN      ( sizeof( WTTTS_PKT_HDR ) + SIZEOF_WTTTS_CHECKSUM )
#define MAX_WTTTS_PKT_LEN      ( sizeof( WTTTS_PKT_HDR ) + sizeof( WTTTS_DATA_UNION ) + SIZEOF_WTTTS_CHECKSUM )


// Packet header defines
//#define WTTTS_HDR_RX   0x29       // inbound from WTTTS
#define WTTTS_HDR_TX   0x46       // outbound to WTTTS
#define MPL_HDR_FROM_HOST	0x81	// packet sent by Host
#define MPL_HDR_FROM_DEVICE	0x15	// packet sent by Device


// Packet type defintions: outbound to WTTTS.
#define WTTTS_CMD_NO_CMD           0x80
#define WTTTS_CMD_START_STREAM     0x82
#define WTTTS_CMD_STOP_STREAM      0x84
#define WTTTS_CMD_SET_RATE         0x86
#define WTTTS_CMD_QUERY_VER        0x8A
#define WTTTS_CMD_ENTER_DEEP_SLEEP 0x8C
#define WTTTS_CMD_SET_RF_CHANNEL   0x8E
#define WTTTS_CMD_SET_CFG_DATA     0xA0
#define WTTTS_CMD_GET_CFG_DATA     0xA2


// Packet type definitions: inbound from WTTTS
#define WTTTS_RESP_STREAM_DATA     0x24
#define WTTTS_RESP_REQ_FOR_CMD     0x26
#define WTTTS_RESP_VER_PKT         0x28
#define WTTTS_RESP_CFG_DATA        0x2A


// packet type definitions for Multi-Plug Launcher
#define MPL_CMD_GET_MIN_MAX			0xA4
#define MPL_CMD_CHANNEL_IN_USE		0xA6
#define MPL_CMD_SET_PIB_CONTACTS	0xA8
#define MPL_CMD_SET_PIB_SOLENOID	0xAA
#define MPL_CMD_SET_SENS_PARAM		0xAC	// Sets magnetometer sampling rate and averaging parameters.
#define MPL_CMD_RESET_PIB_INPUTS    0xAE	// Clears the �set� indication on either a PIB board switch input or a sensor board digital input.
#define MPL_RESP_RFC_PIB			0x2E
#define MPL_RESP_MIN_MAX_PIB		0x22

//// 64 bytes
//typedef struct {
//	BYTE temperature; // signed value, 0.5C per count with, -64C to +63.5C
//	BYTE battType;    // 1 = lithium, 2 = NiMH
//	WORD battVoltage;	//Voltage in mV (eg 3.742V would be 3742)
//	WORD battUsed;    // Value returns number of mA/h used since last time the battery was inserted
//	BYTE pressure[NBR_WTTTS_PRESSURE_BYTES];	//24 bit raw A/D value for pressure.
//	BYTE rpm;         	// current RPM
//	BYTE lastReset;   	// 1 = power-up, 2 = WDT, 3 = brownout reset
//	BYTE rfChannel;   	// 11, 15, 20, 25
//	BYTE currMode;    	// 0 - entering deep sleep, 1 = normal, 2 = low-power
//	BYTE contactState;	//bit flags indicating the state of the output contacts
//	BYTE inputState;	//bit flags indicating the state of the digital inputs
//	BYTE solenoidState;	//value indicating which,if any, of the solenoids are currently active.
//	BYTE rfuArray[4];	// reserved for future use - 4 bytes
//	WORD rfcRate;
//	WORD rfcTimeout;
//	WORD solenoidTimeout;	// ms, denotes how long a solenoid will stay closed after receiving a "close command"
//	BYTE sensorSampleRate;	// magnetometer sampling rate
//	BYTE sensorAveraging;	// magnetometer averaging value.
//	WORD pairingTimeout;
//	BYTE deviceID[6];		// unique identifier
//	BYTE rptSensors[4];		// 4 byte values that contain the sensor numbers being reported in the following fields.
//	WORD sens1AvgValueX;
//	WORD sens1AvgValueY;
//	WORD sens1AvgValueZ;
//	WORD sens2AvgValueX;
//	WORD sens2AvgValueY;
//	WORD sens2AvgValueZ;
//	WORD sens3AvgValueX;
//	WORD sens3AvgValueY;
//	WORD sens3AvgValueZ;
//	WORD sens4AvgValueX;
//	WORD sens4AvgValueY;
//	WORD sens4AvgValueZ;
//} MPL_RFC_PKT;
// 64 bytes
typedef struct {
	BYTE temperature; // signed value, 0.5C per count with, -64C to +63.5C
	BYTE battType;    // 1 = lithium, 2 = NiMH
	WORD battVoltage;	//Voltage in mV (eg 3.742V would be 3742)
	WORD battUsed;    // Value returns number of mA/h used since last time the battery was inserted
	BYTE pressure[NBR_WTTTS_PRESSURE_BYTES];	//24 bit raw A/D value for pressure.
	BYTE rpm;         	// current RPM
	BYTE lastReset;   	// 1 = power-up, 2 = WDT, 3 = brownout reset
	BYTE rfChannel;   	// 11, 15, 20, 25
	BYTE currMode;    	// 0 - entering deep sleep, 1 = normal, 2 = low-power
	BYTE contactState;	//bit flags indicating the state of the output contacts
	BYTE inputState;	//bit flags indicating the state of the digital inputs
	BYTE solenoidState;	//value indicating which,if any, of the solenoids are currently active.
	BYTE rfuArray[4];	// reserved for future use - 4 bytes
	WORD rfcRate;
	WORD rfcTimeout;
	WORD solenoidTimeout;	// ms, denotes how long a solenoid will stay closed after receiving a "close command"
	BYTE sensorSampleRate;	// magnetometer sampling rate
	BYTE sensorAveraging;	// magnetometer averaging value.
	WORD pairingTimeout;
	BYTE deviceID[6];		// unique identifier
	BYTE rptSensors[4];		// 4 byte values that contain the sensor numbers being reported in the following fields.
	WORD sens1AvgValueX;
	WORD sens2AvgValueX;
	WORD sens3AvgValueX;
	WORD sens4AvgValueX;
	WORD sens1AvgValueY;
	WORD sens2AvgValueY;
	WORD sens3AvgValueY;
	WORD sens4AvgValueY;
	WORD sens1AvgValueZ;
	WORD sens2AvgValueZ;
	WORD sens3AvgValueZ;
	WORD sens4AvgValueZ;
} MPL_RFC_PKT;


typedef struct {
	BYTE rptSensors[4];		// 4 byte values that contain the sensor numbers being reported in the following fields.
	WORD sens1AvgValueXmin;
	WORD sens1AvgValueXmax;
	WORD sens1AvgValueYmin;
	WORD sens1AvgValueYmax;
	WORD sens1AvgValueZmin;
	WORD sens1AvgValueZmax;
	WORD sens2AvgValueXmin;
	WORD sens2AvgValueXmax;
	WORD sens2AvgValueYmin;
	WORD sens2AvgValueYmax;
	WORD sens2AvgValueZmin;
	WORD sens2AvgValueZmax;
	WORD sens3AvgValueXmin;
	WORD sens3AvgValueXmax;
	WORD sens3AvgValueYmin;
	WORD sens3AvgValueYmax;
	WORD sens3AvgValueZmin;
	WORD sens3AvgValueZmax;
	WORD sens4AvgValueXmin;
	WORD sens4AvgValueXmax;
	WORD sens4AvgValueYmin;
	WORD sens4AvgValueYmax;
	WORD sens4AvgValueZmin;
	WORD sens4AvgValueZmax;
} MPL_MIN_MAX_PKT;

typedef struct {
	BYTE resetMinMax;
}MPL_GET_MIN_MAX_PKT;

typedef struct {
	BYTE contactState;
}MPL_SET_PIB_CONTACTS_PKT;


typedef struct {
	BYTE solNumber;
	BYTE solState;
}MPL_SET_PIB_SOLENOID_PKT;

typedef struct {
	BYTE samplingRate;
	BYTE avgFactor;
}MPL_SET_SENS_PARAM_PKT;

typedef struct {
	BYTE devAddress;
	BYTE switchFlag;
}MPL_RESET_INPUTS_PKT;

void UpdateTimeParamsFromFlash(void);

BOOL SetTimeParameter(BYTE paramNum, WORD paramVal);

WORD GetTimeParameter(const WTTTS_RATE_TYPE paramNum);

void SendCfgEvent(void);

void SendRfc(void);

BOOL HaveWtttsPacket(BYTE *pBuff, WORD buffLen);

void ExecuteCommand(void);

void SendVersionInfo(void);

void SetNoCommand(BOOL bVal);

void SetStartStream(BOOL bVal);

void SetStopStream(BOOL bVal);

BOOL GetStartStreamStatus(void);

BOOL GetStopStreamStatus(void);

void SetPowerDown(BOOL bVal);

BOOL GetPowerDownStatus(void);

BYTE GetCurrMode(void);

void SendStreamData(void);

void ConvertLongTo3Bytes(signed long slValue, BYTE *pData);

void ConvertLongTo4Bytes(signed long slValue, BYTE *pData);

BOOL GetCommandStatus(void);

BOOL GetRadioPacketStatus(void);

void ClearRadioPacketStatus(void);

BOOL IsRadioBusy(void);

void SetRadioBusy(BOOL bValue);

void SaveSerNumber(BYTE *pData);

void mplSendMinMax(void);

void mplSetPibContacts(BYTE bmContacts);

void mplSetPibSolenoid(BYTE nbrSolenoid, BYTE stateSolenoid);

void mplSetPibParameters(BYTE mSampleRate, BYTE mAveragingFactor);

void mplSendRfcPIB(void);

void* getPibParamAddr(void);

void* getPTSParamAddr(BYTE nbrPts);

void mplResetPibInput(BYTE devAddress, BYTE switchFlag);

void EmulatePIB_SetPIBSolenoid(BYTE solCtrl);
void EmulatePIB_SetPibContacts(BYTE bmContacts);
void EmulatePIB_ResetInputs(BYTE devAddress, BYTE switchFlag);

BYTE CalculateCRC(BYTE *bPointer, WORD wLength);

#endif




//******************************************************************************
//
//  BatteryGas.c: Module Title
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module contains the code to read and write the I2C.
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-Mar-14     CT          Initial Implementation
//
//*******************************************************************************

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "BatteryGas.h"



//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------
#define RUN_MODE         0x10
#define RESET_CHARGE_ACC 0x02
#define POWER_DOWN       0x00

typedef enum
{
    //Control Registers
    REG_MODE = 0,
    REG_CTRL = 1,
    REG_CHARGE_LOW = 2,
    REG_CHARGE_HIGH = 3,
    REG_COUNTER_LOW = 4,
    REG_COUNTER_HIGH = 5,
    REG_CURRENT_LOW = 6,
    REG_CURRENT_HIGH = 7,
    REG_VOLTAGE_LOW = 8,
    REG_VOLTAGE_HIGH = 9,
    REG_TEMP_LOW = 10,
    REG_TEMP_HIGH = 11,

    //Device ID Registers
    REG_ID0 = 24,
    REG_ID1 = 25,
    REG_ID2 = 26,
    REG_ID3 = 27,
    REG_ID4 = 28,
    REG_ID5 = 29,
    REG_ID6 = 30,
    REG_ID7 = 31,

    //RAM Registers
    //#define REG_RAM0  32
    //...
    //#define REG_RAM31 63

    NUM_BATTGAS_REGS
} BatteryGas_Reg;


// Converting Battery level to display indicator:
//===============================================================================
// Gas gauge indicates 'full' at 65536 (0xffff) reading from the battery charge
// accumulator. Now:
//
// Accumulator Resolution:  0.6mAh
// total battery charge:    2500 mAh
// So,  2500/0.6 ~ 4200 steps from 'full' to 'empty'
//
// 4200/100 = 42 steps for each percentage point in the batt. charge
//===============================================================================
#define MAX_BATT_CHARGE 0xFFFF
#define BATT_CHARGE_STEPS_AS_ONE_PERCENT 42

#define BATT_MAX_CHARGE_LEVEL 65535
#define BATT_MIN_CHARGE_LEVEL 61335
#define MID_POINT_UNUSED_RANGE ( BATT_MIN_CHARGE_LEVEL  / 2 )


//******************************************************************************
// Batt Voltage Monitoring:
//
// STC3100 Resolution: 2.44 mV / LSB
//
// Undervoltage Lockout: Set to 2.75 V
//
//   3.1 V
// ----------  = 1270
// 2.44 mV/LSB
//
//
//******************************************************************************
#define UV_CNT_LIMIT     4
#define UVLOW_THRESHOLD  1270
#define CHARGE_VOLTAGE_MAX 1434

//static  BYTE byUVEventCount = 0;
//static  DWORD dwInitCount   = 0;
static BYTE batLife = 0;
static WORD wBatLife = 0;

GAS_GAUGE_DATA sGasGaugeData;

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS
//------------------------------------------------------------------------------
//static BOOL ReadBatteryGasReg ( BatteryGas_Reg regAddr, BYTE* pData );
//static BOOL WriteBatteryGasReg( BatteryGas_Reg regAddr, BYTE Data );
//static BYTE convertBatteryChargeToPercent( WORD wBattChargeLevel );
//static BOOL ReadBatteryGasRegChargeLevel (WORD* ptrResult );


//******************************************************************************
//
//  Function:
//
//  Arguments:
//
//  Returns: TRUE if successful, else FALSE
//
//  Description: Reads a single register.
//
//******************************************************************************
void InitBatteryGasMonitor( void )
{
    BYTE data;
    data = RUN_MODE;
    //switch from standby to run mode
    STC3100WriteRegs(REG_MODE, &data, 2 );

}

void BatteryGasMonitorReadAllRegs(void)
{
	STC3100I2cRead(REG_MODE, 12);
}

void BatteryGasMonitorReadId(void)
{
	STC3100I2cRead(REG_ID0, 8);
}

void ResetBatteryGasMonitor( void )
{
    BYTE pByte[4];

    pByte[0] = RUN_MODE;
    pByte[1] = RESET_CHARGE_ACC;
    //switch from standby to run mode
    STC3100WriteRegs(REG_MODE, pByte,3 );

}

void PowerDownBatteryGasMonitor( void )
{
    BYTE bData;

    bData = POWER_DOWN;
    //switch to standby mode
    STC3100WriteRegs(REG_MODE, &bData, 2 );
}

//******************************************************************************
//
//******************************************************************************
//BOOL getBattVoltage( WORD* ptrBattVoltage )
//{

//    WORD wResult;
//    WORD wLength;
//    BYTE byAddr;
//    BYTE byVoltageLow;
//    BYTE byVoltageHigh;
//    BOOL bResult;
//    BOOL bReadGood;
//
//    bReadGood = TRUE;
//    wLength = 1;
//
//    // read the low byte
//    byAddr  = REG_VOLTAGE_LOW;
//    bResult = STC3100I2cRead( byAddr, &byVoltageLow, wLength );
//    if( !bResult)
//    {
//        bReadGood = FALSE;
//    }
//
//    // read the high byte
//    byAddr  = REG_VOLTAGE_HIGH;
//    bResult = STC3100I2cRead( byAddr, &byVoltageHigh, wLength );
//    if( !bResult)
//    {
//        bReadGood = FALSE;
//    }
//
//
//    wResult = byVoltageHigh;
//    wResult <<= 8;
//    wResult |= byVoltageLow;
//
//    *ptrBattVoltage = wResult;
//    return bReadGood;

//}




//******************************************************************************
//
//  Function:  UpdateBatteryGasMonitor
//
//  Arguments: None
//
//  Returns:   TRUE if successful, else FALSE
//
//  Description: Checks on the battery state of charge and returns this value;
//               the value returned will range from 0 to 5; 0 indicates no
//               charge, 5 indicates fully charged
//
//******************************************************************************
//BOOL UpdateBatteryGasMonitor( BYTE* ptrNewChargeLevel )
//{
//
//    BOOL bBattVoltValid;
//    WORD wBattVoltage;
//	BYTE byChargeLevelInPercent;
//
//    // Take a reading of the current charge level
//    WORD wChargeLevel;
//
//
//
//    if( !ReadBatteryGasRegChargeLevel( &wChargeLevel ) )
//    {
//        *ptrNewChargeLevel = 0;
//        return FALSE;
//    }
//
//    //Check 1
//    //
//    // test the charge level that we get from the coulomb counter for
//    // validity. By this we mean: does it fall into the range we expect.
//    // The reason it may not fall into this range is simple: the first time
//    // the chip powers up it sets the battery charge level at 65535; it
//    // then overflows to '0' if charge is flowing into the battery, or
//    // it starts decrementing if charge is flowing out of the battery. So,
//    // if you plug in an almost fully charged battery, it will power up
//    // to 65535, then the count will over flow to '0', and keep incrementing
//    // until the charger stops. At which point you'll have a very low
//    // value, which actually indicates a full battery.
//    if( wChargeLevel < MID_POINT_UNUSED_RANGE  )
//    {
//        // we must reset the charge level to the default so that the battery
//        // looks full.
//
//        // read the low byte
//        BYTE byAddr = REG_CTRL;
//        STC3100WriteRegs ( byAddr, RESET_CHARGE_ACC );
//
//        // over-ride the read value
//        wChargeLevel = 65535;
//    }
//
//    // Check 2
//    //
//    // We also check the battery voltage; we do this so that we can recover
//    // from cases where the coulomb counter has lost synchronization with
//    // the Battery. We do this as follows:
//    // *) monitor the battery voltage
//    // *) if it goes to 3.5V then the battery is very close to fully charged -
//    // *)      reset the coulomb counter to full charge in this case
//    bBattVoltValid = getBattVoltage( &wBattVoltage );
//    if( bBattVoltValid )
//    {
//        if( wBattVoltage > CHARGE_VOLTAGE_MAX )
//        {
//            // read the low byte
//            BYTE byAddr = REG_CTRL;
//            STC3100WriteRegs ( byAddr, RESET_CHARGE_ACC );
//
//            // over-ride the read value
//            wChargeLevel = 65535;
//
//        }
//
//    }
//
//
//    // convert the charge level reading into the range that is displayed by the
//    // system: 0 for no charge; 5 for fully charged
//    byChargeLevelInPercent = convertBatteryChargeToPercent( wChargeLevel );
//
//    *ptrNewChargeLevel = byChargeLevelInPercent;
//
//    return TRUE;
//}

//
//BYTE convertBatteryChargeToPercent( WORD wBattChargeLevel )
//{
//	BYTE byPercentLeft;
//    int iChargeLost = (int)(MAX_BATT_CHARGE - wBattChargeLevel);
//
//    BYTE byPercentPointsLost = 0;
//
//    while( (iChargeLost > BATT_CHARGE_STEPS_AS_ONE_PERCENT) && ( byPercentPointsLost < 100 ) )
//    {
//        iChargeLost -= BATT_CHARGE_STEPS_AS_ONE_PERCENT;
//        byPercentPointsLost++;
//    }
//
//    byPercentLeft = 100 - byPercentPointsLost;
//
//    return byPercentLeft;
//}


//******************************************************************************
// Private Functions
//******************************************************************************

//******************************************************************************
//
//  Function:    ReadBatteryGasRegChargeLevel
//
//  Arguments:
//
//  Returns:     BOOL - True or False according to read result
//
//  Description: Reads a single register.
//
//******************************************************************************
//BOOL ReadBatteryGasRegChargeLevel (WORD* ptrResult )
//{
//    WORD wLength;
//    BYTE byAddr;
//    BOOL bResult;
//
//    BYTE byChargeLow;
//    BYTE byChargeHigh;
//    WORD wResult;
//
//    wLength = 1;
//
//    // read the low byte
//    byAddr  = REG_CHARGE_LOW;
//    bResult = STC3100I2cRead( byAddr, &byChargeLow, wLength );
//
//    if( !bResult)
//         return FALSE;
//
//    // read the high byte
//    byAddr  = REG_CHARGE_HIGH;
//    bResult = STC3100I2cRead( byAddr, &byChargeHigh, wLength );
//
//    if( !bResult)
//         return FALSE;
//
//    wResult = byChargeHigh;
//    wResult <<= 8;
//    wResult |= byChargeLow;
//
//    *ptrResult = wResult;
//
//    return TRUE;
//}


//******************************************************************************
//
//  Function:  ReadBatteryGasReg
//
//  Arguments:
//
//  Returns:  TRUE if successful, else FALSE
//
//  Description: use to reads a single register location
//
//******************************************************************************
//BOOL ReadBatteryGasReg( BatteryGas_Reg regAddr, BYTE* pData )
//{
//    WORD wLength = 1;
//
//    if ( !STC3100I2cRead( regAddr, pData, wLength ) )
//        return FALSE;
//
//    return TRUE;
//}


//******************************************************************************
//
//  Function: WriteBatteryGasReg
//
//  Arguments:

//
//  Returns: TRUE if successful, else FALSE
//
//  Description: Writes a single register.
//
//******************************************************************************
//BOOL WriteBatteryGasReg( BatteryGas_Reg regAddr, BYTE regVal )
//{
//    if ( !STC3100WriteReg ( regAddr, regVal ) )
//        return FALSE;
//
//    return TRUE;
//}



//******************************************************************************
//
//******************************************************************************
//void ResetBattUnderVoltageMonitor( void )
//{
//    byUVEventCount = 0;
//    dwInitCount    = 0;
//}






//******************************************************************************
//
//******************************************************************************
//void updateBattUnderVoltageMonitor( BOOL* ptrBattUVLOIsValid, BOOL* ptrBattOk )
//{
//
//
//    WORD wBattVoltage;
//    BOOL bBattReadValid;
//
//    bBattReadValid = getBattVoltage( &wBattVoltage);
//    if(!bBattReadValid)
//    {
//        *ptrBattUVLOIsValid = FALSE;
//        *ptrBattOk          = TRUE;
//    }
//
//    // increment the 'init' counter which we use to check the number of samples
//    // that have been processed; we do this to determine if we have had enough
//    // samples to make a judgement on the battery:
//    dwInitCount++;
//    if( dwInitCount <  UV_CNT_LIMIT )
//    {
//        // we haven't taken enough samples to know what the battery state really
//        // is; therefore, we indicate that the UVLO is not yet valid
//        *ptrBattUVLOIsValid = FALSE;
//        *ptrBattOk          = TRUE;
//        return;
//
//    }
//
//    // now: we have taken enough samples to know if we are undervoltage or
//    //      otherwise:
//    *ptrBattUVLOIsValid = TRUE;
//
//    if( wBattVoltage <  UVLOW_THRESHOLD )
//    {
//        // The battery is below the UVLOW threshold! Increment the count and check
//        // to see if we have had the minimum number of consecutive undervoltage events
//        // to indicate 'battery not OK'
//        byUVEventCount++;
//        if( byUVEventCount >= UV_CNT_LIMIT )
//        {
//            *ptrBattOk          = FALSE;
//        }
//    }
//    else
//    {
//        // we are not undervoltage; therefore, we reset the undervoltage event counter
//        byUVEventCount = 0;
//        *ptrBattOk          = TRUE;
//    }
//}


BYTE GetBatLife(void)
{
	return batLife;
}


void ParseGasGaugeData(BYTE *pData)
{
	WORD wTmp;
	sGasGaugeData.ChargeDataRaw = ((WORD)pData[3]<<8)|pData[2];
	sGasGaugeData.CurrentValue = ((WORD)pData[7]<<8)|pData[6];
	sGasGaugeData.NumberOfConversion = ((WORD)pData[5]<<8)|pData[4];
	sGasGaugeData.BatVoltageMv = (WORD)((((DWORD)pData[9]<<8)|pData[8])*244 /100);
//	sGasGaugeData.TempMilliDegree = (((WORD)pData[11]<<8)|pData[10]) * 125;
	sGasGaugeData.ChargeDataMa = sGasGaugeData.ChargeDataRaw *(-2)/10;

//	batLife = sGasGaugeData.ChargeDataMa % 100;
	wTmp = sGasGaugeData.BatVoltageMv /20;		// show batLife as voltage in 20mV steps
	batLife = (BYTE)wTmp;
	wBatLife = sGasGaugeData.ChargeDataMa;
}


WORD GetBatUsedmAh(void)
{
	return wBatLife;
}


WORD GetBatVoltageGasGauge(void)
{
	return sGasGaugeData.BatVoltageMv;
}



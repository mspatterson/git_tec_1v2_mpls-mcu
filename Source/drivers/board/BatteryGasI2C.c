//******************************************************************************
//
//  BatteryGasI2C.c
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module contains the code to read and write the STC3100
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-Mar-12     JK          Adapted from Codec_I2C
//
//*******************************************************************************

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

#include "BatteryGasI2C.h"

 

#define STC3100_PORTNUM       2
#define STC3100_DEV_ADDR_W    0xE0
#define STC3100_DEV_ADDR_R    0xE1

#define STC3100_DEV_BIT_RATE  90000       //400 kHz max (fast), 100 kHz (standard mode)
#define STC3100_ADDRESS			0x70

#define BAT_GAUGE_BUF_SIZE		20

I2C_OBJECT	i2cBatGauge;
BYTE RxBatGaugeBuffer[BAT_GAUGE_BUF_SIZE];
BYTE TxBatGaugeBuffer[BAT_GAUGE_BUF_SIZE];

//******************************************************************************
//
//  Function: I2cInit
//
//  Arguments: void
//
//  Returns: TRUE if successful, else FALSE
//
//  Description: Initialize the I2C CODEC.
//
//******************************************************************************
//BOOL STC3100I2cInit( void )
//{
//
//	i2cBatGauge.bySlaveAddress = STC3100_ADDRESS;
// 	i2cBatGauge.ptrTxQ = TxBatGaugeBuffer;
//   	i2cBatGauge.wTxQIndex = 0;
//    i2cBatGauge.wTxQSize = 4;
//    i2cBatGauge.bTxQSending = TRUE;
//    i2cBatGauge.ptrRxQ = RxBatGaugeBuffer;
//    i2cBatGauge.wRxQIndex = 0;
//    i2cBatGauge.wRxQSize = 0;
//    i2cBatGauge.bRxQReceiving = FALSE;
//
//    // the MSB of the SUB address has to be '1' to allow multiple data read/write
//    TxBatGaugeBuffer[0] = 0x80;		// address of the first config register
//	TxBatGaugeBuffer[1] = DATA_RATE_220_HZ;		// CRA_REG_M register data
//	TxBatGaugeBuffer[2] = INPUT_RANGE_1P3_GAUSS;	// CRB_REG_M register data
//	TxBatGaugeBuffer[3] = CONT_MODE;				// MR_REG_M	register data
//
//	i2cCompassWrite(&i2cBatGauge);
//
//    // no initialization required
//    return( TRUE );
//}



//******************************************************************************
//
//  Function: STC3100I2cBusy
//
//  Arguments: none
//
//  Returns: TRUE if  is busy (internal write), else FALSE if ready for new write
//
//  Description: Checks if  is busy with internal write.
//
//******************************************************************************
BOOL STC3100I2cBusy( void )
{
    // write to  with no address or data, if success  is not busy
//    if( I2cWrite( STC3100_PORTNUM, STC3100_DEV_ADDR_W, NULL, 0, TRUE ) == I2C_WRITE_SUCCESS )
//    {
//        return( FALSE );
//    }

    // fall through means  is busy
    return( TRUE );
}


//******************************************************************************
//
//  Function: STC3100WriteReg
//
//  Arguments: regAddr - register address
//             regVal - write data
//
//  Returns: TRUE if all data was written, else FALSE
//
//  Description: Writes data to the power manager.
//
//******************************************************************************
BOOL STC3100WriteRegs (BYTE regAddr, BYTE *pRegVal , BYTE length)
{
	unsigned int i;

    if( (i2cBatGauge.bTxQSending == TRUE)
    	||
    	(i2cBatGauge.bRxQReceiving == TRUE) )
		return FALSE;

	i2cBatGauge.bySlaveAddress = STC3100_ADDRESS;
 	i2cBatGauge.ptrTxQ = TxBatGaugeBuffer;
   	i2cBatGauge.wTxQIndex = 0;                              
    i2cBatGauge.wTxQSize = length;
    i2cBatGauge.bTxQSending = TRUE;
    i2cBatGauge.ptrRxQ = RxBatGaugeBuffer;
    i2cBatGauge.wRxQIndex = 0;                              
    i2cBatGauge.wRxQSize = 0;
    i2cBatGauge.bRxQReceiving = FALSE;



    TxBatGaugeBuffer[0] = regAddr;		// address of the first config register	
    for(i=0;i<(length-1);i++)
    {
    	TxBatGaugeBuffer[i+1] = *pRegVal;		// CRA_REG_M register data
    	pRegVal++;								// increase the pointer
    }
	
	
    i2cCompassWrite(&i2cBatGauge);

    return TRUE;
}



//******************************************************************************
//
//  Function: I2cRead
//
//  Arguments: regAddr - location in  to start read from
//             Length - number of bytes to read
//
//  Returns: TRUE if requested length was read, else FALSE
//
//  Description: Reads requested number of bytes from the .  If the length
//               goes past the last address, it will wrap around to 0.
//
//******************************************************************************
BOOL STC3100I2cRead( BYTE regAddr, WORD Length )
{

	if((i2cBatGauge.bTxQSending == TRUE)
			||
			(i2cBatGauge.bRxQReceiving == TRUE))
		return FALSE;

	memset(RxBatGaugeBuffer, 0,sizeof(RxBatGaugeBuffer));

	i2cBatGauge.bySlaveAddress = STC3100_ADDRESS;
	i2cBatGauge.ptrTxQ = TxBatGaugeBuffer;
	i2cBatGauge.wTxQIndex = 0;
	i2cBatGauge.wTxQSize = 1;
	i2cBatGauge.bTxQSending = TRUE;
	i2cBatGauge.ptrRxQ = RxBatGaugeBuffer;
	i2cBatGauge.wRxQIndex = 0;
	i2cBatGauge.wRxQSize = Length;
	i2cBatGauge.bRxQReceiving = TRUE;

	TxBatGaugeBuffer[0] = regAddr;		// address of the first config register
	i2cCompassRead(&i2cBatGauge);

	return TRUE;
}

void STC3100GetRawData(BYTE *pData, WORD maxSize)
{
	unsigned int i=0;
	while((i< 12)&&(i<maxSize))
	{
		pData[i] = RxBatGaugeBuffer[i];
		i++;
	}
}

BOOL STC3100DataReady(void)
{
	return (!i2cBatGauge.bRxQReceiving);
}

BOOL STC3100TxDone(void)
{
	return (!i2cBatGauge.bTxQSending);
}


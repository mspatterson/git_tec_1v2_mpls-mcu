/*
 * compass.c
 *
 *  Created on: 2013-04-09
 *      Author: DD
 */
 
#include "accel.h"

#define ACCEL_BUF_SIZE 40
I2C_OBJECT	i2cAccel;
BYTE RxAccelBuffer[ACCEL_BUF_SIZE];
BYTE TxAccelBuffer[ACCEL_BUF_SIZE];

int	AccelData[3];
/*
The accelerometer readings are a 12-bit reading "left-justified" in a 16 bit space, 
so you have to right shift it by 4. It isn't explicitly said in the datasheet, 
but you can infer it from a couple of inconsistencies in the datasheet and experimentation.
*/
 
void AccelInit(void)
{
	BYTE i=0;
	TIMERHANDLE thCompassTimer;
	thCompassTimer = RegisterTimer();
	CompassEnable();
	ResetTimer( thCompassTimer, COMPASS_POWER_UP_TIME );
	StartTimer( thCompassTimer ); 
	if(thCompassTimer)
	{
		while(!TimerExpired( thCompassTimer))
		{
			ResetWatchdog();
		}
	}
	else
	{
		ResetWatchdog();
		__delay_cycles(400000);
	}
	UnregisterTimer(thCompassTimer);

	// clear the buffers
	for(i=0;i<sizeof(RxAccelBuffer);i++)
	{
		RxAccelBuffer[i]=0;
		TxAccelBuffer[i]=0;
	}
	
	
 	i2cAccel.bySlaveAddress = ACCEL_ADDRESS;
 	i2cAccel.ptrTxQ = TxAccelBuffer;
   	i2cAccel.wTxQIndex = 0;
    i2cAccel.wTxQSize = 5;
    i2cAccel.bTxQSending = TRUE;
    i2cAccel.ptrRxQ = RxAccelBuffer;
    i2cAccel.wRxQIndex = 0;
    i2cAccel.wRxQSize = 0;
    i2cAccel.bRxQReceiving = FALSE;
    
    // the MSB of the SUB address has to be '1' to allow multiple data read/write
    TxAccelBuffer[0] = CTRL_REG1_ADDR |0x80;		// address of the first config register
	TxAccelBuffer[1] = DATA_RATE_50_HZ|Z_AXIS_EN|Y_AXIS_EN|X_AXIS_EN;		// CTRL_REG1_A register data
//	TxAccelBuffer[1] = DATA_RATE_50_HZ|LOW_POWER_EN|Z_AXIS_EN|Y_AXIS_EN|X_AXIS_EN;		// CTRL_REG1_A register data
//	TxAccelBuffer[2] = 0x00;				// CTRL_REG2_A register data
	TxAccelBuffer[2] = FILTER_EN;				// CTRL_REG2_A register data
	TxAccelBuffer[3] = 0x00;				// CTRL_REG3_A register data
	TxAccelBuffer[4] = DATA_UPDATE|BIG_ENDIAN|FULL_SCALE_8G|HIGH_RES_EN;		// CTRL_REG4_A register data
//	TxAccelBuffer[4] = DATA_UPDATE|BIG_ENDIAN;			// CTRL_REG4_A register data
//	TxAccelBuffer[4] = DATA_UPDATE|BIG_ENDIAN;			// CTRL_REG4_A register data
	
    i2cCompassWrite(&i2cAccel);
     	
}


void AccelReadDataStart(void)
{
 	i2cAccel.bySlaveAddress = ACCEL_ADDRESS;
 	i2cAccel.ptrTxQ = TxAccelBuffer;
   	i2cAccel.wTxQIndex = 0;
    i2cAccel.wTxQSize = 1;
    i2cAccel.bTxQSending = TRUE;
    i2cAccel.ptrRxQ = RxAccelBuffer;
    i2cAccel.wRxQIndex = 0;
    i2cAccel.wRxQSize = 6;
    i2cAccel.bRxQReceiving = TRUE;
    
    TxAccelBuffer[0] = OUT_X_L_REG_ADDR | 0x80;		// address of the first config register
    i2cCompassRead(&i2cAccel);
     	
}


BOOL AccelDataReady(void)
{
	return (!i2cAccel.bRxQReceiving);
}

BOOL AccelTxDone(void)
{
	return (!i2cAccel.bTxQSending);
}

// read all conffiguration registers
//CRA_REG_M (00h), CRB_REG_M (01h), MR_REG_M (02h)
void AccelRegisterDump(void)
{
	i2cAccel.bySlaveAddress = ACCEL_ADDRESS;
 	i2cAccel.ptrTxQ = TxAccelBuffer;
   	i2cAccel.wTxQIndex = 0;
    i2cAccel.wTxQSize = 1;
    i2cAccel.bTxQSending = TRUE;
    i2cAccel.ptrRxQ = RxAccelBuffer;
    i2cAccel.wRxQIndex = 0;
    i2cAccel.wRxQSize = 29;
    i2cAccel.bRxQReceiving = TRUE;
    
    TxAccelBuffer[0] = CTRL_REG1_ADDR| 0x80;		// address of the first config register
    i2cCompassRead(&i2cAccel);
}

// copy the compass data into the given buffer 
void AccelGetRawData(BYTE *pData, WORD maxSize)
{
	unsigned int i=0;
	while((i< i2cAccel.wRxQIndex)&&(i<maxSize))
	{
		pData[i] = RxAccelBuffer[i];
		i++;
	}
}


void AccelEnable(void)
{
}

void AccelDisable(void)
{

}







#ifndef ACCEL_H_
#define ACCEL_H_

#include "..\drivers.h"

#define ACCEL_ADDRESS		0x19//0X3C
#define CTRL_REG1_ADDR		0X20
#define CTRL_REG2_ADDR		0X21
#define CTRL_REG3_ADDR		0X22
#define CTRL_REG4_ADDR		0X23
#define CTRL_REG5_ADDR		0X24
#define CTRL_REG6_ADDR		0X25
#define REFER_REG_ADDR		0X26
#define STAT_REG_ADDR		0X27
#define OUT_X_L_REG_ADDR	0X28
#define OUT_X_H_REG_ADDR	0X29
#define OUT_Y_L_REG_ADDR	0X2A
#define OUT_Y_H_REG_ADDR	0X2B
#define OUT_Z_L_REG_ADDR	0X2C
#define OUT_Z_H_REG_ADDR	0X2D

#define FIFO_CTRL_REG_ADDR	0X2E
#define FIFO_SRC_REG_ADDR	0X2F
#define INT1_CFG_REG_ADDR	0X30
#define INT1_SRC_REG_ADDR	0X31
#define INT1_THS_REG_ADDR	0X32
#define INT1_DUR_REG_ADDR	0X33
#define INT2_CFG_REG_ADDR	0X34
#define INT2_SRC_REG_ADDR	0X35
#define INT2_THS_REG_ADDR	0X36
#define INT2_DUR_REG_ADDR	0X37
#define CLICK_CFG_REG_ADDR	0X38
#define CLICK_SRC_REG_ADDR	0X39
#define CLICK_THS_REG_ADDR	0X3A
#define TIME_LIMIT_REG_ADDR	0X3B
#define TIME_LATENCY_REG_ADDR	0X3C
#define TIME_WINDOW_REG_ADDR	0X3D

#define POWER_DOWN_MODE		0X00
#define DATA_RATE_1_HZ		1<<4
#define DATA_RATE_10_HZ		2<<4
#define DATA_RATE_25_HZ		3<<4
#define DATA_RATE_50_HZ		4<<4
#define DATA_RATE_100_HZ	5<<4
#define DATA_RATE_200_HZ	6<<4
#define DATA_RATE_400_H		7<<4
#define DATA_RATE_1620_HZ	8<<4
#define DATA_RATE_5376_HZ	9<<4
#define LOW_POWER_EN		1<<3
#define Z_AXIS_EN			1<<2
#define Y_AXIS_EN			1<<1
#define X_AXIS_EN			1<<0

#define DATA_UPDATE			1<<7	//output registers not updated until MSB and LSB reading
#define BIG_ENDIAN			1<<6	// 1: data MSB @ lower address
#define FULL_SCALE_2G		0<<4
#define FULL_SCALE_4G		1<<4
#define FULL_SCALE_8G		2<<4
#define FULL_SCALE_16G		3<<4
#define HIGH_RES_EN			1<<3

#define HPMSEL_0			0<<6
#define HPMSEL_1			1<<6
#define HPMSEL_2			2<<6
#define HPMSEL_3			3<<6
#define FILTER_EN			1<<3




void AccelInit(void);
void AccelReadDataStart(void);
BOOL AccelDataReady(void);
BOOL AccelTxDone(void);
void AccelRegisterDump(void);
void AccelGetRawData(BYTE *pData, WORD maxSize);
void AccelEnable(void);
void AccelDisable(void);


#endif /*ACCEL_H_*/

/*
 * compass.c
 *
 *  Created on: 2012-07-25
 *      Author: DD
 */
 
#include "compass.h"

#define COMPASS_BUF_SIZE 10 

I2C_OBJECT	i2cCompass; 
BYTE RxCompassBuffer[COMPASS_BUF_SIZE];
BYTE TxCompassBuffer[COMPASS_BUF_SIZE];
TIMERHANDLE thCompassTimer;

const BYTE CompassDataRate[]=
{
		COMP_M_ODR_800HZ,
		COMP_M_ODR_400HZ,
		COMP_M_ODR_200HZ,
		COMP_M_ODR_100HZ,
		COMP_M_ODR_50HZ,
		COMP_M_ODR_12HZ,
		COMP_M_ODR_6HZ,
		COMP_M_ODR_1HZ

};
int	CompassData[3];
 
//void CompassInit(BYTE number)
//{
//	BYTE i=0;
//
//	if(thCompassTimer ==0)
//	{
//		thCompassTimer = RegisterTimer();
//	}
//
//	ResetTimer( thCompassTimer, COMPASS_POWER_UP_TIME );
//	StartTimer( thCompassTimer );
////	if(thCompassTimer)
////	{
////		while(!TimerExpired( thCompassTimer));
////	}
////	else
////	{
////		__delay_cycles(400000);
////	}
////	UnregisterTimer(thCompassTimer);
//
//	__delay_cycles(1000);
//	// clear the buffers
//	for(i=0;i<sizeof(RxCompassBuffer);i++)
//	{
//		RxCompassBuffer[i]=0;
//		TxCompassBuffer[i]=0;
//	}
//
//	switch(number)
//	{
//		case 0:
//			i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
//			break;
//		case 1:
//			i2cCompass.bySlaveAddress = COMPASS_ADDRESS_2;
//			break;
//		default:
//			break;
//	}
//
// 	i2cCompass.ptrTxQ = TxCompassBuffer;
//   	i2cCompass.wTxQIndex = 0;
//    i2cCompass.wTxQSize = 2;
//    i2cCompass.bTxQSending = TRUE;
//    i2cCompass.ptrRxQ = RxCompassBuffer;
//    i2cCompass.wRxQIndex = 0;
//    i2cCompass.wRxQSize = 0;
//    i2cCompass.bRxQReceiving = FALSE;
//
//
//    TxCompassBuffer[0] = M_CTRL_REG1_ADDR;
//	TxCompassBuffer[1] = MAGN_ONLY_ON;
//
//    i2cCompassWrite(&i2cCompass);
////	ResetTimer( thCompassTimer, COMPASS_WRITE_TIME );
////	while(!TimerExpired( thCompassTimer));
//    __delay_cycles(10000);
//	i2cCompass.ptrTxQ = TxCompassBuffer;
//	i2cCompass.wTxQIndex = 0;
//	i2cCompass.wTxQSize = 2;
//	i2cCompass.bTxQSending = TRUE;
//	i2cCompass.ptrRxQ = RxCompassBuffer;
//	i2cCompass.wRxQIndex = 0;
//	i2cCompass.wRxQSize = 0;
//	i2cCompass.bRxQReceiving = FALSE;
//
//
//	TxCompassBuffer[0] = CTRL_REG1_ADDR;
//	TxCompassBuffer[1] = M_ODR_100HZ | ACTIVE_MODE;
//    i2cCompassWrite(&i2cCompass);
////	ResetTimer( thCompassTimer, COMPASS_WRITE_TIME );
////	while(!TimerExpired( thCompassTimer));
//    __delay_cycles(10000);
//
//}

void CompassInit(void)
{
	BYTE i=0;

	if(thCompassTimer ==0)
	{
		thCompassTimer = RegisterTimer();
	}

	ResetTimer( thCompassTimer, COMPASS_POWER_UP_TIME );
	StartTimer( thCompassTimer ); 
//	if(thCompassTimer)
//	{
//		while(!TimerExpired( thCompassTimer));
//	}
//	else
//	{
//		__delay_cycles(400000);
//	}
//	UnregisterTimer(thCompassTimer);

	__delay_cycles(400000);
	// clear the buffers
	for(i=0;i<sizeof(RxCompassBuffer);i++)
	{
		RxCompassBuffer[i]=0;
		TxCompassBuffer[i]=0;
	}
	

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
	
 	i2cCompass.ptrTxQ = TxCompassBuffer;                           
   	i2cCompass.wTxQIndex = 0;                              
    i2cCompass.wTxQSize = 4;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;                              
    i2cCompass.wRxQSize = 0;
    i2cCompass.bRxQReceiving = FALSE;
    

    TxCompassBuffer[0] = COMP_M_CTRL_REG1_ADDR;
	TxCompassBuffer[1] = COMP_MAGN_ONLY_ON;
	TxCompassBuffer[2] = 0x10;
	TxCompassBuffer[3] = 0x80;

    i2cCompassWrite(&i2cCompass);
//	ResetTimer( thCompassTimer, COMPASS_WRITE_TIME );
//	while(!TimerExpired( thCompassTimer));
    __delay_cycles(20000);
	i2cCompass.ptrTxQ = TxCompassBuffer;
	i2cCompass.wTxQIndex = 0;
	i2cCompass.wTxQSize = 2;
	i2cCompass.bTxQSending = TRUE;
	i2cCompass.ptrRxQ = RxCompassBuffer;
	i2cCompass.wRxQIndex = 0;
	i2cCompass.wRxQSize = 0;
	i2cCompass.bRxQReceiving = FALSE;


	TxCompassBuffer[0] = COMP_CTRL_REG1_ADDR;
	TxCompassBuffer[1] = COMP_M_ODR_100HZ | COMP_ACTIVE_MODE;
    i2cCompassWrite(&i2cCompass);
//	ResetTimer( thCompassTimer, COMPASS_WRITE_TIME );
//	while(!TimerExpired( thCompassTimer));
    __delay_cycles(20000);

}

void CompassReadDataStart(void)
{

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
 	i2cCompass.ptrTxQ = TxCompassBuffer;                           
   	i2cCompass.wTxQIndex = 0;                              
    i2cCompass.wTxQSize = 1;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;                              
    i2cCompass.wRxQSize = 6;
    i2cCompass.bRxQReceiving = TRUE;
    
    TxCompassBuffer[0] = COMP_M_OUT_X_MSB_ADDR;
//    SetOutputPin(TP2, TRUE);
    i2cCompassRead(&i2cCompass);
     	
}


BOOL CompassDataReady(void)
{
	return (!i2cCompass.bRxQReceiving);
}

BOOL CompassTxDone(void)
{
	return (!i2cCompass.bTxQSending);
}

// read all conffiguration registers
//CRA_REG_M (00h), CRB_REG_M (01h), MR_REG_M (02h)
void CompassRegisterDump(void)
{

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
	i2cCompass.ptrTxQ = TxCompassBuffer;
   	i2cCompass.wTxQIndex = 0;                              
    i2cCompass.wTxQSize = 1;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;                              
    i2cCompass.wRxQSize = 3;
    i2cCompass.bRxQReceiving = TRUE;
    
    TxCompassBuffer[0] = COMP_M_CTRL_REG1_ADDR;		// address of the first config register
    i2cCompassRead(&i2cCompass);
}


void CompassReadId(void)
{

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
	i2cCompass.ptrTxQ = TxCompassBuffer;
   	i2cCompass.wTxQIndex = 0;
    i2cCompass.wTxQSize = 1;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;
    i2cCompass.wRxQSize = 2;
    i2cCompass.bRxQReceiving = TRUE;

    TxCompassBuffer[0] = COMP_WHO_AM_I_ADDR;		// address of the first config register
    i2cCompassRead(&i2cCompass);
}

// copy the compass data into the given buffer 
void CompassGetRawData(BYTE *pData, WORD maxSize)
{
	unsigned int i=0;
	while((i< 6)&&(i<maxSize))
	{
		pData[i] = RxCompassBuffer[i];
		i++;
	}
}

// return the address of the compass data
BYTE* CompassGetDataPointer(void)
{
	return RxCompassBuffer;
}


int* CompassGetDataPointerInt(void)
{
	return (int*)RxCompassBuffer;
}


void CompassDisable(void)
{

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
	i2cCompass.ptrTxQ = TxCompassBuffer;
	i2cCompass.wTxQIndex = 0;
	i2cCompass.wTxQSize = 2;
	i2cCompass.bTxQSending = TRUE;
	i2cCompass.ptrRxQ = RxCompassBuffer;
	i2cCompass.wRxQIndex = 0;
	i2cCompass.wRxQSize = 0;
	i2cCompass.bRxQReceiving = FALSE;

	TxCompassBuffer[0] = COMP_CTRL_REG1_ADDR;
	TxCompassBuffer[1] = 0;
    i2cCompassWrite(&i2cCompass);
    __delay_cycles(10000);
}

void CompassEnable(void)
{

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
	i2cCompass.ptrTxQ = TxCompassBuffer;
	i2cCompass.wTxQIndex = 0;
	i2cCompass.wTxQSize = 2;
	i2cCompass.bTxQSending = TRUE;
	i2cCompass.ptrRxQ = RxCompassBuffer;
	i2cCompass.wRxQIndex = 0;
	i2cCompass.wRxQSize = 0;
	i2cCompass.bRxQReceiving = FALSE;


	TxCompassBuffer[0] = COMP_CTRL_REG1_ADDR;
	TxCompassBuffer[1] = COMP_M_ODR_100HZ | COMP_ACTIVE_MODE;
    i2cCompassWrite(&i2cCompass);
    __delay_cycles(10000);

}


void CompassPowerDown(void)
{

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
	i2cCompass.ptrTxQ = TxCompassBuffer;
	i2cCompass.wTxQIndex = 0;
	i2cCompass.wTxQSize = 2;
	i2cCompass.bTxQSending = TRUE;
	i2cCompass.ptrRxQ = RxCompassBuffer;
	i2cCompass.wRxQIndex = 0;
	i2cCompass.wRxQSize = 0;
	i2cCompass.bRxQReceiving = FALSE;

	TxCompassBuffer[0] = COMP_CTRL_REG2_ADDR;
	TxCompassBuffer[1] = COMP_SLEEP_MODE;
    i2cCompassWrite(&i2cCompass);
    __delay_cycles(10000);

}


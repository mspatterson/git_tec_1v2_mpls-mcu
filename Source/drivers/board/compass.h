#ifndef COMPASS_H_
#define COMPASS_H_

#include "..\drivers.h"

#define DEVICE_PN		FXOS8700CQ

#define COMPASS_ADDRESS_1	0x1E		// U1 on SCH-00686-03-2v0
//#define COMPASS_ADDRESS_2	0x1D		// U2 on SCH-00686-03-2v0

#define COMP_SYSMOD_ADDR			0X0B
#define COMP_INT_SOURCE_ADDR		0X0C
#define COMP_WHO_AM_I_ADDR			0X0D

#define COMP_CTRL_REG1_ADDR			0X2A
#define COMP_CTRL_REG2_ADDR			0X2B
#define COMP_CTRL_REG3_ADDR			0X2C
#define COMP_CTRL_REG4_ADDR			0X2D
#define COMP_CTRL_REG5_ADDR			0X2E

#define COMP_M_DR_STATUS_ADDR		0X32
#define COMP_M_OUT_X_MSB_ADDR		0X33
#define COMP_M_OUT_X_LSB_ADDR		0X34
#define COMP_M_OUT_Y_MSB_ADDR		0X35
#define COMP_M_OUT_Y_LSB_ADDR		0X36
#define COMP_M_OUT_Z_MSB_ADDR		0X37
#define COMP_M_OUT_Z_LSB_ADDR		0X38
#define COMP_TEMP_ADDR				0X51
#define COMP_M_CTRL_REG1_ADDR		0X5B
#define COMP_M_CTRL_REG2_ADDR		0X5C
#define COMP_M_CTRL_REG3_ADDR		0X5D
#define COMP_M_INT_SRC_ADDR			0X5E
#define COMP_M_VECM_CFG_ADDR		0X69
#define COMP_M_VECM_THS_MSB_ADDR	0X6A
#define COMP_M_VECM_THS_LSB_ADDR	0X6B

#define COMP_WHO_AM_I_VAL			0XC7
#define COMP_ACTIVE_MODE			0X01
#define COMP_M_ODR_800HZ			0<<3
#define COMP_M_ODR_400HZ			1<<3
#define COMP_M_ODR_200HZ			2<<3
#define COMP_M_ODR_100HZ			3<<3
#define COMP_M_ODR_50HZ				4<<3
#define COMP_M_ODR_12HZ				5<<3
#define COMP_M_ODR_6HZ				6<<3
#define COMP_M_ODR_1HZ				7<<3

#define COMP_ACC_ONLY_ON			0X00
#define COMP_MAGN_ONLY_ON			0X01
#define COMP_ACC_AND_MAGN_ON		0X11

#define COMP_STANDBY_MODE			0X00
#define COMP_WAKE_MODE				0X01
#define COMP_SLEEP_MODE				3<<3

#define COMP_X_AXIS					0
#define COMP_Z_AXIS					1
#define COMP_Y_AXIS					2

#define COMPASS_POWER_UP_TIME		50
#define COMPASS_WRITE_TIME			10
#define NBR_FIELD_RANGE				7

void CompassInit(void);
void CompassReadDataStart(void);
BOOL CompassDataReady(void);
BOOL CompassTxDone(void);
void CompassRegisterDump(void);
void CompassGetRawData(BYTE *pData, WORD maxSize);
void CompassEnable(void);
void CompassDisable(void);
BYTE* CompassGetDataPointer(void);
int* CompassGetDataPointerInt(void);
void CompassReadId(void);
void CompassPowerDown(void);

#endif /*COMPASS_H_*/

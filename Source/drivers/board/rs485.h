#ifndef RS485_MODULE_H
#define RS485_MODULE_H

#include "..\drivers.h"


void DisableRS485(void);
void EnRXRS485(void);
void EnTXRS485(void);
void EnTXRXRS485(void);
BOOL InitRs485Interface(void);
void atRs485TxISR(void);
void atRs485RxISR(void);
BOOL rs485ReceiverHasData(void);
WORD rs485ReadReceiverData(BYTE *pData, WORD maxSize);
BOOL rs485SendBinaryData(BYTE* pData, WORD wDataLen);
BOOL rs485SendCommand(BYTE* pData, WORD wDataLen );
BOOL IsRs485Sending(void);


#endif /*RS485_MODULE_H*/

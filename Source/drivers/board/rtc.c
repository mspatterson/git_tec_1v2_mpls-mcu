/*
 * rtc.c
 *
 *  Created on: 2013-11-19
 *      Author: DD
 */
 
#include "rtc.h"

#define RTC_BUF_SIZE 10
I2C_OBJECT	i2cRtc;
BYTE RxRtcBuffer[RTC_BUF_SIZE];
BYTE TxRtcBuffer[RTC_BUF_SIZE];

//
//void CompassInit(void)
//{
//	BYTE i=0;
//	TIMERHANDLE thCompassTimer;
//	thCompassTimer = RegisterTimer();
//	CompassEnable();
//	ResetTimer( thCompassTimer, COMPASS_POWER_UP_TIME );
//	StartTimer( thCompassTimer );
//	if(thCompassTimer)
//	{
//		while(!TimerExpired( thCompassTimer))
//		{
//			ResetWatchdog();
//		}
//	}
//	else
//	{
//		ResetWatchdog();
//		__delay_cycles(400000);
//	}
//	UnregisterTimer(thCompassTimer);
//
//	// clear the buffers
//	for(i=0;i<sizeof(RxRtcBuffer);i++)
//	{
//		RxRtcBuffer[i]=0;
//		TxRtcBuffer[i]=0;
//	}
//
//
// 	i2cRtc.bySlaveAddress = COMPASS_ADDRESS;
// 	i2cRtc.ptrTxQ = TxRtcBuffer;
//   	i2cRtc.wTxQIndex = 0;
//    i2cRtc.wTxQSize = 4;
//    i2cRtc.bTxQSending = TRUE;
//    i2cRtc.ptrRxQ = RxRtcBuffer;
//    i2cRtc.wRxQIndex = 0;
//    i2cRtc.wRxQSize = 0;
//    i2cRtc.bRxQReceiving = FALSE;
//
//    // the MSB of the SUB address has to be '1' to allow multiple data read/write
//    TxRtcBuffer[0] = CRA_REG_M_ADDR |0x80;		// address of the first config register
////	TxRtcBuffer[1] = DATA_RATE_220_HZ;		// CRA_REG_M register data
//	TxRtcBuffer[1] = DATA_RATE_7P5_HZ;		// CRA_REG_M register data
//	TxRtcBuffer[2] = INPUT_RANGE_1P3_GAUSS;	// CRB_REG_M register data
//	TxRtcBuffer[3] = CONT_MODE;				// MR_REG_M	register data
//
//    i2cRtcWrite(&i2cRtc);
//
//}

//
//void RtcReadDataStart(void)
//{
// 	i2cRtc.bySlaveAddress = COMPASS_ADDRESS;
// 	i2cRtc.ptrTxQ = TxRtcBuffer;
//   	i2cRtc.wTxQIndex = 0;
//    i2cRtc.wTxQSize = 1;
//    i2cRtc.bTxQSending = TRUE;
//    i2cRtc.ptrRxQ = RxRtcBuffer;
//    i2cRtc.wRxQIndex = 0;
//    i2cRtc.wRxQSize = 6;
//    i2cRtc.bRxQReceiving = TRUE;
//
//    TxRtcBuffer[0] = X_DATA_HIGH_ADDR | 0x80;		// address of the first config register
//    i2cRtcRead(&i2cRtc);
//
//}


BOOL RtcDataReady(void)
{
	return (!i2cRtc.bRxQReceiving);
}

BOOL RtcTxDone(void)
{
	return (!i2cRtc.bTxQSending);
}

// read all conffiguration registers
////CRA_REG_M (00h), CRB_REG_M (01h), MR_REG_M (02h)
//void RtcRegisterDump(void)
//{
//	i2cRtc.bySlaveAddress = COMPASS_ADDRESS;
// 	i2cRtc.ptrTxQ = TxRtcBuffer;
//   	i2cRtc.wTxQIndex = 0;
//    i2cRtc.wTxQSize = 1;
//    i2cRtc.bTxQSending = TRUE;
//    i2cRtc.ptrRxQ = RxRtcBuffer;
//    i2cRtc.wRxQIndex = 0;
//    i2cRtc.wRxQSize = 3;
//    i2cRtc.bRxQReceiving = TRUE;
//
//    TxRtcBuffer[0] = CRA_REG_M_ADDR| 0x80;		// address of the first config register
//    i2cRtcRead(&i2cRtc);
//}

// copy the compass data into the given buffer 
//void RtcGetRawData(BYTE *pData, WORD maxSize)
//{
//	unsigned int i=0;
//	while((i< 10)&&(i<maxSize))
//	{
//		pData[i] = RxRtcBuffer[i];
//		i++;
//	}
//}



void StopRtc(void)
{
	i2cRtc.bySlaveAddress = RTC_ADDRESS;
 	i2cRtc.ptrTxQ = TxRtcBuffer;
   	i2cRtc.wTxQIndex = 0;
    i2cRtc.wTxQSize = 2;
    i2cRtc.bTxQSending = TRUE;
    i2cRtc.ptrRxQ = RxRtcBuffer;
    i2cRtc.wRxQIndex = 0;
    i2cRtc.wRxQSize = 0;
    i2cRtc.bRxQReceiving = FALSE;

    TxRtcBuffer[0] = 0x01;			// address of the first config register
	TxRtcBuffer[1] = 0x80;			// Bit D7 of register 01h contains the STOP bit (ST).

	i2cCompassWrite(&i2cRtc);
}




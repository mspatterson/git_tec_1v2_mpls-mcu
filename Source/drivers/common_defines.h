#ifndef COMMON_DEF_H_
#define COMMON_DEF_H_

#include "drivers.h"
//  Date            Author      Comment
//  ----------------------------------------------------------------
// 	2015-09-09		DD			added - use ADC4_CSn (#6). Change ADC_USED_LAST to 6.
//*******************************************************************************

#define NBR_MAGNETIC_DATA		1

typedef struct
{
	int iXField[NBR_MAGNETIC_DATA];
	int iZField[NBR_MAGNETIC_DATA];
	int iYField[NBR_MAGNETIC_DATA];
}MAGNETIC_DATA;


typedef struct
{
	int iXField[NBR_MAGNETIC_DATA];
	int iZField[NBR_MAGNETIC_DATA];
	int iYField[NBR_MAGNETIC_DATA];
}ACCELEROMETOR_DATA;


typedef struct
{
	int ChargeDataRaw;
	WORD ChargeDataMa;
	int CurrentValue;
	WORD NumberOfConversion;
	WORD BatVoltageMv;
	WORD TempMilliDegree;

}GAS_GAUGE_DATA;

#define RADIO_POWER_4DBM			0XF5
#define RADIO_POWER_2DBM			0XE5
#define RADIO_POWER_1DBM			0XD5
#define RADIO_POWER_N8DBM			0X75

#define TX_ACK						0XB3
#define TX_NACK						0XDC
#define COMMAND						0X53
#define TX_DATA						0X29
#define RADIO_POWER_DOWN			0XA7
#define RADIO_CHANGE_CHANNEL		0XA9
#define RADIO_CHANGE_POWER			0XA6
#define RADIO_RCVR_ON           	0XA1
#define RADIO_RCVR_OFF          	0XA3
#define RADIO_RCVR_ON_TIMED     	0XA2
#define RADIO_RCVR_OFF_TIMED    	0XA4
#define RADIO_SET_ADDRESS       	0XA5
#define RADIO_SET_PAN_ID        	0XA8
#define RADIO_SET_TIMEOUT       	0XCA

#define DEFAULT_RADIO_POWER			RADIO_POWER_N8DBM
#define RFC_RESPONSE_TIME			200	//ms
#define BAT_INPUTS					3
#define BAT_NIMH_MARGINE 			400
#define BATTERY_NIMH				2
#define BATTERY_LI					1
#define RFC_HUNTING_PERIOD_MS		100
#define CHANNEL_CYCLE_MS			500
#define UPDATE_CHANNEL_NBR_TR		6
#define NBR_RESP_CONNECTION			3
#define HUNTING_MODE_TIMEOUT_SEC	900	// SEC = 15MIN
#define IDLE_MODE_TIMEOUT_SEC		30	// sec
#define INIT_IDLE_PERIOD			200	//ms
#define TRANSMISSION_TIMEOUT		10
#define RADIO_COMM_TIMEOUT			60000	// 30 sec.


#define ZB_CHANNEL_1 			11
#define ZB_CHANNEL_2 			15
#define ZB_CHANNEL_3 			20
#define ZB_CHANNEL_4 			25
#define NBR_RF_CHANNELS			4

#define WDT_ARST_8M       (WDTPW+WDTCNTCL+WDTSSEL0+WDTIS1)                         /* 1000ms  " */

#define BUF_LENGTH				128
#define ALL_SIX_ADC_READ		0X3F
#define ALL_7_ADC_READ			0X7F
#define IDLE_MODE_ADC_SAMPLES	4
#define IDLE_TASK_TIME_MS		15	//20

#define ZB_DEBUG		1
#define ADC_RAW_DATA	1
#define ADC_DEBUG_SLOW	1
#define ADC_USED_FIRST	0	// number of the first adc used for strain gauges
//#define ADC_USED_LAST	7
#define ADC_USED_LAST	6	// number of the last adc used for strain gauges
#define ADS1220			1220
#define ADC_NOT_USED	6	// number of the adc available on the board but not used in current measurement. must be in power down mode.

#define ADC_USED_AS_TEMP	7	// number of the adc used as temp sensor in RFC mode.
#define ADC_EPSON_GYRO 		7	// number of the adc connected to the analog gyro.
#define ADC_TORQUE_CH0	0
#define ADC_TORQUE_CH1	1
#define ADC_TENSION_CH0	2
#define ADC_TENSION_CH1	3
#define ADC_TENSION_CH2	4
#define ADC_TENSION_CH3	5

#define ADS1220_TOTAL				8

//#define USED_ADC_PN		ADS1220
#define USED_ADC_PN		ADS1220_REVC

#define	ADC_TORQUE_000		0
#define	ADC_TORQUE_180		1
#define	ADC_TENSION_000		2
#define	ADC_TENSION_090		3
#define	ADC_TENSION_180		4
#define	ADC_TENSION_270		5
#define	ADC_SWITCH_IN		6

//define only one full scale
//#define ST_GYRO_250DPS
#define ST_GYRO_2000DPS		1

#define IDLE_SLEEP_TIME		450
#define SAMPLE_BATTERY_TIME_SEC		5//60

#define UVLO_LOCKOUT_MV			2800
#define TENSION_SG_OFF_PERIOD	900
#define TENSION_SG_ON_PERIOD	95
#define TENSION_SG_STARTUP		5
#define TORQUE_VREF_ON_TIME		7
#define TORQUE_READ_START_TIME	3	//4

#define RADIO_RX_ON_TIME	500//10		// time the radio receiver will be on - in tens of milliseconds

#define TIME_1000MS			1000


#define NBR_PTS_BOARDS	4

#endif /*COMMON_DEF_H_*/

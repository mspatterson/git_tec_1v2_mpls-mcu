/*
 * SPI.h
 *
 *  Created on: 2012-05-17
 *      Author: Owner
 */

#ifndef SPI_H_
#define SPI_H_

#include "..\drivers.h"


BOOL SpiA0Initialize(void);
void SpiA0TxRxPacket(BYTE *pDataBuffer, WORD wLength);
BOOL SpiB0Initialize(void);
void SpiB0TxRxPacket(BYTE *pDataBuffer, WORD wLength);

BOOL SpiA3Initialize(void);
void SpiA3TxRxPacket(BYTE *pDataBuffer, WORD wLength);

#endif /* SPI_H_ */

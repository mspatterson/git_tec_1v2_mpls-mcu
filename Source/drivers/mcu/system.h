/*
 * system.h
 *
 *  Created on: 2012-05-17
 *      Author: Owner
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "..\drivers.h"

void Set_System_Clock (void);

void SetSystemDeepSleep(void);

void Set_System_Clock_LPM (void);

void SetWatchdog(void);

void ResetWatchdog(void);

void StopWatchdog(void);

void ReadLastResetCause(void);

BYTE GetLastReset(void);

void DisablePeriferals(void);

#endif /* SYSTEM_H_ */
